site_name: "Ressources pour la spécialité NSI"
site_description: Des documents aidant à réaliser des projets « un peu fun », pour les débutants et les plus-si-débutants que ça !
site_author: Tristan LEY
site_url: https://trieles.gitlab.io/nsi
repo_url: https://gitlab.com/trieles/nsi
edit_uri: tree/main/docs/

docs_dir: docs

nav:
  - "🏡 Accueil": index.md
  - ... | regex=^(?:(?!_REM.md).)*$


theme:
    name: material
    font: false                        # RGPD -> pas de fonte Google
    language: fr
    features:
        - navigation.instant
        - navigation.tabs
        - navigation.expand
        - navigation.top
        - toc.integrate
        - header.autohide
        - content.code.annotate
        #- content.tabs.link
    custom_dir: my_theme_customizations/
    palette:
        # Light mode
        - media: "(prefers-color-scheme: light)"
          scheme: default
          primary: teal
          accent: green
          toggle:
              icon: material/weather-night  #toggle-switch-off-outline
              name: Passer l'affichage en mode sombre

        # Dark mode
        - media: "(prefers-color-scheme: dark)"
          scheme: slate
          primary: grey
          accent: teal
          toggle:
              icon: material/weather-sunny   #toggle-switch
              name: Passer l'affichage en mode clair

markdown_extensions:
    - meta
    - abbr
    - def_list                      # Les listes de définition.
    - attr_list                     # Un peu de CSS et des attributs HTML.
    - footnotes                     # Notes[^1] de bas de page.  [^1]: ma note.
    - md_in_html                    # Markdown in HTML
    - pymdownx.caret                # Passage ^^souligné^^ ou en ^exposant^.
    - pymdownx.mark                 # Passage ==surligné==.
    - pymdownx.tilde                # Passage ~~barré~~ ou en ~indice~.
    - pymdownx.snippets             # Inclusion de fichiers externe.
    - admonition                    # Blocs colorés  !!! info "ma remarque" 
    - pymdownx.details              # ... qui peuvent se plier/déplier
    - pymdownx.highlight:           # Coloration syntaxique du code
          linenums: true
          auto_title: true
          auto_title_map:
              "Python": "🐍 Script Python"
              "Python Console Session": "🐍 Console Python"
              "Text Only": "📋 Texte"
              "E-mail": "📥 Entrée"
              "Text Output": "📤 Sortie"
    - pymdownx.inlinehilite         # pour  `#!python  <python en ligne>`
    - pymdownx.tasklist:            # Cases à cocher  - [ ]  et - [x]
          custom_checkbox: false    # ...avec cases d'origine
          clickable_checkbox: true  # ...et cliquables.
    - pymdownx.inlinehilite
    - pymdownx.superfences          # Imbrication de blocs.
    - pymdownx.snippets             # Pour importer des fichiers extérieurs
    - pymdownx.keys:                # Touches du clavier.  ++ctrl+d++
        separator: "\uff0b"
    - pymdownx.tabbed:
          alternate_style: true
    - pymdownx.betterem:            # Amélioration de la gestion de l'emphase (gras & italique).
          smart_enable: all
    - pymdownx.emoji:               # Émojis  :boom:
          emoji_index: !!python/name:materialx.emoji.twemoji
          emoji_generator: !!python/name:materialx.emoji.to_svg
    - pymdownx.arithmatex:          # Formules en LaTeX dans le MD, converties en MathJax
          generic: true
    - toc:
          permalink: ⚓︎
          toc_depth: 4
    - pymdownx.superfences:
        custom_fences:
          - name: mermaid
            class: mermaid
            format: !!python/name:pymdownx.superfences.fence_code_format

plugins:
    - search
    - exclude-search:           # exclusion de pages de la recherche
        exclude:
            - N*/*/*/*.md         # les REM
            - N*/*/*/*.py         # Les corrigés
            - xtra/*.md           # inutile de chercher cela
    - awesome-pages:
        collapse_single_pages: true
    - macros
    #~ - pdf-export

extra:
    social:
        - icon: fontawesome/solid/paper-plane
          link: mailto:qui@chez.moi
          name: Écrire à l'auteur

extra_javascript:
    - xtra/javascripts/mathjax-config.js                    # MathJax
    - javascripts/config.js
    - https://polyfill.io/v3/polyfill.min.js?features=es6
    - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js

extra_css:
    - xtra/pyoditeur.css
    - xtra/stylesheets/ajustements.css                      # ajustements
