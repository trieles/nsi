# Tkinter pour créer des jeux

La bibliothèque Tkinter n'est pas vraiment pensée pour le jeu : [PyGame][pygame-wikipedia]
est clairement plus adaptée.

Néanmoins, il est fort possible, sinon probable, que la connaissance
de Tkinter vous rende quelques services dans l'avenir : si vous travaillez
dans le monde de l'informatique, il vous arrivera certainement d'avoir besoin
d'un petit logiciel doté d'une interface graphique, que vous devrez coder
rapidement : le couple Python / Tkinter excelle, pour cela !

D'où l'intérêt de passer un peu de temps à apprendre à s'en servir, y compris
dans une optique un peu ludique. Car même si Tkinter n'est pas « fait pour »,
on peut faire des choses sympathiques avec...

Mais il faut pour cela avoir conscience des limites de l'outil, et savoir
comment les dépasser.

La première section ci-dessous donne d'abord les réponses : quelqu'un qui
veut juste « savoir comment faire » peut s'en contenter, dans un premier
temps. Mais il sera instructif et profitable pour lui de lire également
la seconde section, qui expose pas à pas les difficultés et explicite les
moyens de les contourner.


## Comment réaliser un jeu d'action / arcade ?

Ce qui est indiqué ici est le bilan, à retenir et à utiliser, des expériences
qui sont décrites et expliquées plus loin, dans la [seconde section](#les-difficultes-expliquees-pas-a-pas).


### Les difficultés principales

* Il sera diﬃcile d'aborder des sujets « riches » en programmation impérative :
tant qu'un traitement long ne viendra pas à son terme, l'interface graphique
sera figée ! Ainsi, il faut s'assurer que les calculs nécessaires à l'actualisation
d'un jeu soient réalisables durant le laps de temps qui sépare deux actualisations
de l'affichage.  
**Solution :** utiliser la [Programmation Orientée Objet (POO)](https://fr.wikipedia.org/wiki/Programmation_orient%C3%A9e_objet){target='__blank' rel='noopener'}
et les [threads](https://python-course.eu/applications-python/threads.php){target='__blank' rel='noopener'}.
Mais en NSI, si la Programmation Orientée Objet est abordée en classe de
Terminale, la programmation multi-threadée est *très largement* hors-programme...

* Il faut ruser pour gérer les appuis d'un joueur (ou de plusieurs joueurs)
sur plusieurs touches du clavier « à la fois » (ainsi que le fait que des
touches puissent rester enfoncées).

* Il faut ruser pour contourner le délai avant qu'un appui « long » sur
une touche ne déclenche une répétition de l'action associée. En effet, une
temporisation existe *systématiquement*, dans les interfaces graphiques :
qui voudrait qu'en appuyant de façon un tout petit peu trop longue sur la
++"barre d'espacement"++ ou sur la touche ++a++ le caractère correspondant
soit *immédiatement* inséré de façon répétée ? Le clavier deviendrait inutilisable !

* Les *bindings* peuvent avoir des comportements surprenants à première
vue (mais parfaitement logiques lorsqu'on lit la documentation !). Exemple :
redéﬁnir le comportement de la touche ++enter++ sur un widget `Text` (où
elle est censée faire passer à la ligne) aura des conséquences pénibles,
si l'on n'y prend pas garde...

* Windows et Linux ne fonctionnent pas de la même manière :
    * maintenir une touche enfoncée génère sous Linux une succession d'appuis
    / relâchements (cf. [:gb: cette question sur stackoverflow](https://stackoverflow.com/questions/8082277/tkinter-return-evt-on-key-release){target='__blank' rel='noopener'}) ;
    * sous Windows, il semble qu'il y ait une succession d'appuis et *un
    seul* relâchement ﬁnal (*quid* de MacOS ?) ;
    * presser et maintenir enfoncée une première touche, puis une seconde,
    stoppe le ﬂux d'appuis / relâchements de la première, ce qui a pour
    eﬀet d'empêcher la reprise du mouvement initial. Concrètement, imaginez
    un disque que vous voudriez pouvoir déplacer dans les 4 directions à
    l'aide des flèches de direction du clavier : si vous appuyez *et maintenez
    enfoncé* la flèche de droite, le disque ira vers la droite *sans discontinuer*.
    Si, **en plus**, vous appuyez sur la flèche « vers le haut », alors
    le mouvement vers la droite pourra cesser, **mais surtout le mouvement
    vers la droite ne reprendra pas** lorsque vous relâcherez l'appui sur la
    « flèche vers le haut ».
    

### Les solutions

* Lorsqu'on veut déplacer un objet, il faut éviter d'attacher une fonction
à chacune des touches qui gère le mouvement. **Il faut plutôt intercepter
les appuis sur *toutes* les touches, déduire les déplacements à eﬀectuer à
partir des touches enfoncées, puis les réaliser.** Concrètement, on attache
une seule et même fonction à l'ensemble des touches utilisées dans le jeu.

* Pour garder la mémoire des touches enfoncées, y compris lorsqu'elle sont
pressées simultanément, il ne faut pas se contenter d'accumuler dans une
liste le code de la touche associé à l'évènement (il est contenu dans l'attribut
`keysym` de l'objet `evenement` généré, donc `evenement.keysym` ou `evt.keysym` pour
abréger) : on aurait de nombreuses répétititions inutiles. On peut donc
n'ajouter un nouveau « symbole de touche » **que s'il n'est pas déjà présent**
dans la liste des « touches déjà enfoncées » (d'où la nécessité de réaliser
d'abord un test d'appartenance). On peut aussi utiliser une structure de
données Python qui a la propriété d'éliminer les redondances : le
[`set`](https://docs.python.org/fr/3/tutorial/datastructures.html#sets){target='__blank' rel='noopener'}
(ou ensemble).

* On utilise naturellement la méthode `after` de la fenêtre principale de
l'application pour déclencher une action et la répéter périodioquement.
**La fonction qui gère les déplacements ― mettons qu'elle se nomme `action()` ―
ne devra être appelée qu'une fois**, au démarrage du jeu, et **vers la fin
de son code source, la fenêtre principale devra utiliser sa méthode `after`
pour la rappeler**. Exemple : `fenetre.after(25, action)` (ainsi, il y aura
40 exécutions de la fonction `action()` par seconde, un appel tous les 25 ms).


### Un exemple fonctionnel complet

Essayez le code ci-dessous (copiez-collez-le dans un fichier `animer.py`)
et analysez-le en détail ! Il est documenté (à la fois à travers les
commentaires et *via* les *docstrings* des fonctions).

```python
# coding: utf8
from tkinter import *
from datetime import datetime

DEBUG = False
touches = set()

def enfoncer(evt):
    """Ajoute une touche à l'ensemble des touches enfoncées"""
    touches.add(evt.keysym)
    if DEBUG:
        print("Pression :", touches)

def relacher(evt):
    """Enlève une touche à l'ensemble des touches enfoncées"""
    if evt.keysym in touches:
        touches.remove(evt.keysym)
    if DEBUG:
        print("Relâchement :", touches)

def action():
    """Gère les déplacements"""
    #if DEBUG:
    #    print(datetime.now())
    if "Left" in touches:
        c.move(r, -20, 0)   # Décalage vers la gauche (∆x = -20, ∆y = 0).
    if "Right" in touches:
        c.move(r, 20, 0)    # Décalage vers la droite (∆x = +20, ∆y = 0).
    if "Up" in touches:
        c.move(r, 0, -20)   # Décalage vers le haut (∆x = 0, ∆y = -20).
    if "Down" in touches:   # ⚠ l'axe des ordonnées est orienté VERS LE BAS !
        c.move(r, 0, 20)    # Décalage vers le bas  (∆x = 0, ∆y = +20).
    f.after(25, action)     # action() est exécutée toutes les 25 ms

f = Tk()
c = Canvas(f, bg='dark grey', width=800, height=600)
r = c.create_oval(350, 350, 450, 450, fill="red", width=10, outline="blue")
c.pack()
action()                    # action() est appelée une première fois

f.bind('<KeyPress>', enfoncer)      # enfoncer() est lancée par un appui sur N'IMPORTE QUELLE touche
f.bind('<KeyRelease>', relacher)    # relacher() est lancée lorsqu'on relâche N'IMPORTE QUELLE touche

f.mainloop()
```

Voici une image animée montrant le code précédent en action. C'est bien
le « joueur » qui décide des mouvements de la balle en pressant, éventuellement
simultanément, les flèches de direction du clavier (question bonus : à votre
avis, que se passera-t-il s'il appuie en même temps sur les flèches haut
et bas ++"↑"+"↓"++, par exemple :wink: ?).  
![](images/bien_animer.gif){width=400}


## Les difficultés expliquées pas à pas

On se met ici dans la peau de quelqu'un qui découvre Tkinter, son *widget*
`Canvas` et les *bindings*. Cette personne souhaite déplacer un élément
graphique dessiné sur un canevas. Ce faisant, elle va rencontrer plusieurs
difficultés : nous allons voir comment l'aider à les dépasser...

### Un code naïf

Enregistrez le code ci-dessous dans un fichier nommé `animation1.py`.

```python
from tkinter import *

def gauche(evt):
    c.move(r, -20, 0)

def droite(evt):
    c.move(r, 20, 0)

def haut(evt):
    c.move(r, 0, -20)

def bas(evt):
    c.move(r, 0, 20)

f = Tk()
c = Canvas(f, bg='dark grey', width=800, height=600)
r = c.create_rectangle(100, 300, 500, 500, fill="red", width=10, outline="blue")
c.pack()
f.bind("<Left>", gauche)
f.bind("<Right>", droite)
f.bind("<Up>", haut)
f.bind("<Down>", bas)
f.mainloop()
```

**Observations**

Lorsqu'on exécute ce script `animation1.py`, et que l'on fait bouger le
carré affiché à l'aides des flèches directionnelles du clavier ++"←"++,
++"→"++, ++"↑"++ et ++"↓"++, on remarque vite que le déplacement n'est
pas tel qu'on le souhaite !

On voudrait :

* pouvoir se déplacer en diagonale ;

* qu'une première touche enfoncée, suivie d'une autre (pour un déplacement
diagonal, par exemple) reste active **après le relâchement** de la seconde ;

* ne pas avoir de latence au début du maintient en pression d'une touche.

    ??? info "Remarque"
    
        Cette latence est due à la gestion du clavier par le système d'exploitation,
        qui introduit une temporisation avant répétition. Il est ainsi possible
        de « taper une seule lettre à la fois » ! Autrement, des lettres seraient
        immédiatement insérées en grand nombre (et les ordinateurs modernes
        sont *très très* rapides !) : presser la touche ++a++ ne doit pas
        prendre plus que quelques centièmes de seconde, mais ce délai est
        suffisant pour qu'un ordinateur insère des *centaines* de lettres
        « A » !


### Investigation : comprendre ce qui advient lorsqu'on presse ou relâche une touche

Copiez-collez le code ci-dessous dans un fichier `saisies.py`. Exécutez-le,
décalez la fenêtre qui s'affiche de sorte à pouvoir observer ce qui aparaîtra
dans la console Python. Ensuite, après vous être assuré que la fenêtre Tkinter
« a le focus » (ce qui signifie que c'est la fenêtre active ― dans le doute
vous pouvez cliquer sur le bouton associé dans la barre des tâches ou le
*dock* pour la rendre active) :

* pressez **une seule** touche puis relachez la ;
* pressez **une seule touche ET gardez la enfoncée**, observez la console
 puis relachez la touche ;
* en observant bien la console Python, pressez **une seule touche ET gardez
la enfoncée**, puis **pressez une SECONDE touche**. Ensuite, **relachez
la seconde touche enfoncée PUIS la première**.

```python
from tkinter import *

def pression(evt):
    print("Appui sur", evt.keysym)

def relachement(evt):
    print("Fin de l'appui sur", evt.keysym)

f = Tk()
f.bind('<KeyPress>', pression)
f.bind('<KeyRelease>', relachement)
f.mainloop()
```

**Observations**

* Maintenir une touche enfoncée génère sous Linux une succession d'appuis
/ relâchements (cf. [:gb: cette page](https://stackoverflow.com/questions/8082277/tkinter-return-evt-on-key-release){target='__blank' rel='noopener'}).

* Sous Windows®, il semble qu'il y ait une succession d'appuis et un seul
relâchement ﬁnal (*quid* sous MacOS ? à vous de vérifier...). Presser et
maintenir enfoncée une première touche, puis une seconde, stoppe le ﬂux
d'appuis / relâchements de la première, ce qui a pour eﬀet d'empêcher la
reprise du mouvement initial !

**Moralité :** lorsque l'on veut déplacer un objet, il faut **éviter d'attacher
une fonction à chacune des touches** qui gère le mouvement. Il faut plutôt
**intercepter** les appuis sur toutes les touches, déduire les déplacements à eﬀectuer à partir des touches enfoncées, puis réaliser les actions utiles.


### Intercepter *tous* les appuis sur une touche

On souhaite n'avoir qu'une seule fonction, qui soit appelée dès qu'une touche
est enfoncée. Mais, si l'on veut que cette fonction réagisse de façon adaptée,
il faut qu'elle soit capable de réagir différemment selon la touche enfoncée !

Pour cela, on va exploiter l'évènement que la fenêtre Tkinter génère automatiquement
et passe à une fonction « *bindée* ». On a en effet passé sous silence,
jusqu'ici, le paramètre `evt` des fonctions `gauche`, `droite`, `haut` et
`bas` du code précédent !

Ce paramètre `evt` (abréviation de « évènement »), est un objet Python qui
comporte de nombreuses informations sur l'action qui a déclenché sa création.
En particulier, lorsqu'une touche est pressée, `evt.keysym` est une chaîne
de caractères identifiant cette touche.

Ne reste plus qu'à associer une unique fonction `action(evt)` à l'appui sur
une touche *quelconque* du clavier : la chaîne `"<Key>"` désigne justement
n'importe quelle touche, pour Tkinter.

Cela donne le code ci-dessous : vous pouvez le recopier dans le fichier
`animation2.py`.

```python
#!/usr/bin/env python3
# coding: utf8
from tkinter import *

def action(evt):
    touche = evt.keysym
    if touche == "Left":
        c.move(r, -20, 0)
    elif touche == "Right":
        c.move(r, 20, 0)
    elif touche == "Up":
        c.move(r, 0, -20)
    elif touche == "Down":
        c.move(r, 0, 20)

f = Tk()
c = Canvas(f, bg='dark grey', width=800, height=600)
r = c.create_rectangle(100, 300, 500, 500, fill="red", width=10, outline="blue")
c.pack()
f.bind("<Key>", action)
f.mainloop()
```


### Intercepter et conserver la trace de *toutes* les touches enfoncées

Pour pouvoir déplacer notre rectangle en diagonale, il est classique et
légitime que l'utilisateur appuie simultanément sur deux touches (par exemple
++"→"++ et ++"↑"++ pour un déplacement « vers le nord-est »). Il nous
faut donc pouvoir conserver **l'ensemble** des touches enfoncées.

On va utiliser pour cela une structure de données Python hors-programme
de NSI : le `set`, ou **ensemble** (...justement !). Lorsqu'on ajoute un
élément à un `set`, Python vérifie s'il n'y figure pas déjà :

* si ça n'est pas le cas, le nouvel élément est ajouté ;
* sinon... il ne l'est pas ! Tout simplement !

??? info "Remarque"

    Un `set` peut être remplacé par un ajout conditionnel à une liste :
    
    ```python
    touches = []
    if touche not in touches:
        touches.append(touche)
    ```
    
    Cela revient au même qu'utiliser un `set`, mais c'est moins lisible !

??? done "Analogie avec les ensembles mathématiques"

    Un `set` Python fonctionne comme un ensemble, en mathématiques :
    les « choses » que l'on y met ne peuvent s'y trouver qu'en un seul exemplaire. Prenons l'exemple de la résolution de l'équation $x^3-3x+2=0$. On peut remarquer que $x^3-3x+2 = (x-1)^2(x+2) = (x-1)(x-1)(x+2)$.  
    Ainsi, d'après le théorème du produit nul (*un produit de facteurs est nul si et seulement si l'un au moins de ces facteurs est nul*), cette équation a 3 solutions : $x=1$, $x=1$ et $x=-2$. Finalement, son ***ensemble* solution** est $S = \{-2 ; 1\}$ : la « solution double » $1$ n'apparaît qu'_une seule_ fois dans $S$.

Ré-écrivons le code précédent à l'aide d'un `set` : vous obtiendrez le script
`animation3.py`.

```python
from tkinter import *

DEBUG = True
touches = set()

def enfoncer(evt):
    touches.add(evt.keysym)
    if DEBUG :
        print("Pression :", touches)
    action()

def relacher(evt):
    if evt.keysym in touches:
        touches.remove(evt.keysym)
    if DEBUG :
        print("Relâchement :", touches)

def action():
    if "Left" in touches :
        c.move(r, -20, 0)
    if "Right" in touches :
        c.move(r, 20, 0)
    if "Up" in touches :
        c.move(r, 0, -20)
    if "Down" in touches :
        c.move(r, 0, 20)

f = Tk()
c = Canvas(f, bg="dark grey", width=800, height=600)
r = c.create_rectangle(350, 350, 450, 450, fill="red", width=10,
outline="blue")
c.pack()
f.bind("<KeyPress>", enfoncer)
f.bind("<KeyRelease>", relacher)
f.mainloop()
```

**Remarques sur le code précédent**

* On a ajouté un « mode de débogage » : à vous de le désactiver !

* Le `set` permet de ne pas se soucier des doublons : on peut donc ajouter
sans précaution tout « symbole de touche » (ligne 7). Par contre, il est
plus prudent de vérifier sa présence avant de demander sa suppression,
lorsque la touche est relachée (ligne 13) : autrement, on risquerait de
provoquer une erreur.  
En pratique, cela n'arrivera pas : la suppression d'un symbole de touche
n'arrive que lorsque l'évènement `<"KeyRelease">` survient, ce qui signifie
qu'une touche a nécessairement été relachée, donc qu'elle avait bien été
enfoncée précédemment, et donc que le symbole correspondant était obligatoirement
présent dans le `set` (ici nommé `touches`).


Ne reste plus, maintenant, qu'à venir à bout de la temporisation qui empêche
le rectangle de bouger immédiatement, dès l'appui sur une touche de direction.


### Mécanique du fluide : la méthode `after`

Si vous avez lu le [document général présentant Tkinter](https://trieles.gitlab.io/nsi/2-tkinter/#animer-mieux-avec-la-methode-after){target='__blank' rel='noopener'}. Sinon, cliquez sur le lien précédent
pour en apprendre davantage !

On va donc exploiter cette méthode dans le code de notre fonction `action`,
afin que la fenêtre principale de notre application l'appelle périodioquement,
toutes les 25 ms ici. On obtient alors le script `animation4.py` dont le
code figure ci-dessous.

```python
from tkinter import *

touches = set()

def enfoncer(evt):
    touches.add(evt.keysym)
    action()

def relacher(evt):
    if evt.keysym in touches:
        touches.remove(evt.keysym)

def action():
    if "Left" in touches:
        c.move(r, -20, 0)
    if "Right" in touches:
        c.move(r, 20, 0)
    if "Up" in touches:
        c.move(r, 0, -20)
    if "Down" in touches:
        c.move(r, 0, 20)
    f.after(25, action)

f = Tk()
c = Canvas(f, bg='dark grey', width=800, height=600)
r = c.create_rectangle(350, 350, 450, 450, fill="red", width=10, outline="blue")
c.pack()
f.bind('<KeyPress>', enfoncer)
f.bind('<KeyRelease>', relacher)
f.mainloop()
```

**Observations**

Soyez circonspects en appuyant sur une touche de direction : effectuez
un seul appui bref, puis un autre en sens contraire. Puis recommencez,
et encore... 

C'est formidable : le carré bouge instantanément, sans temporisation aucune !
Mais il y a un mais : vous observerez également que le carré se déplace
de plus en plus vite ! Et même si on se contente d'appuis extrêmement brefs,
rien à faire : il accélère toujours !

Mais pourquoi ? C'est simple : à chaque appui sur une touche, la méthode
`after` est appelée. Comme elle n'est jamais stoppée ensuite, les appels
se multiplient : au lieu de n'être appelée toutes les 25 millisecondes,
la fonction `action` est en fait appelée bien plus souvent !

D'ailleurs, vous pouvez vous contenter d'appuyer de nombreuses fois sur
la barre d'espacement du clavier : cet appui n'occasionne aucun changement
de l'affichage, mais déclenche néanmoins le processus précédemment décrit.
Si maintenant vous appuyez **très brièvement** sur une flèche de direction,
vous constaterez que le carré démarrera « en trombe » :sweat_smile: !


### On touche au but !

Pour éviter le phénomène précédent, la solution est toute simple : **il suffit
d'appeler une et une seule fois la fonction `action`**, dès la fin du code
construisant l'interface de l'application. Ainsi, on n'aura bien qu'un seul
cycle d'appels de `action` qui se répètera toutes les 25 ms...

On obtient le script final `animation5.py`.

```python
from tkinter import *

touches = set()

def enfoncer(evt):
    touches.add(evt.keysym)

def relacher(evt):
    if evt.keysym in touches:
        touches.remove(evt.keysym)

def action():
    if "Left" in touches:
        c.move(r, -20, 0)
    if "Right" in touches:
        c.move(r, 20, 0)
    if "Up" in touches:
        c.move(r, 0, -20)
    if "Down" in touches:
        c.move(r, 0, 20)
    f.after(25, action)

f = Tk()
c = Canvas(f, bg='dark grey', width=800, height=600)
r = c.create_rectangle(350, 350, 450, 450, fill="red", width=10, outline="blue")
c.pack()
action()
f.bind('<KeyPress>', enfoncer)
f.bind('<KeyRelease>', relacher)
f.mainloop()
```

### Pourquoi `event=None` en paramètre d'une fonction associée à un évènement ?

Une fonction attachée à un événement lié au clavier (ou à la souris) reçoit
**obligatoirement** un paramètre, dans lequel on trouvera des informations
liées à cet événement (dans les codes précédents, le paramètre s'appelle
`evt`). *A contrario*, une fonction appelée depuis un menu Tkinter n'a pas
cette contrainte.

**Comment faire pour qu'une même fonction puisse, par exemple, être appelée
aussi bien par un clic dans un menu _et_ par un raccourci clavier (par
exemple pour démarrer une nouvelle partie d'un jeu) ?**

C'est simple : on affecte la valeur par défaut `None` au paramètre, et on
teste dans le corps de la fonction la valeur de ce paramètre, pour agir
en conséquence :

```python
def jouer(evt=None):
    if evt is None:
        ...
    else:
        ...
```


## Un exemple (semi-)complet : Pyng-Pong !

Étudiez attentivement le code ci-dessous, il y a de nombreuses choses à
comprendre (les menus, les raccourcis-clavier, etc.) ! Il y a également
des choses qu'il faudrait *vraiment* améliorer, vous vous en rendrez compte
en essayant le jeu :wink: (typiquement pour le rebond de la balle sur la
raquette, vous devriez jouer un peu avec les épaisseurs, et voir ce qu'il
se passe pour les collisions proches du bord...). 

Voici une petite animation montrant le résultat obtenue.

![](images/pyng-pong.gif){width=400}

```python
--8<--- "docs/codes/tkinter-pour-jouer/pyng_pong.py"
```



[swinnen]: http://inforef.be/swi/python.htm
[tkinter-doc]: http://tkinter.fdex.eu/ "Documentation de Tkinter traduite en français"
[grid-forget]: http://tkinter.fdex.eu/doc/gp.html#w.grid_forget
[pack-forget]: https://waytolearnx.com/2020/07/comment-afficher-ou-masquer-un-widget-tkinter-python.html
[stack-widget-forget]: http://stackoverflow.com/questions/3819354/in-tkinter-is-there-any-way-to-make-a-widget-not-visible
[canvas]: http://tkinter.fdex.eu/doc/caw.html "Widget Canvas de Tkinter (canevas, pour dessiner)"
[tkinter-canvas-items]: http://tkinter.fdex.eu/doc/caw.html#identification-des-items-graphiques "Les élements graphiques du Canvas de Tkinter"
[tkinter-canvas-items-id]: http://tkinter.fdex.eu/doc/caw.html#canvasidnum "Identifier un objet graphique du Canvas de Tkinter"
[tkinter-canvas-tag_lower]: http://tkinter.fdex.eu/doc/caw.html\#Canvas.tag_lower "Gérer la superposition d'éléments graphiques dans un Canvas"
[tkinter-canvas-tag_bind]: http://tkinter.fdex.eu/doc/caw.html#Canvas.tag_bind "Associer un tag à un élément graphique du Canvas"
[tkinter-evts]: http://tkinter.fdex.eu/doc/evt.html#sequence-d-evenements
[tkinter-vars]: http://tkinter.fdex.eu/doc/ctrvar.html
[bounding-box]: https://en.wikipedia.org/wiki/Minimum_bounding_box "Qu'est-ce qu'une bounding box ?"
[tuto-tkinter-developpez.com]: https://tarball69.developpez.com/tutoriels/python/isn-passe-ton-faq/#LIII-C-8 "Un didacticiel sur Tkinter du site developpez.com"
[detection-collision]: https://fr.wikipedia.org/wiki/D%C3%A9tection_de_collision#Jeux_vid%C3%A9o
[stack-oy-bas]: https://gamedev.stackexchange.com/questions/83570/why-is-the-origin-in-computer-graphics-coordinates-at-the-top-left
[analog-tv]: https://en.wikipedia.org/wiki/Analog_television#Displaying_an_image
[ordinateur]: https://fr.wikipedia.org/wiki/Ordinateur
[konrad-zuse]: https://fr.wikipedia.org/wiki/Ordinateur#Ann%C3%A9es_1930
[entrelacement]: https://fr.wikipedia.org/wiki/Entrelacement_(vid%C3%A9o)
[stack-get-widget-size]: http://stackoverflow.com/questions/3950687/how-to-find-out-the-current-widget-size-in-tkinter
[stack-canvas-resize]: http://stackoverflow.com/questions/22835289/how-to-get-tkinter-canvas-to-dynamically-resize-to-window-width
[pygame]: https://www.pygame.org/news "PyGame : le site"
[pygame-wikipedia]: https://fr.wikipedia.org/wiki/Pygame "PyGame : présentation sur Wikipedia"
