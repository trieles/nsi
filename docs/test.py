## coding: utf8

from pathlib import Path
import os, re, shutil, shlex, subprocess
# ~ from MediaInfo import MediaInfo

EXTs = [ "*.mp4" ]
# ~ EXTs = [ "*.mp4", "*.m4v", "*.mkv", "*.wmv", "*.avi" ]
# ~ BASE = "/home/tley/TMP/"
BASE = "."
ERROR = os.path.join(BASE, "ERROR")
if not os.path.exists(ERROR):
    os.makedirs(ERROR)
