#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Ce programme est prévu pour enfouir une image en noir et blanc (1 bit)
# au sein d'une image en niveau de gris (256 nuances), chaque image étant
# enregistrée au format PNG. Les 2 images doivent avoir la même taille.
# Il FAUT éviter la présence d'un canal alpha (transaparence) dans les
# images !

from PIL import Image

conteneur = Image.open('montagne-800x600.png')
secret = Image.open('carte-800x600.png')
sortie = "montagne-800x600-cachant-mieux-carte.png"

pixels_conteneur = conteneur.load()     # On charge les pixels du conteneur.
pixels_secret = secret.load()           # Puis ceux de l'image à cacher.
largeur, hauteur = conteneur.size       # On récupère les dimensions.

for i in range(largeur):      # On parcourt les pixels des images de façon
    for j in range(hauteur):  # naturelle - c'est facile ici, grâce à Pillow.
        pxc = pixels_conteneur[i,j] # Valeur d'un pixel du conteneur.
        pxs = pixels_secret[i,j]    # Pixel correspondant de l'image à cacher
        pxn = pxc - pxc % 2 + pxs   # On altère le bit de poids faible.
        pixels_conteneur[i,j] = pxn # On mémorise la nouvelle valeur.

conteneur.save(sortie)  # Écriture de l'image de sortie à partir du tableau
# de pixels modifiés de sorte à camoufler l'image secrète : c'est tout !
