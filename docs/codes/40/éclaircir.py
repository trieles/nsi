# Ce programme travaille à partir d'une image PGM dotée d'un commentaire
# dans l'entête et dont chaque ligne contient un et un seul code !
fichier_src = "src.pgm" # Fichier-image de départ (chemin à préciser).
fichier_dst = "dst.pgm" # Fichier-image de sortie (chemin à préciser).

def éclaircir_par_multiplication(valeur, coeff, valeur_max=255):
    assert 1 < coeff, "Il faut un coefficient (multiplicateur) > 1"
    return min(int(valeur * coeff), valeur_max)

def éclaircir_par_addition(valeur, coeff, valeur_max=255):
    assert 0 < coeff, "Il faut ajouter une quantité > 0"
    return min(int(valeur - coeff), valeur_max)

with open(fichier_src, "r") as src, open(fichier_dst, "w") as dst:
    # 1ère ligne : on suppose qu'elle contient le type du fichier et
    src.readline()      # on "consomme" la ligne sans vérification.
    dst.write("P2\n")   # On définit le type de l'image produite.
    
    # 2e ligne : un commentaire qu'on ignore en « consommant » la ligne.
    src.readline()
    
    ligne_src = src.readline()  # 3ème ligne : la taille de l'image.
    dst.write(ligne_src)        # On la recopie vers la destination.
    
    ligne_src = src.readline()  # 4ème ligne : la finesse du codage.
    valeur_max = int(ligne_src.strip()) # On récupère cette valeur !
    dst.write(ligne_src)        # On la copie aussi vers la destination.

    pas_fini = True
    while pas_fini:     # Début du traitement des données de l'image.
        ligne_src = src.readline()   # Lecture de la ligne courante.
        if ligne_src == "" : # On est à la fin du fichier : on quitte !
            pas_fini = False
        else:
            valeur_src = int(ligne_src.strip()) # Nettoyage & conversion.
            valeur = éclaircir_par_multiplication(valeur_src, 2, valeur_max)
            ligne_dst = "{}\n".format(valeur)   # /!\ Saut de ligne final
            dst.write(ligne_dst)        # Écriture effective de la ligne.
