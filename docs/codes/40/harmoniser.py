#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from pprint import pprint

DEBUG = False

# ~ fichier_src = "e.pbm"
# ~ fichier_src = "e.pgm"
# ~ fichier_src = "e-bis.pbm"
# ~ fichier_src = "e-bis.pgm"
fichier_src = "e-bad.pgm"

def est_utile(ligne):
    """Renvoie True si la ligne est utile, False sinon.
    Cette dernière situation correspond à une ligne qui ne contient que
    des espaces, ou présentant un caractères # après des espaces."""
    return len(ligne) > 0 and ligne[0] != "#"   # Évaluation paresseuse, ici

def ecrire_valeur(matrice, valeur, pos, largeur, hauteur):
    x = pos % largeur
    y = pos // largeur
    try:
        matrice[y][x] = int(valeur)
    except:
        return False
    return True

def ouvre_pgm(fichier):
    """Ouvre un fichier PBM ou PGM encodé ASCII et renvoie les données qui le
    composent.
    
    Paramètre : une chaîne de caractères désignant le nom d'une image PxM.
    
    Valeur de retour : un dictionnaire dont les clefs seront "type",
    "largeur", "hauteur", "valeur_max", "données" et "statut.
    - La clef "type" correspondra une chaîne de caractères (ex. : "P1").
    - Les clefs "largeur" et "hauteur" correspondront à des entiers.
    - La clef "valeur_max" correspond à la valeur maximale pour un pixel.
      Elle vaut 1 si on est en PBM...
    - La clef "données" correspondra à une matrice (liste de listes)
    contenant les valeurs de chaque pixel.
    - La clef "statut" est un booléen qui indique que le parsing du fichier
    s'est bien passé (ou pas...)
    """
    dico = { "type": "", "largeur": 0, "hauteur": 0, "valeur_max": None,
             "données": [], "statut": None }
    with open(fichier, "r") as src:
        type_pxm = ""           # Le type de l'image
        numero_ligne = 0        # Numéro de ligne dans le fichier
        nb_lignes_utiles = 0    # Le nombre de lignes non vides ni commentaires
        pos = 0                 # La position dans le flux de valeurs des pixels
        il_reste_des_lignes = True  # FIXME : vérfier que le fichier n'est pas vide !
        matrice = []
        valeur_max = None
        while il_reste_des_lignes:
            numero_ligne += 1
            ligne = src.readline()
            if ligne == "":     # Fin du fichier : ligne vide ("\n" != vide).
                il_reste_des_lignes = False
            else:
                ligne = ligne.strip()  # Virer les « espaces » (début & fin).
                if est_utile(ligne):
                    # On a une ligne utile
                    nb_lignes_utiles += 1
                    if ligne in [ "P1", "P2", "P3"]:    # Mieux : ligne[0] == "P"
                        # On a le type de l'image
                        type_pxm = ligne
                        dico["type"] = type_pxm
                        if type_pxm == "P1":
                            dico["valeur_max"] = 1
                    else:
                        # On a soit la taille, soit la valeur max, soit des données.
                        portions_ligne = ligne.split()   # On coupe sur " "
                        nb_portions = len(portions_ligne)
                        if nb_portions == 2 and nb_lignes_utiles == 2:
                            # On a la taille
                            try:
                                # Tentative de récupération de la largeur et de
                                # la hauteur de l'image en cours de traitement.
                                largeur, hauteur = map(int, portions_ligne) 
                                dico["largeur"] = largeur
                                dico["hauteur"] = hauteur
                                matrice = [ [ None for x in range(largeur)] for y in range(hauteur) ]
                            except:
                                if DEBUG: print("Dimensions incorrectes dans {} : '{}' (ligne {})".format(fichier, ligne, numero_ligne))
                                il_reste_des_lignes = False
                                break
                        elif nb_portions == 1 and nb_lignes_utiles == 3 and type_pxm != "P1":
                            # On a normalement la valeur maximale codant un pixel, ici.
                            try:
                                # Tentative de récupération de la valeur maximale.
                                # codant un pixel de l'image en cours de traitement.
                                valeur_max = int(portions_ligne[0])
                                dico["valeur_max"] = valeur_max
                            except:
                                if DEBUG: print("Valeur maximale incorrecte dans {} : '{}' (ligne {})".format(fichier, ligne, numero_ligne))
                                il_reste_des_lignes = False
                                break
                        else:
                            # On a des données : il faut les parcourir, et les mettre
                            # au bon endroit de la matrice.
                            val = ""
                            if DEBUG: print("=== Ligne n°{}".format(nb_lignes_utiles), ": '"+ligne+"' ===")
                            for i in range(len(ligne)):
                                car = ligne[i]
                                if DEBUG: print("  '"+car+"'", "en position", i, "| val =", "'"+val+"'")
                                # On parcourt chaque caractère de la ligne. Il faut les assembler
                                # de sorte à reconstituer chacune des valeurs codant un pixel.
                                if car.strip() == "":       # Un espace ou apparenté
                                    if type_pxm != "P1":    # Fin de valeur ou succession « d'espaces » ?
                                        if val != "":       # Si doit vraiment écrire quelque chose, on le fait.
                                            if DEBUG: print("  --> Écriture de ", "'"+val+"'")
                                            if ecrire_valeur(matrice, val, pos, largeur, hauteur):
                                                pos += 1        # On passe à la valeur / position suivante
                                                val = ""        # On réinitialise 'val' à la ""
                                            else:
                                                if DEBUG: print("Valeur incorrecte dans {} : '{}' (ligne {})".format(fichier, ligne, numero_ligne))
                                                if DEBUG: print("'"+val+"'")
                                                dico["statut"] = False
                                                il_reste_des_lignes = False
                                                break
                                    #else:   # On a un espace en mode P1, c'est donc un caractère parasite
                                    #        pass
                                elif car in "0123456789":   # 'car' représente un (morceau) de valeur.
                                    # Si type P1, alors un morceau EST une valeur (mais il faut que le 'car' soit OK !).
                                    if type_pxm == "P1":
                                        if car == "0" or car == "1":
                                            if DEBUG: print("  --> Écriture de ", "'"+car+"'", "pos", pos)
                                            if ecrire_valeur(matrice, car, pos, largeur, hauteur):
                                                # On pourrait penser qu'il n'y a pas besoin de test ici !
                                                # En effet, on a vérifié avant la valeur de 'car'...
                                                # Pourtant si jamais il y a plus de valeurs dans le fichier
                                                # image que la taille définie ne l'autorise, il y a un souci...
                                                pos += 1
                                            else:
                                                if DEBUG: print("Fichier {} : soucis avec la valeur '{}' (ligne {}, valeur n°{} / {})".format(fichier, ligne, numero_ligne, pos+1, largeur*hauteur))
                                                dico["statut"] = False
                                                il_reste_des_lignes = False
                                                break
                                        else:
                                            if DEBUG: print("Valeur incorrecte dans {} : '{}' (ligne {})".format(fichier, ligne, numero_ligne))
                                            dico["statut"] = False
                                            il_reste_des_lignes = False
                                            break
                                    else:                   # Si on n'est pas en type P1, alors 'car' est vraiment
                                        val += car          # un morceau d'une valeur et on complète 'val'...
                                        # Remarque : c'est lent, de procéder ainsi, car on recrée une nouvelle
                                        # chaîne de caractères à chaque fois !
                                        if DEBUG: print("  Valeur en cours :", val)
                                else:   # On a un caractère incorrect... On actualise le statut.
                                    if DEBUG: print("Valeur incorrecte dans {} : '{}' (ligne {})".format(fichier, ligne, numero_ligne))
                                    if DEBUG: print("'"+val+"'")
                                    dico["statut"] = False
                                    il_reste_des_lignes = False
                                    break

                            # On a fini une ligne.
                            if DEBUG: print("  ** Fin de la ligne !")
                            if val != "":   # A-t-on vraiment quelque chose à écrire ?
                                if DEBUG: print("  --> Écriture de ", "'"+val+"'")
                                if ecrire_valeur(matrice, val, pos, largeur, hauteur):
                                    pos += 1        # On passe à la valeur / position suivante.
                                    val = ""        # On prépare l'accueil de la valeur suivante.
                                else:
                                    if DEBUG: print("Valeur incorrecte dans {} : '{}' (ligne {})".format(fichier, ligne, numero_ligne))
                                    if DEBUG: print("'"+val+"'")
                                    dico["statut"] = False
                                    il_reste_des_lignes = False
                                    break

        dico["données"] = matrice
        
        # Vérifications supplémentaires de la validité du parsing
        # Si on a un fichier PBM (P1), on a valeur_max qui est None.
        if dico["type"] == "P1":
            if valeur_max == None:  # Tout va bien ici
                valeur_max = 1
            else:                   # Là il y a un problème
                dico["statut"] = False
        # On parcourt l'ensemble des valeurs pour vérifier qu'il ne reste
        # aucun None dans la matrice d'origine, et qu'elles sont toutes
        # inférieures à valeur_max.
        if dico["statut"] is None:
            valeurs_OK = True
            for ligne in dico["données"]:
                for valeur in ligne:
                    if valeur is None or valeur > valeur_max:
                        valeurs_OK = False
                        break
            dico["statut"] = valeurs_OK

    return dico
    
pprint(ouvre_pgm(fichier_src))
