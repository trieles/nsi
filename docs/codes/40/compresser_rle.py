# Ce programme travaille à partir d'une image PBM dotée d'un commentaire
# dans l'entête et dont chaque ligne mesure 70 caractères au maximum et
# contient une suite de 1 et de 0 sans espacement pour les séparer.
# ~ fichier_src = "e.pbm"   # Fichier-image de départ (chemin à préciser).
# ~ fichier_dst = "e.pbmz"  # Fichier-image de sortie (chemin à préciser).
fichier_src = "lune-eclipse.pbm"   # Fichier-image de départ.
fichier_dst = "lune-eclipse.pbmz"  # Fichier-image de sortie.

with open(fichier_src, "r") as src, open(fichier_dst, "w") as dst:
    src.readline()      # Ligne 1 (le type), "consommée" sans vérification.
    dst.write("P1z\n")  # On définit le type de l'image produite.
    
    src.readline()      # Ligne 2 (commentaire ignoré en "consommant" la ligne).
    
    ligne_src = src.readline()  # Ligne 3 : les dimensions de l'image.
    dst.write(ligne_src)        # On recopie cette ligne vers la destination.

    pas_fini = True
    while pas_fini:             # Début du traitement des données de l'image.
        ligne_src = src.readline()  # Lecture de la ligne courante.
        if ligne_src == "" :    # On est à la fin du fichier : on quitte !
            pas_fini = False
        else:
            valeur_prec = "0"   # La valeur précédente, "0" initialement.
            occurrences = [0]   # Au début "0" est donc 0 fois dans la ligne.
            for valeur_src in ligne_src.strip():    # Pas de \n final !
                if valeur_src != valeur_prec:       # Change-t-on de valeur ?
                    valeur_prec = valeur_src        # Oui ! On a une alternance
                    occurrences.append(1)           # d'où 1 nouvelle valeur.
                else:
                    occurrences[-1] += 1    # Si non, la valeur en cours est
                    # présente une fois de plus, d'où l'incrémentation.
            # Parcours de ligne fini: écriture du résultat dans fichier_dst.
            # Pour créer une chaîne, le plus efficace est d'utiliser join()
            # qui prend une liste de... chaînes ! On crée cette liste par
            # compréhension, c'est ce qu'il y a de plus EFFICACE !
            ligne_dst = ",".join(str(o) for o in occurrences)
            dst.write(ligne_dst)  # Écriture effective de la ligne.
            dst.write("\n")       # /!\ Saut de ligne final nécessaire !
