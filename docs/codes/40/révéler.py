#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ATTENTION, ce programme est prévu pour travailler à partir d'une image
# PGM générée par GIMP. Celui-ci semble toujours enregistrer chaque
# valeur codant un pixel sur une ligne à part.

fichier_conteneur = "montagne-cachant-carte.pgm"
fichier_dst = "carte-décachée.pbm"  # Un PBM : pas envie de m'embêter !

with open(fichier_conteneur, "r") as conteneur, \
     open(fichier_dst, "w") as dst:
    conteneur.readline()    # 1ère ligne : le type (on consomme).
    dst.write('P1\n')       # On écrit le type P1 (cf. ligne 9).
    
    # 2ère ligne : GIMP met toujours un commentaire, mais on a décidé de
    # l'ignorer lors de la création de l'image qui en camoufle une autre.
    # On a donc ici la taille de l'image, qu'il faut absolument recopier !
    ligne_conteneur = conteneur.readline()
    dst.write(ligne_conteneur)
    
    # 3ème ligne : la finesse du codage. On NE recopie PAS puisqu'on a
    # décidé de créer un fichier de type P1 (une image PBM, en 1 bit).
    conteneur.readline()    # Donc on consomme la ligne...

    # On attaque le traitement des données de chaque image...
    while True:
        ligne_conteneur = conteneur.readline()
        if ligne_conteneur == "" :
            break   # On est à la fin des fichiers : on quitte !
        else :
            # On extrait les 2 derniers caractères de la ligne
            ligne_dst = ligne_conteneur[-2:]
            dst.write(ligne_dst)
