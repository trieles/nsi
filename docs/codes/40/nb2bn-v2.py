# Ce programme est prévu pour travailler à partir d'une image PGM dont
# chaque ligne contient un et un seul code !
fichier_src = "src.pgm" # Fichier-image de départ (chemin à préciser).
fichier_dst = "dst.pgm" # Fichier-image de sortie (chemin à préciser).

def est_commentaire(ligne):
    """Renvoie True si ligne commence par le caractère "#", False sinon.
    """
    if ligne == "": # Mieux vaut distinguer le cas de la ligne vide !
        return False
    else:
        return ligne[0] == "#"

with open(fichier_src, "r") as src, open(fichier_dst, "w") as dst:
    # 1ère ligne : on suppose qu'elle contient le type du fichier et
    ligne_src = src.readline()
    if ligne_src == "P2\n":     # Vérification du type d'image.
        dst.write(ligne_src)    # On recopie le type de l'image.
        
        ligne_src = src.readline()        # La ligne suivante peut être
        if est_commentaire(ligne_src):    # un commentaire qu'on ignore
            ligne_src = src.readline()    # en passant à la ligne d'après.
        
        # ligne_src doit normalement contenir la taille de l'image.
        dst.write(ligne_src)        # On la recopie vers la destination.
        
        ligne_src = src.readline()  # La finesse du codage.
        dst.write(ligne_src)        # On la copie aussi vers la destination.

        pas_fini = True
        while pas_fini:     # Début du traitement des données de l'image.
            ligne_src = src.readline()   # Lecture de la ligne courante.
            if ligne_src == "" : # On est à la fin du fichier : on quitte !
                pas_fini = False
            elif not est_commentaire(ligne_src):
                valeur_src = int(ligne_src.strip()) # Nettoyage & conversion.
                if valeur_src == 255:
                    ligne_dst = "0\n"       # /!\ Saut de ligne \n final /!\
                else :
                    ligne_dst = "255\n"     # /!\ Saut de ligne \n final /!\
                dst.write(ligne_dst)        # Écriture effective de la ligne.
