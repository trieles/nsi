#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Attention, ce programme est prévu pour travailler à partir d'une image PGM générée par
# Gimp. Celui-ci, du moins dans ma version (2.8.16), met chaque valeur sur une ligne à
# part

fichier_src = "src.pgm"  # Fichier d'origine.
fichier_dst = "dst.pzm"  # Fichier produit en sortie.

def fabrique_liste(fichier):
    """Prend en paramètre une image PGM (niveaux de gris) et renvoie :
    - une liste contenant les valeurs de chaque pixel ;
    - la largeur de cette matrice (et donc de l'image) ;
    - la hauteur de cette matrice ;
    - la valeur maximale codant le blanc pur.
    """
    with open(fichier_src, "r") as src:
        # 1ère ligne : le type (on suppose que tout est OK et conforme : aucun test !)
        src.readline()      # On "consomme" la ligne sans vérification.
        
        # 2ère ligne : Gimp met toujours un commentaire. On décide de l'ignorer...
        # Il faut pourtant "consommer" cette ligne pour passer à la suivante !
        src.readline()
        
        # 3ème ligne : la taille de l'image (on suppose que les dimensions sont
        # égales, là encore aucun test : tout est censé être OK !).
        ligne_src = src.readline()
        largeur, hauteur = ligne_src.split()    # On a une liste de 2 chaînes ici.
        # Bien lire la doc de la méthode split ! Dans Idle, faire : help("".split)
        largeur = int(largeur)                  # Conversion en entier.
        hauteur = int(hauteur)
        
        # 4ème ligne : la finesse du codage : on ignore !
        valeur_max = int(src.readline())

        # On crée la liste qui va accueillir l'ensemble des valeurs codant
        # le niveau de gris de chaque pixel.
        pixels_image = [ ]

        # On attaque la récupération des données de l'image...
        while True:
            ligne_src = src.readline()
            if ligne_src == "":  # On est à la fin des fichiers : on quitte !
                break
            else:
                valeur = int(ligne_src)     # La valeur codant le pixel.
                pixels_image.append(valeur)
    return pixels_image, largeur, hauteur, valeur_max


def compresser(liste, taille):
    compression = [ ]
    pos = 0
    while pos < taille:
        valeur = liste[pos]
        compteur = 1
        ok = False if pos==taille-1 else True
        while ok:
            valeur_suivante = liste[pos + compteur]
            if valeur_suivante == valeur:
                compteur += 1
                if compteur == taille-1:
                    ok = False
            else:
                ok = False
        print("Valeur", valeur, "présente", compteur, "fois")
        compression.append((valeur, compteur))
        pos += 1
    return compression

def enregistre_image_PGM(fichier_dst, liste, largeur, hauteur, valeur_max):    
    with open(fichier_dst, "w") as dst:
        dst.write('P2z\n')               # On définit le type de l'image produite.
        dst.write("# Image compressée\n")  # Un commentaire, pour rester cohérent.
        dst.write("{} {}\n".format(largeur, hauteur))   # La taille de l'image.
        dst.write("{}\n".format(valeur_max))    # La valeur codant le blanc.
        for couple in liste:
            dst.write("{},{}\n".format(couple[0], couple[1]))    # La  valeur du pixel.

image, L, H, valeur_blanc = fabrique_liste(fichier_src)
print("Image d'origine chargée. Début de la compression...")
image_compressee = compresser(image, L*H)
print("Compression effectuée. Enregistrement...")
enregistre_image_PGM(fichier_dst, image_compressee, L, H, valeur_blanc)
print("L'image compressée", fichier_dst, "est créée !")

