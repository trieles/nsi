# Ce programme est prévu pour travailler à partir d'une image PGM dont
# chaque ligne contient un et un seul code !
fichier_src = "src.pgm" # Fichier-image de départ (chemin à préciser).
fichier_dst = "dst.pgm" # Fichier-image de sortie (chemin à préciser).

with open(fichier_src, "r") as src, open(fichier_dst, "w") as dst:
    # 1ère ligne : on suppose qu'elle contient le type du fichier et
    src.readline()      # on "consomme" la ligne sans vérification.
    dst.write("P2\n")   # On définit le type de l'image produite.
    
    # 2ème ligne : un logiciel comme GIMP y met toujours un commentaire.
    # On doit l'ignorer, mais il faut tout de même « consommer » cette
    # ligne pour pouvoir lire la suivante ! Il serait toutefois sain de
    # vérifier qu'il y a bien un commentaire ici, sinon ça va coincer...
    src.readline()
    
    ligne_src = src.readline()  # 3ème ligne : la taille de l'image.
    dst.write(ligne_src)        # On la recopie vers la destination.
    
    ligne_src = src.readline()  # 4ème ligne : la finesse du codage.
    dst.write(ligne_src)        # On la copie aussi vers la destination.

    pas_fini = True
    while pas_fini:     # Début du traitement des données de l'image.
        ligne_src = src.readline()   # Lecture de la ligne courante.
        if ligne_src == "" : # On est à la fin du fichier : on quitte !
            pas_fini = False
        else:
            valeur_src = int(ligne_src.strip()) # Nettoyage & conversion.
            if valeur_src == 255:
                ligne_dst = "0\n"       # /!\ Saut de ligne \n final /!\
            else :
                ligne_dst = "255\n"     # /!\ Saut de ligne \n final /!\
            dst.write(ligne_dst)        # Écriture effective de la ligne.
