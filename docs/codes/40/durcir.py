#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Attention, ce programme est prévu pour travailler à partir d'une image PGM générée par
# Gimp. Celui-ci, du moins dans ma version (2.8.16), met chaque valeur sur une ligne à
# part

fichier_src = "src.pgm"  # Fichier d'origine.
fichier_dst = "dst.ppm"  # Fichier produit en sortie.

def fabrique_matrice(fichier):
    """Prend en paramètre une image PGM (niveaux de gris) et renvoie :
    - une matrice (liste de liste) contenant les valeurs de chaque pixel ;
    - la largeur de cette matrice (et donc de l'image) ;
    - la hauteur de cette matrice ;
    - la valeur maximale codant le blanc pur.
    """
    with open(fichier_src, "r") as src:
        # 1ère ligne : le type (on suppose que tout est OK et conforme : aucun test !)
        src.readline()      # On "consomme" la ligne sans vérification.
        
        # 2ère ligne : Gimp met toujours un commentaire. On décide de l'ignorer...
        # Il faut pourtant "consommer" cette ligne pour passer à la suivante !
        src.readline()
        
        # 3ème ligne : la taille de l'image (on suppose que les dimensions sont
        # égales, là encore aucun test : tout est censé être OK !).
        ligne_src = src.readline()
        largeur, hauteur = ligne_src.split() # On a une liste de 2 chaînes ici.
        # Bien lire la doc de la méthode split ! Dans Idle, faire : help("".split)
        largeur = int(largeur)                  # Conversion en entier.
        hauteur = int(hauteur)
        
        # 4ème ligne : la finesse du codage : on ignore !
        valeur_max = int(src.readline())

        # On crée une structure capable d'accueillir l'ensemble des valeurs codant
        # le niveau de gris de chaque pixel : une liste de liste.
        pixels_image = [ [ 0 for i in range(largeur)] for j in range(hauteur) ]

        # On attaque le traitement des données de chaque image...
        compteur = 0
        while True:
            ligne_src = src.readline()
            if ligne_src == "":  # On est à la fin des fichiers : on quitte !
                break
            else:
                i = compteur % largeur      # « L'abscisse » du pixel.
                j = compteur // largeur     # « L'ordonnée » du pixel.
                valeur = int(ligne_src)     # La valeur codant le pixel.
                pixels_image[j][i] = valeur # Piège ici : d'abord l'ordonnée !
            compteur += 1
    return pixels_image, largeur, hauteur, valeur_max


def voisinage(x, y, distance, i_max, j_max):
    """Donne les coordonnées de toutes les cases voisines d'une case d'un matrice
    (voisines au sens : à une certaine distance).
    
    Paramètres :
     - x puis y (l'abscisse puis l'ordonnée de la case cible) ;
     - la distance (si elle vaut 2, on regardera une zone qui s'étend
       2 pixels à gauche, à droite, au-dessus et au-dessous du pixel concerné) ;
     - les valeurs maximales admissibles en abscisse puis en ordonnée.
    
    Valeur de retour : une liste de tuples correspondant aux coordonnées
    de chaque case voisine de la case dont les coordonnées ont été passées
    en paramètres.
    """
    voisinage = [(x+i, y+j) for i in [ _ for _ in range(-distance, distance+1)]
                            for j in [ _ for _ in range(-distance, distance+1)]
                 if not i == j == 0     # On ne regarde pas la case courante
                 and 0 <= x+i < i_max and 0 <= y+j < j_max] # On ne déborde pas
    return voisinage


def durcir(matrice, rayon, largeur, hauteur, valeur_blanc):
    pixels_image_dure = [ [ 0 for i in range(largeur)] for j in range(hauteur) ]
    for j in range(hauteur):
        print("Ligne {} / {} traitée".format(j, hauteur))
        for i in range(largeur):
            nb_pixels = 0
            somme = 0
            coords_pixels_voisins = voisinage(i, j, rayon, largeur, hauteur)
            # ~ print(coords_pixels_voisins)
            for x, y in coords_pixels_voisins:
                nb_pixels += 1
                somme += matrice[y][x]
            valeur_moyenne = somme / nb_pixels
            tmp = matrice[j][i] - valeur_moyenne
            if tmp < 0:
                nouvelle_valeur = max(matrice[j][i] - abs(tmp) * .8, 0)
            else:
                nouvelle_valeur = min(matrice[j][i] + tmp * 1.2, valeur_blanc)
            pixels_image_dure[j][i] = int(nouvelle_valeur)
    return pixels_image_dure

def enregistre_image_PGM(fichier_dst, matrice, largeur, hauteur, valeur_max):    
    with open(fichier_dst, "w") as dst:
        dst.write('P2\n')               # On définit le type de l'image produite.
        dst.write("# Image durcie\n")  # Un commentaire, pour rester cohérent.
        dst.write("{} {}\n".format(largeur, hauteur))   # La taille de l'image.
        dst.write("{}\n".format(valeur_max))    # La valeur codant le blanc.
        for j in range(hauteur):
            for i in range(largeur):
                dst.write("{}\n".format(matrice[j][i])) # La  valeur du pixel.


image, L, H, valeur_blanc = fabrique_matrice(fichier_src)
print("Image d'origine chargée. Début du durcissement...")
image_dure = durcir(image, 2, L, H, valeur_blanc)
print("Floutage effectué. Enregistrement...")
enregistre_image_PGM(fichier_dst, image_dure, L, H, valeur_blanc)
print("L'image durcie", fichier_dst, "est créée !")

