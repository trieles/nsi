# Ce programme travaille à partir d'une image PGM dotée d'un commentaire
# dans l'entête et dont chaque ligne contient un et un seul code !
fichier_src = "src.pgm" # Fichier-image de départ (chemin à préciser).
fichier_dst = "dst.pgm" # Fichier-image de sortie (chemin à préciser).

SEUIL = 0.5 # Si la valeur maximale des codes est 255, le seuil entre
            # le noir et le blanc serait int(0.5*255) = 127.

with open(fichier_src, "r") as src, open(fichier_dst, "w") as dst:
    # 1ère ligne : on suppose qu'elle contient le type du fichier et
    src.readline()      # on "consomme" la ligne sans vérification.
    dst.write("P2\n")   # On définit le type de l'image produite.
    
    # 2e ligne : un commentaire qu'on ignore en « consommant » la ligne.
    src.readline()
    
    ligne_src = src.readline()  # 3ème ligne : la taille de l'image.
    dst.write(ligne_src)        # On la recopie vers la destination.
    
    ligne_src = src.readline()  # 4ème ligne : la finesse du codage.
    valeur_max = int(ligne_src.strip()) # On récupère cette valeur !
    dst.write(ligne_src)        # On la copie aussi vers la destination.
    seuil = int(valeur_max * SEUIL)     # Calcul du seuil.

    pas_fini = True
    while pas_fini:     # Début du traitement des données de l'image.
        ligne_src = src.readline()   # Lecture de la ligne courante.
        if ligne_src == "" : # On est à la fin du fichier : on quitte !
            pas_fini = False
        else:
            valeur_src = int(ligne_src.strip()) # Nettoyage & conversion.
            valeur = valeur_max if valeur_src > seuil else 0    # Sympa !
            ligne_dst = "{}\n".format(valeur)   # /!\ Saut de ligne final
            dst.write(ligne_dst)        # Écriture effective de la ligne.
