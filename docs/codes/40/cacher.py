#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ATTENTION, ce programme est prévu pour travailler à partir d'une image
# PGM générée par GIMP. Celui-ci semble toujours enregistrer chaque valeur
# codant un pixel sur une ligne à part.

fichier_conteneur = "montagne.pgm"                  # Fichier "conteneur"
fichier_a_cacher =  "carte.pgm"                     # Fichier "a_cacher"
fichier_dst =       "montagne-cachant-carte.pgm"    # Fichier "dst"

with open(fichier_conteneur, "r") as conteneur, \
     open(fichier_a_cacher, "r") as a_cacher, \
     open(fichier_dst, "w") as dst:
    # 1ère ligne : le type (on suppose que tout est conforme : aucun test !)
    conteneur.readline()    # Mais il faut tout de même consommer ces lignes
    a_cacher.readline()     # (inutile de les mémoriser, cependant).
    dst.write('P2\n')       # On écrit le type dans le fichier-destination.
    
    # 2ème ligne : GIMP met toujours un commentaire. On l'ignore... Mais
    # là aussi il faut "consommer" cette ligne pour passer à la suivante !
    conteneur.readline()
    a_cacher.readline()
    
    # 3ème ligne : la taille de l'image (on suppose les dimensions égales,
    # là encore on n'effectue aucun test : tout est censé être OK !).
    ligne_conteneur = conteneur.readline()
    a_cacher.readline()     # Inutile de mémoriser !
    dst.write(ligne_conteneur)
    
    # 4ème ligne : la finesse du codage : on recopie !
    ligne_conteneur = conteneur.readline()
    a_cacher.readline()     # Inutile de mémoriser !
    dst.write(ligne_conteneur)

    # On attaque le traitement des données de chaque image...
    while True:
        ligne_conteneur = conteneur.readline()
        ligne_a_cacher = a_cacher.readline()

        if ligne_conteneur == "" and ligne_a_cacher == "" :            
            # On est à la fin des fichiers : on quitte ! Cette façon de
            # faire fera hurler les puriste : tant pis ;-p !
            break
        else :
            # On supprime le "\n" final et le caractère qui le précède dans
            # la valeur de l'image conteneur : c'est l'approche "informatique"
            # qui exploite directement les chaînes de caractères. Oui, je
            # suis fainéant.
            ligne_conteneur = ligne_conteneur[:-2]
            if ligne_a_cacher == "255\n" :
                ligne_dst = ligne_conteneur + "0\n"
            else :
                ligne_dst = ligne_conteneur + "1\n"
            dst.write(ligne_dst)
