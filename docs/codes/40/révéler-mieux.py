#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Ce programme est prévu pour révéler une image en noir et blanc (1 bit)
# cachée au sein d'une image en niveau de gris (256 nuances), l'image
# conteneur étant au format PNG. Une image PNG est générée en sortie...

from PIL import Image

conteneur = Image.open("montagne-800x600-cachant-carte.png")
sortie = "carte-800x600-décachée.png"

pixels_conteneur = conteneur.load()      # On charge les pixels du conteneur

for i in range(conteneur.size[0]):      # On parcourt les pixels de
    for j in range(conteneur.size[1]):  # l'image conteneur.
        pxc = pixels_conteneur[i,j]     # On récupère chaque valeur.
        pxn = 255 - (pxc % 2) * 255     # On extrait le bit de poids faible.
        pixels_conteneur[i,j] = pxn     # On mémorise cette valeur.

conteneur = conteneur.convert('1')      # Conversion de l'image en 1 bit.
conteneur.save(sortie)                  # On crée l'image de sortie à partir
# du tableau de pixels précédemment modifié. Et c'est tout !
