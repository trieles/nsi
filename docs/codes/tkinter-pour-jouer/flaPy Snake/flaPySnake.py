#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from tkinter import *
import random

LARGEUR = 800               # Largeur de la fenêtre
HAUTEUR = 600               # Hauteur de la fenêtre
TAILLE_JOUEUR = 20          # Hauteur du « sprite » (avatar du joueur)
FPS = 50                    # Fréquence de rafraîchissement du jeu (itérations / s)
Dt = int(1000/FPS)          # Intervalle de temps entre chaque image
VITESSE_ASCENSION = 100     # La vitesse ajoutée à chaque pression sur une touche
GRAVITE = 9.81*15           # Valeur de gravité (ajustée pour une jouabilité correcte)

f=Tk()
f.title("FlaPy Snake")
f.geometry("800x600+200+100")

appui = BooleanVar(False)   # Servira à savoir si une touche est pressée ou non
# Attention ! Si on met ici un IntVar, pour des valeurs trop faibles de
# la vitesse, on reste à 0 et rien ne se passe !!!
vitesse = DoubleVar(0.0)    # Pour conserver la vitesse verticale du « sprite »

c = Canvas(f, background="light yellow", width=LARGEUR, height=HAUTEUR)
c.pack()

corps = c.create_rectangle(LARGEUR/2-TAILLE_JOUEUR/2,HAUTEUR/2-TAILLE_JOUEUR/2,
                   LARGEUR/2+TAILLE_JOUEUR/2,HAUTEUR/2+TAILLE_JOUEUR/2,
                   fill="blue", tags=("joueur"))
c.create_polygon(LARGEUR/2+TAILLE_JOUEUR/2,HAUTEUR/2+TAILLE_JOUEUR/2,
                 LARGEUR/2+TAILLE_JOUEUR,HAUTEUR/2,
                 LARGEUR/2+TAILLE_JOUEUR/2,HAUTEUR/2-TAILLE_JOUEUR/2,
                 fill="red", tags="joueur")

#~ s = c.create_polygon(0,10,
    #~ 0,30, 25,50, 40,40, 30,10, 40,8, 48,20, 50,17, 48,10, 40,0, 35,0, 25,0, 20,10, 30,35, 20,40, 0,30,
    #~ fill="red", smooth=1, joinstyle=ROUND)

def enfoncer(event=None):
    #~ print("Touche pressée :", event.keysym)
    appui.set(True)
    
def relacher(event=None):
    #~ print("Touche relachée :", event.keysym)
    appui.set(False)
    
def animer():
    # S'il y a moins de 3 obstacles à la fois à l'écran, on en crée d'autres
    # Il faut au minimum une distance de LARGEUR*2/5 entre chaque obstacle
    if len(c.find_withtag("obstacle")) < 2 and c.find_overlapping(LARGEUR*3/5,0,LARGEUR,HAUTEUR) == ():
        # 3 types d'obstacle : descendant du plafond, montant du sol, ou
        # les deux à la fois ! On laisse aussi la possibilité de ne pas
        # avoir d'obstacle (génération d'un 0)
        nature = random.randint(0,3)
        if nature == 1:
            # Création d'un obstacle « au plafond »
            c.create_rectangle(LARGEUR,0,
                               LARGEUR+random.randint(5,TAILLE_JOUEUR*2), random.randint(TAILLE_JOUEUR*4, HAUTEUR-TAILLE_JOUEUR*4),
                               fill="yellow", tags=("obstacle"))
        elif nature == 2:
            # Création d'un obstacle « au sol »
            c.create_rectangle(LARGEUR,HAUTEUR,
                               LARGEUR+random.randint(5,TAILLE_JOUEUR*2), HAUTEUR-random.randint(TAILLE_JOUEUR*4, HAUTEUR-TAILLE_JOUEUR*4),
                               fill="green", tags=("obstacle"))
        elif nature == 3:
            # Création d'obstacle « au sol et au plafond »
            trou = random.randint(TAILLE_JOUEUR*2,TAILLE_JOUEUR*6)
            yMinTrou = random.randint(TAILLE_JOUEUR*2,int((HAUTEUR-trou)/2))
            c.create_rectangle(LARGEUR,0,
                   LARGEUR+random.randint(5,TAILLE_JOUEUR*2), yMinTrou,
                   fill="orange", tags=("obstacle"))
            c.create_rectangle(LARGEUR,HAUTEUR,
                               LARGEUR+random.randint(5,TAILLE_JOUEUR*2), yMinTrou+trou,
                               fill="orange", tags=("obstacle"))

    v0 = vitesse.get()
    if appui.get():
        # La vitesse verticale « augmente »
        v1 = v0+VITESSE_ASCENSION/10
    else:
        # La vitesse verticale « diminue »
        v1 = v0-GRAVITE*Dt/1000

    # Valeur du déplacement du « sprite » (avatar du joueur)
    Dz = GRAVITE/2*(Dt/1000)**2-v1*Dt/1000
    # Actualisation de la vitesse
    vitesse.set(v1)
    
    # Bouger le « sprite » et les obstacles : le sprite d'abord
    # La variable ok permet de savoir s'il n'y a pas eu de collision, et
    # donc s'il est utile de relancer une boucle d'affichage.
    ok = True
    for i in c.find_withtag("joueur"):
        coordonnees = c.coords(i)
        x1, y1, x2, y2 = c.coords(corps)
        # On teste si l'ordonnée la plus basse est trop faible, si l'ordonnée la
        # plus haute est trop élevée, et s'il y a strictement plus de 2 objets
        # qui se chevauchent au niveau du corps principal du « sprite » (le carré) :
        # le carré et le triangle, OK, mais si un objet du cannevas est également
        # détecté, c'est qu'il y a eu une collision !
        if coordonnees[1] <= Dz or coordonnees[3] >= HAUTEUR-Dz or len(c.find_overlapping(x1, y1, x2, y2))>2:
            #~ print("Sortie du cadre ou collision !")
            ok = False
        c.move(i, 0, Dz)
    # Puis on bouge les obstacles
    for i in c.find_withtag("obstacle"):
        c.move(i, -2, 0)
        # On détruit les objets sortis de la zone d'affichage
        if c.coords(i)[2] <= 0:
            c.delete(i)
        
    # Rappel de la fonction de déplacement en l'absence de collision
    if ok:
        f.after(Dt, animer)

f.bind('<KeyPress>', enfoncer)
f.bind('<KeyRelease>', relacher)
f.after(Dt, animer)

f.mainloop()
