#!/usr/bin/env python3
# -*- coding: utf8 -*-

from tkinter import *

f = Tk()

def pression(event):
    print("Appui sur", event.keysym)

def relachement(event):
    print("Fin de l'appui sur", event.keysym)

f.bind('<KeyPress>', pression)
f.bind('<KeyRelease>', relachement)

f.mainloop()
