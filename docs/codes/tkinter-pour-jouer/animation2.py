#!/usr/bin/env python3
# coding: utf8
from tkinter import *

def action(evt):
    touche = evt.keysym
    if touche == "Left":
        c.move(r, -20, 0)
    elif touche == "Right":
        c.move(r, 20, 0)
    elif touche == "Up":
        c.move(r, 0, -20)
    elif touche == "Down":
        c.move(r, 0, 20)

f = Tk()
c = Canvas(f, bg='dark grey', width=800, height=600)
r = c.create_rectangle(100, 300, 500, 500, fill="red", width=10, outline="blue")
c.pack()
f.bind("<Key>", action)
f.mainloop()

