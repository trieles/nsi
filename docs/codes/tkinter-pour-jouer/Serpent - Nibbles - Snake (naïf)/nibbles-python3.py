#!/usr/bin/env python3
# -*- coding: utf8 -*-

from tkinter import *
from random import randrange
import tkinter.messagebox as messageBox


#####################
# Fonctions annexes #
#####################

# Une boucle pour consommer du temps CPU !
def attendre(t):
    for k in range(t): pass

# No comment...
def apropos():
    messageBox.showinfo("Mon « Nibbles » à moi","Ce bout de code est\ndistribué sous licence\nCreative Commons 3.0\nBY-NC-SA\n\nVersion 1.1 - 12/03/2003\nVersion 1.2 - 20/02/2012\n\nAuteur : Tristan LEY")

# No comment...
def regleDuJeu():
    fenTmp = Toplevel(fen)
    fenTmp.title("Règle de mon « Nibbles » à moi")
    texte = Text(fenTmp, width=55, height=13, relief='ridge', bd=2, wrap='word')
    texte.insert(END, "Dans ce jeu, vous contrôlez une sorte de serpent, representé par une succession de carrés de couleur jaune.\n\n")
    texte.insert(END, "Vous devez toucher les carrés cibles de couleur rouge qui apparaissent dans l'aire de jeu avec la tête de votre serpent, ce qui a pour effet de l'allonger. ")
    texte.insert(END, "Pour vous déplacer, utilisez les flèches de direction ; pour mettre en pause ou reprendre je jeu, pressez la barre d'espacement.\n\n")
    texte.insert(END, "Attention ! Si au cours d'une manœuvre vous venez à percuter la queue de votre serpent, vous avez perdu !")
    texte.pack(fill='both', expand=1)
    Button(fenTmp, text="Fermer", command=fenTmp.destroy).pack()

# Dessin d'un carre...
def carre(x, y, couleur='yellow'):
    global can
    return can.create_rectangle(x, y, x+10, y+10, fill=couleur, width=2)

# Comment demarrer le jeu ?
def demarrer():
    global on, pause
    # Tant que le jeu est en marche (on=1) et qu'il n'est pas en pause (pause=0),
    # on appelle la fonction 'bouge()', qui bouge le serpent et allume les cibles !
    while on and not pause:
        bouge()
 
# Comment quitter le jeu !
def quitter():
    global on
    # D'abord on l'arrete !
    on=0
    # Puis on quitte la fenetre !
    fen.quit()

# Deplacement vers le haut.
def enHaut(event):
    global dx, dy, cd
    # On interdit au serpent de rebrousser chemin en se marchant dessus.
    if on and not pause and not dy and not cd:
        # On met a jour ses decalages en abscisse et en ordonnee.
        dx=0
        dy=-10
        # cd signifie « changement de direction ». Ce drapeau indique qu'un
        # changement de direction est à effectuer.
        cd=1

# Deplacement vers le bas.
def enBas(event):
    global dx, dy, cd
    if on and not pause and not dy and not cd:
        dx, dy, cd = 0, 10, 1

# Deplacement vers la gauche.
def aGauche(event):
    global dx, dy, cd
    if on and not pause and not dx and not cd:
        dx, dy, cd = -10, 0, 1

# Deplacement vers la droite.
def aDroite(event):
    global dx, dy, cd
    if on and not pause and not dx and not cd:
        dx, dy, cd, = 10, 0, 1

# La fonction qui gere les deplacements
def bouge():
    global dx, dy, serpent, h, w, i, n, cibles, crash, on, score, delaiD, cd
    contact = 0
    i += 1
    # S'il n'y a pas (ou plus) de cible, on en cree une !
    if not n:
        afficheCible()
    # Calcul des nouvelles coordonnees de la tete du serpent.
    x, y = (can.coords(serpent[0])[0] + dx) % w, (can.coords(serpent[0])[1] + dy) % h
    # Si un changement de direction a ete demande, il a normalement du etre fait, dans
    # ce cas, on remet 'cd' a 0.
    if cd:
        cd = 0
    # On determine s'il y a collision ou non. Dans ce cas, on passe 'crash' a 1.
    for k in range(3,len(serpent)):
        if x == can.coords(serpent[k])[0] and y == can.coords(serpent[k])[1]:
            crash = 1
            clignoter(k)
    # En cas de crash, on affiche le score et on termine la partie.
    if crash:
        messageBox.showwarning("Ooops ! Crash !","Partie terminee !\nVotre score : " + str(score))
        on = 0
    # S'il n'y a pas de crash, on teste si la cible a ete atteinte.
    if n and x == can.coords(cibles[0])[0] and y == can.coords(cibles[0])[1]:
        # Si c'est le cas, on integre la cible dans le serpent. Pour cela, il faut la changer
        # de couleur, et indiquer quel objet du cannevas est a deplacer dans la structure du
        # serpent. Ici, c'est la cible elle-meme...
        can.itemconfig(cibles[0], fill='yellow')
        aBouger=cibles[0]
        contact = 1
        majScore(contact)
    if not contact:
        # S'il n'y a pas rencontre du serpent avec une cible, on fait passer le dernier carre
        # du serpent en tete. Je *suppose* que cette technique est moins gourmande que celle
        # qui consiste a deplacer chacun des carres du serpent...
        aBouger = serpent[-1]
        can.coords(aBouger, x, y, x+10, y+10)
        serpent = [aBouger] + serpent[:-1]
    else:
        # S'il y a rencontre du serpent avec une cible, on installe tout simplement la cible
        # en tete du serpent. On n'oublie alors pas de supprimer la cible, et d'indiquer que
        # le nombre de cible est desormais 0 !
        serpent = [aBouger] + serpent
        cibles = []
        n = 0
    # On met a jour le cannevas...
    can.update()
    # ...et on patiente un peu !
    attendre(delaiD)
    # Autre facon de faire qui consiste a bouger chaque carre du serpent...
    # A voir laquelle des deux est la plus rapide !
    #aBouger = serpent[-1]
    #can.coords(aBouger, x, y, x+10, y+10)
    #l = len(serpent)-1
    #for i in range(l):
    #    serpent[l-i] = serpent[l-i-1]
    #serpent[0] = aBouger

# Une fonction qui fait clignoter un carre.
def clignoter(n):
    global serpent
    for j in range(100):
        can.itemconfig(serpent[n], fill='blue')
        attendre(100000)
        can.update()
        can.itemconfig(serpent[n], fill='white')
        attendre(100000)
        can.update()
    can.itemconfig(serpent[n], fill='black')
    can.update()

# La fonction qui affiche les cibles.
def afficheCible():
    global cibles, n
    xC = randrange(0, w/10-1)
    yC = randrange(0, h/10-1)
    # Prevu au depart pour pouvoir afficher simultanement plusieurs cibles, avec des
    # durees de vie differentes... et des rapports de points differents ! Pour une
    # prochaine version, peut-etre !
    cibles.append(carre(xC*10, yC*10, 'red'))
    n = 1

# Le mode pause
def pauser(event):
    global on, pause, textPause
    if on:
        if pause:
            pause = 0
            # On efface le gros message 'Pause' !
            can.delete(textPause)
            demarrer()
        else:
            pause = 1
            # On affiche en gros que le jeu est en pause !
            textPause = can.create_text(200, 200, text="Pause", font=("Arial", 72, 'italic'), fill='green')

# La fonction qui met a jour le score
def majScore(ds=0):
    global score, scoreSV
    score += ds
    scoreSV.set("Score : " + str(score))

# La fonction qui initialise un nouveau jeu.
def init():
    global serpent, on, pause, crash, delaiD, delaiA, cibles, n, i, score, textPause, dx, dy, cd
    # Decalages initiaux en abscisse et en ordonnee du serpent.
    dx, dy = -10, 0
    # Balise marquant un changement de direction en cours. Indispensable pour empecher
    # qu'un deuxieme changement de direction n'intervienne avant que le premier ait ete
    # completement pris en compte (changement des coordonnees de la tete du serpent - sans
    # cette precaution, un rebroussement fatal peut avoir lieu !)
    cd = 0
    # Si le compteur 'i' n'est pas nul, alors un jeu vient juste de se terminer. On efface
    # alors les objets du cannevas !
    if i:
        can.delete(cibles[0])
        for k in range(len(serpent)):
            can.delete(serpent[k])
        # Si 'pause' est a 1, il faut aussi effacer le texte 'Pause' !
        if pause:
            can.delete(textPause)
    # On (re)initialise le compteur de deplacement du derpent. C'est un entier long
    # (je suis probablement beaucoup trop optimiste ;-) !)
    i = 0
    # On met le jeu en marche...
    on = 1   
    # ...et pas en pause !
    pause = 0
    # Le delai avant le deplacement du serpent.
    delaiD = 2000000
    # Le delai avant affichage d'une nouvelle cible.
    # delaiA=10000
    # Le tableau qui stocke les cibles.
    cibles = []
    # Le nombre de cibles.
    n = len(cibles)
    # On indique qu'il n'y a pas (encore) eu de crash...
    crash = 0
    # On initialise le score a 0...
    score = 0
    majScore()
    # Creation du serpent initial.
    serpent = []
    for i in range(5):
        serpent.append(carre(180 + 10*i, 200))
    # Enfin on demarre le jeu proprement dit !
    demarrer()


#######################
# Programme principal #
#######################

# Pour nettoyer la fenetre de debogage, en mode interactif !
for i in range(30): print("")

# Longueur et largeur du cannevas.
h,w = 400, 400

# Indispensable pour eviter une erreur si qqn appuie sur la barre
# d'espacement alors qu'une fenetre 'A propos' ou 'Regle du jeu'
# est affichee.
on = 0

# Indispensable pour le premier passage par la fonction 'init()' !
i = 0

# La fenetre du jeu.
fen = Tk()
# Le titre de la fenetre.
fen.title("Mon « Nibbles » à moi !")

# La barre de menu.
bar = Menu(fen)

# Le premier menu.
menuJeu = Menu(bar)
# Le menu qui contient 'Nouveau jeu' et 'Quitter'
menuJeu.add_command(label="Nouveau jeu", command=init)
menuJeu.add_command(label="Quitter", command=quitter)
# On ajoute ce menu a la barre.
bar.add_cascade(label="Nouveau jeu", menu=menuJeu)

# Le menu 'Aide', avec la regle et un 'A propos'.
menuAide = Menu(bar)
menuAide.add_command(label="Règle du jeu", command=regleDuJeu)
menuAide.add_command(label="A propos", command=apropos)
# On ajoute ce menu a la barre.
bar.add_cascade(label="Aide", menu=menuAide)

# On ajoute la barre de menus a la fenetre.
fen.config(menu=bar)

# Le cannevas dans lequel apparaitront les dessins.
can = Canvas(fen, bg='dark grey', height=h, width=w)
can.pack()

# Le label qui affiche le score.
scoreSV = StringVar()
Label(fen, textvariable=scoreSV).pack()

# Les actions de deplacement avec les fleches de direction
# sont liees au cannevas.
fen.bind("<Up>", enHaut)
fen.bind("<Down>", enBas)
fen.bind("<Left>", aGauche)
fen.bind("<Right>", aDroite)
fen.bind("<space>", pauser)

# Boucle principale de la fenetre.
fen.mainloop()
# Quand on quitte la boucle principale, il faut detruire la fenetre !
fen.destroy()   
