#!/usr/bin/env python3
# -*- coding: utf8 -*-

from tkinter import *
from random import randrange
import tkinter.messagebox as messageBox

def attendre(t):
    for k in range(t): pass

def apropos():
    messageBox.showinfo("Mon « Nibbles » à moi","Ce bout de code est\ndistribué sous licence\nCreative Commons 3.0\nBY-NC-SA\n\nVersion 1.1 - 12/03/2003\nVersion 1.2 - 20/02/2012\n\nAuteur : Tristan LEY")

def regleDuJeu():
    fenTmp = Toplevel(fen)
    fenTmp.title("Règle de mon « Nibbles » à moi")
    texte = Text(fenTmp, width=55, height=13, relief='ridge', bd=2, wrap='word')
    texte.insert(END, "Dans ce jeu, vous contrôlez une sorte de serpent, representé par une succession de carrés de couleur jaune.\n\n")
    texte.insert(END, "Vous devez toucher les carrés cibles de couleur rouge qui apparaissent dans l'aire de jeu avec la tête de votre serpent, ce qui a pour effet de l'allonger. ")
    texte.insert(END, "Pour vous déplacer, utilisez les flèches de direction ; pour mettre en pause ou reprendre je jeu, pressez la barre d'espacement.\n\n")
    texte.insert(END, "Attention ! Si au cours d'une manœuvre vous venez à percuter la queue de votre serpent, vous avez perdu !")
    texte.pack(fill='both', expand=1)
    Button(fenTmp, text="Fermer", command=fenTmp.destroy).pack()

def carre(x, y, couleur='yellow'):
    global can
    return can.create_rectangle(x, y, x+10, y+10, fill=couleur, width=2)

def demarrer():
    global on,pause
    while on and not pause:
        bouge()
 
def quitter():
    global on
    on=0
    fen.quit()

def enHaut(event):
    global dx, dy, cd
    if on and not pause and not dy and not cd:
        dx=0
        dy=-10
        cd=1

def enBas(event):
    global dx, dy, cd
    if on and not pause and not dy and not cd:
        dx, dy, cd = 0, 10, 1

def aGauche(event):
    global dx, dy, cd
    if on and not pause and not dx and not cd:
        dx, dy, cd = -10, 0, 1

def aDroite(event):
    global dx, dy, cd
    if on and not pause and not dx and not cd:
        dx, dy, cd, = 10, 0, 1

def bouge():
    global dx, dy, serpent, h, w, i, n, cibles, crash, on, score, delaiD, cd
    contact = 0
    i += 1
    if not n:
        afficheCible()
    x, y = (can.coords(serpent[0])[0] + dx) % w, (can.coords(serpent[0])[1] + dy) % h
    if cd:
        cd = 0
    for k in range(3,len(serpent)):
        if x == can.coords(serpent[k])[0] and y == can.coords(serpent[k])[1]:
            crash = 1
            clignoter(k)
    if crash:
        messageBox.showwarning("Ooops ! Crash !","Partie terminee !\nVotre score : " + str(score))
        on = 0
    if n and x == can.coords(cibles[0])[0] and y == can.coords(cibles[0])[1]:
        can.itemconfig(cibles[0], fill='yellow')
        aBouger=cibles[0]
        contact = 1
        majScore(contact)
    if not contact:
        aBouger = serpent[-1]
        can.coords(aBouger, x, y, x+10, y+10)
        serpent = [aBouger] + serpent[:-1]
    else:
        serpent = [aBouger] + serpent
        cibles = []
        n = 0
    can.update()
    attendre(delaiD)

def clignoter(n):
    global serpent
    for j in range(100):
        can.itemconfig(serpent[n], fill='blue')
        attendre(100000)
        can.update()
        can.itemconfig(serpent[n], fill='white')
        attendre(100000)
        can.update()
    can.itemconfig(serpent[n], fill='black')
    can.update()

def afficheCible():
    global cibles, n
    xC = randrange(0, w/10-1)
    yC = randrange(0, h/10-1)
    cibles.append(carre(xC*10, yC*10, 'red'))
    n = 1

def pauser(event):
    global on, pause, textPause
    if on:
        if pause:
            pause = 0
            can.delete(textPause)
            demarrer()
        else:
            pause = 1
            textPause = can.create_text(200, 200, text="Pause", font=("Arial", 72, 'italic'), fill='green')

def majScore(ds=0):
    global score, scoreSV
    score += ds
    scoreSV.set("Score : " + str(score))

def init():
    global serpent, on, pause, crash, delaiD, delaiA, cibles, n, i, score, textPause, dx, dy, cd
    dx, dy = -10, 0
    cd = 0
    if i:
        can.delete(cibles[0])
        for k in range(len(serpent)):
            can.delete(serpent[k])
        if pause:
            can.delete(textPause)
    i = 0
    on = 1   
    pause = 0
    delaiD = 2000000
    cibles = []
    n = len(cibles)
    crash = 0
    score = 0
    majScore()
    serpent = []
    for i in range(5):
        serpent.append(carre(180 + 10*i, 200))
    demarrer()

for i in range(30): print("")

h,w = 400, 400

on = 0

i = 0

fen = Tk()
fen.title("Mon « Nibbles » à moi !")

bar = Menu(fen)

menuJeu = Menu(bar)
menuJeu.add_command(label="Nouveau jeu", command=init)
menuJeu.add_command(label="Quitter", command=quitter)
bar.add_cascade(label="Nouveau jeu", menu=menuJeu)

menuAide = Menu(bar)
menuAide.add_command(label="Règle du jeu", command=regleDuJeu)
menuAide.add_command(label="A propos", command=apropos)
bar.add_cascade(label="Aide", menu=menuAide)

fen.config(menu=bar)

can = Canvas(fen, bg='dark grey', height=h, width=w)
can.pack()

scoreSV = StringVar()
Label(fen, textvariable=scoreSV).pack()

fen.bind("<Up>", enHaut)
fen.bind("<Down>", enBas)
fen.bind("<Left>", aGauche)
fen.bind("<Right>", aDroite)
fen.bind("<space>", pauser)

fen.mainloop()
fen.destroy()
