#!/usr/bin/env python3
# coding: utf8

from tkinter import *

DEBUG = True
touches = set()

def enfoncer(evt):
    touches.add(evt.keysym)
    if DEBUG:
        print("Pression :", touches)
    action()

def relacher(evt):
    # ~ if evt.keysym in touches:
    touches.remove(evt.keysym)
    if DEBUG:
        print("Relâchement :", touches)

def action():
    if "Left" in touches:
        c.move(r, -20, 0)
    if "Right" in touches:
        c.move(r, 20, 0)
    if "Up" in touches:
        c.move(r, 0, -20)
    if "Down" in touches:
        c.move(r, 0, 20)

f = Tk()
c = Canvas(f, bg='dark grey', width=800, height=600)
r = c.create_rectangle(350, 350, 450, 450, fill="red", width=10, outline="blue")
c.pack()

f.bind('<KeyPress>', enfoncer)
f.bind('<KeyRelease>', relacher)

f.mainloop()
