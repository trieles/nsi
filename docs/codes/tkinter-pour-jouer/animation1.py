#!/usr/bin/env python3
# coding: utf8

from tkinter import *

def gauche(evt):
    c.move(r, -20, 0)

def droite(evt):
    c.move(r, 20, 0)

def haut(evt):
    c.move(r, 0, -20)

def bas(evt):
    c.move(r, 0, 20)

f = Tk()
c = Canvas(f, bg='dark grey', width=800, height=600)
r = c.create_rectangle(100, 300, 500, 500, fill="red", width=10, outline="blue")
c.pack()
f.bind("<Left>", gauche)
f.bind("<Right>", droite)
f.bind("<Up>", haut)
f.bind("<Down>", bas)
f.mainloop()

