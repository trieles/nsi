#!/usr/bin/env python3
# coding: utf8
from A import *

def affiche_console():
    """Fonction réalisant un affichage du plateau de jeu dans la console.
    Sur la 1ère et la dernière ligne, on met des lettres pour les abscisses.
    Sur les 3 premières et les 3 dernières colonnes, on met des nombres
    pour les ordonnées. Pour séparer nettement ces nombres de ceux qui
    pourraient s'afficher sur le plateau, on met un « trait vertical » │.
    """
    # On affiche en premier une ligne avec des lettres, pour repérer les abscisses
    print("".rjust(3), end="")  # rjust() justifie à droite le contenu de la chaîne
    for i in range(largeur):
        print(ascii_uppercase[i], end="")
    print() # On passe à la ligne
    # On affiche le plateau du jeu
    for j in range(hauteur):
        # On affiche successivement les lignes du plateau de jeu. Chacune débute
        #  # par son numéro, justifié sur 2 caractères, suivi du délimiteur « │ »
        print("{}│".format(j).rjust(3), end="")
        ...
        ...
        ...      # On remet le numéro de la ligne, justifié sur 2 caractères
        ...      # On passe à la ligne
    # On finit par remettre les lettres, pour mieux repérer les abscisses
    ...
    ...
    ...
    ...


def jeu_fini():
    """Fonction qui vérifie si le jeu est fini.
    Conditions de fin de jeu : toutes les mines suspectes correspondent
    aux vrais emplacements des mines, toutes les autres cases sont à
    découvert (il n'y a donc pas plus de mines suspectes que de mines à
    localiser).
    
    Paramètre : aucun.
    Valeur de retour : le booléen True si le jeu est fini, False sinon.
    """
    pass


def analyse_saisie(chaine):
    """Analyse la saisie du joueur, en s'assurant qu'elle est valide, et en
extrayant les informations pertinentes (coordonnées de la case ciblée et
statut éventuel qu'on veut lui donner : suspicieuse, douteuse ou neutre).
Valeur de retour : un tuple de 3 valeurs si la sasie est valide ou False.
Si la saisie est valide, le tuple comporte dans l'ordre :
 * l'abscisse de la case concernée : un entier ;
 * l'ordonnée de la case concernée : un entier ;
 * l'action à entreprendre : une chaîne de 1 caractère en majuscule (parmi
   " ", qui signifie « visiter la case » ; "!" qui marque la case comme
   étant suspecte, c'est-à-dire potentiellement minée ; "?" qui marque la
   case comme étant douteuse, afin de founrir un support visuel au joueur
   lorsqu'il émet des suppositions ; et "X" pour ramener la case au statut
   de case cachée).
    """
    verification = ER_SAISIE_VALIDE.match(chaine)
    if verification:
        lettre, nombre, action = verification.groups()
        x = ascii_uppercase.index(lettre.upper())
        y = int(nombre)
         # Il ne faut pas que les coordonnées saisies excèdent celles qui
         # définissent le plateau de jeu !
        if x < largeur and y < hauteur:
            return x, y, action.upper()
        else:
            return False
    else:
        return False


# Initialisation de la partie et début du jeu
initialise_jeu()
jeu_en_cours = True
perdu = True
nb_cases_suspectes = 0

while jeu_en_cours:
    affiche_console()
    print("Vous pensez avoir localisé {} mines sur {}\n".format(nb_cases_suspectes, nb_mines))
    print("""* Saisir une coordonnée sous la forme « B13␣ », par exemple (une lettre
suivie de 1 à 2 chiffres, sans guillemet, puis OBLIGATOIREMENT d'un espace)
révèle la case correspondante.
* Remplacer l'espace par un « ! » marque la case comme (peut-être) minée.
* Remplacer l'espace par un « ? » marque la case comme étant incertaine.
* Remplacer l'espace par un « X » retire toute marque associée à la case.""")
    saisie_valide = False
    while not saisie_valide:
        saisie_joueur = input(">>> Saisir votre bilan_analyse : ")
        bilan_analyse = analyse_saisie(saisie_joueur)
        if bilan_analyse:
            saisie_valide = True
            x = bilan_analyse[0]
            y = bilan_analyse[1]
            action = bilan_analyse[2]
            ...
    print()

# Si on arrive là, c'est que le jeu est terminé
solution()          # On révèle l'emplacement de toutes les mines.
affiche_console()   # On réalise le dernier affichage

if perdu:
    print("\nPerdu...")
else:
    print("\nBRAVO !!!")
