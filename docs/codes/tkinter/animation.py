#!/usr/bin/env python3
# coding: utf8
from tkinter import *
from random import randrange as rnd

# Dictionnaire rassemblant les paramètres essentiels du « jeu ».
config = {"Dx": rnd(1, 21), "Dy": rnd(1, 21), "animation": ""}

def bouge():
    """Fonction gérant les animations"""
    Dx = config["Dx"]
    Dy = config["Dy"]
    c.move(o, Dx, Dy)
    if c.coords(o)[0] <= 0 or c.coords(o)[2] >= c.winfo_width():
        config["Dx"] = -Dx
    if c.coords(o)[1] <= 0 or c.coords(o)[3] >= c.winfo_height():
        config["Dy"] = -Dy
    animation = f.after(20, bouge)              # Délai de 20 ms, soit 50 FPS...
    c.itemconfigure(t, text="Animation : {}".format(animation))
    no_animation = int(animation.split("#")[1]) # Extrait l'entier de 'after#123'.
    config["animation"] = animation
    if no_animation >= 400:
        f.after_cancel(animation)               # Arrêt de l'animation

# Construction de l'interface graphique (minimaliste).
f = Tk()
f.title("Animations avec le canevas de Tkinter")
# Le canevas
c = Canvas(f, background="light yellow", width=800, height=600)
c.pack()
# Les éléments graphiques du canevas. Nouveauté : un texte !
o = c.create_oval(200, 50, 250, 100, fill="yellow")
t = c.create_text(10, 10, text="Animation : stoppée", anchor='nw')

bouge()         # On doit appeler la fonction 'bouge()' pour initier l'animation !
f.mainloop()    # Démarrage de la boucle principale de la fenêtre.
