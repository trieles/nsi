from tkinter import *
f = Tk()
c = Canvas(f, bg="light yellow", width=600, height=600)
c.pack()
c1 = c.create_oval(0, 0, 300, 300, fill="red")
print("Identifiant du cercle C1 : {}".format(c1))
c2 = c.create_oval(250, 250, 550, 550, fill="blue")
print("Identifiant du cercle C2 : {}".format(c2))
collision = c.find_overlapping(250, 250, 300, 300)
print("""Éléments du canevas présents dans le rectangle
(250, 250) --> (300, 300) : {}""".format(collision))
c.create_rectangle(250, 250, 300, 300, outline="green", width=5)
