# coding: utf8
from tkinter import *

largeur_canevas = 500
hauteur_canevas = 150

def dessine(event):
   rouge_orange = "#ff6042"
   x1, y1 = event.x - 1, event.y - 1
   x2, y2 = event.x + 1, event.y + 1
   c.create_oval(x1, y1, x2, y2, fill=rouge_orange, outline=rouge_orange)

f = Tk()
f.title("Dessinons en pointillé")
c = Canvas(f, width=largeur_canevas, height=hauteur_canevas)
c.pack(expand=YES, fill=BOTH)
c.bind('<B1-Motion>', dessine) # Autre possibilité : f.bind(...)

message = Label(f, text="Cliquer et déplacer la souris pour dessiner")
message.pack(side=BOTTOM)

f.mainloop()
