# coding: utf8
from tkinter import *
from math import *

def evaluer(event):
    resultat.configure(text="Résultat : {}".format(eval(saisie.get())))

f = Tk()
f.title("Calculette")
saisie = Entry(f)
saisie.bind("<Return>", evaluer)
resultat = Label(f)
saisie.pack()
resultat.pack()

f.mainloop()
