# Sommaire de l'espace NSI

* [Projet-flash : le puissance 4](https://trieles.gitlab.io/nsi/5-puissance4/){target='__blank' rel='noopener'}.

* [Projet semi-guidé : le jeu du démineur](https://trieles.gitlab.io/nsi/10-demineur/){target='__blank' rel='noopener'}.

* [Introduction à Tkinter](https://trieles.gitlab.io/nsi/20-tkinter/){target='__blank' rel='noopener'}.  
Ce document (hors-programme de la spécialité NSI) pourra néanmoins être
utile (par exemple dans le cadre d'un projet).

* [Comment réaliser des petits jeux d'arcade avec Tkinter ?](https://trieles.gitlab.io/nsi/30-tkinter-pour-jouer/){target='__blank' rel='noopener'}  
Ce document (également hors-programme de la spécialité NSI) présente l'ensemble
des difficultés auxquelles un néophyte en programmation avec Tkinter devra
faire face s'il veut réaliser un petit jeu d'arcade (Pong, Casse-Briques,
Tetris, etc.). Les solutions adaptées ainsi que quelques exemples sont également
de mise, bien sûr :wink: !
