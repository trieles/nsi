# Projet-flash : le jeu du « puissance 4 »

Pour découvrir le jeu et les principes qui le régissent, je vous demande
de consulter **à l'avance** [la page de Wikipedia consacrée au jeu][puissance4]{target=__blank rel='noopener'}.
Vous devez être familiarisé avec le jeu (je vous recommande même de faire
quelques parties ! si si !) : c'est **essentiel** pour bien comprendre la
logique sous-jacente, les mécanismes du jeu, ainsi que les possibilités
d'action des joueurs.

<figure markdown>
![Animation jeu de puissance 4](images/5/puissance4.gif)
</figure>

L'objectif de ce document est de vous amener à la réalisation d'un proto-jeu
de puissance 4. Le jeu ne sera pas vraiment fonctionnel, mais vous pourrez
jouer en ligne de commande (dans un interpréteur Python en mode interactif),
en appelant diverses fonctions et en leur passant les paramètres adéquats.

??? info "Saviez-vous qu'il y a encore des recherches en mathématiques fondamentales sur ce jeu ?"

    <div class="video-wrapper" style="text-align: center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/Ya69dn_vZYw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>


## 1. L'alpha : la structure de données

Linus Torvalds, créateur du noyau Linux, toujours en charge de coordonner
le projet actuellement, a déclaré : *« [Bad programmers worry about the code.
Good programmers worry about data structures and their relationships](https://www.goodreads.com/quotes/1188397-bad-programmers-worry-about-the-code-good-programmers-worry-about){target=__blank rel='noopener'} »*.
Et il a bien raison : si vous vous loupez dans votre analyse préalable d'un
problème qui doit être résolu informatiquement, vous allez rater votre choix
des structures informatiques qui vont accueillir les données de ce problème.
Il est donc fondamental de bien réfléchir à ce que l'on veut conserver dans
la mémoire de l'ordinateur, le temps que dure le jeu.

Seul ou à 2, songez-y 5 minutes, avec un papier à portée de main. Vous listerez :

* les éléments physiques du jeu d'origine, qui devront impérativement avoir
leur pendant informatique ;

* la « façon » dont vous allez les mémoriser (quel type de données Python ?) ;

* le nom que vous allez donner aux variables qui désigneront ces structures
de données.

??? done "L'organisation qu'on retiendra"

    * On utilisera une **liste de listes** pour mimer et mémoriser la grille du
    jeu.
    
    * Il faudra être en mesure de distinguer une case vide, une case occupée
    par un pion du joueur 1 et une case occupée par un pion du joueur 2.
    On représentera une case vide par la valeur  `0` à la position idoine de
    la liste de listes ; un pion du joueur 1 correspondra à la valeur `1`,
    la valeur `2` identifiant un pion du joueur 2 (on aurait tout aussi bien
    pu mettre respectivement `None`, `"J1"` et `"J2"`, ou toutes autres
    valeurs permettant de discriminer les différents états possibles pour
    chaque case de la grille ― mais les valeurs de type `<int>` que sont
     `0`, `1` et `2` constituent un meilleur choix ― se reporter au
     [paraphrage 4. a.](#4-a-les-constantes) pour plus de détails).
    
    * Le nom de variable `grille` servira à désigner la... grille !

??? warning "Remarques importantes"

    * Avez-vous réfléchi au fait qu'en Python déclarer et initialiser une
    variable se fait en une seule et même étape ([dans d'autres langages,
    comme le C, cela peut être fait *via* des étapes distinctes](https://stacklima.com/differentes-manieres-d-initialiser-une-variable-en-c-c/){target=__blank rel='noopener'}) ?
    Il faut donc songer à la façon de construire la grille du jeu, initialement
    vide. Comment allez-vous faire ? Tâchez de trouver 3 solutions :

        * par construction directe (avec une boucle) ;
        
        * en utilisant astucieusement les « capacités multiplicatives »
        de Python (testez ce que renvoie `[0] * 10`) ;
        
        * en utilisant une compréhension de listes.

        Vous pourrez ensuite comparer leurs mérites respectifs... et leurs failles
        éventuelles !

    * Il faudra écrire `grille[y][x]` pour accéder au contenu de la case
    d'abscisse `x` et « d'ordonnée `y` » (en effet, `grille[y]` désignera une
    liste, qui elle-même désignera de façon commode une ligne de la grille
    de jeu) ! Il pourra donc s'avérer pratique d'écrire des fonctions utilitaires
    afin de rendre les manipulations du plateau (lectures ou écritures)
    et l'affichage plus naturels.

     * Avez-vous pensé à la notion d'ordonnée, dans ce contexte ? Comment
     trouveriez-vous simple / agréable / logique / *whatever* d'orienter
     l'axe des ordonnées dans ce contexte ?

         Dit autrement, à partir d'une grille vide, lorsque le premier joueur
         met son jeton, celui-ci tombe tout au fond de la grille : quelle ordonnée
         aurez-vous envie d'attribuer à ce jeton ?


## 2. Liste des actions à réaliser pour le bon déroulement d'une partie

### 2. a. Actions des joueurs

Un joueur ne peut faire qu'une chose : jouer un jeton. Celui-ci glissera
dans une colonne donnée (qu'on peut repérer par un nombre, qu'on appellera
naturellement l'abscisse du jeton) jusqu'au niveau le plus bas possible
(qui dépendra du fait que la colonne soit vierge ou non de tout jeton).

Il faut donc écrire le code nécessaire pour permettre la modification de
la grille suite aux actions des joueurs. 

??? warning "Remarques importantes"

    Faut-il écrire une fonction, ou deux fonctions (puisqu'il y a deux joueurs),
    ou aucune fonction ? Les fonctions éventuelles auront-elles des valeurs
    de retour, et si oui lesquelles ? 

??? done "L'organisation qu'on retiendra"

    On choisira naturellement d'écrire une seule fonction, qui prendra en
    paramètres :

    * le numéro du joueur, numéro qui sera utilisé pour remplir la grille
    de jeu à la place des `0` initiaux ;
    
    * le numéro de la colonne dans laquelle un joueur joue ;

    * la valeur de retour sera `True` si le coup joué était possible, `False`
    sinon.

### 2. b. Autres actions

Voyez-vous d'autres actions à mener dans le cadre d'un jeu informatique,
qui ne sont pas directement reliées à la volonté des joueurs de jouer tel
ou tel coup ?

??? done "Avez-vous pensé à tout ?"

    Il faut prévoir :

    * une fonction qui analyse la grille après chaque coup joué par l'un
    des joueurs. Cette fonction doit pouvoir dire :

        * si un joueur a gagné ;
        * ou s'il y a match nul ;
        * ou si la partie doit continuer.

    * une fonction qui réalise un affichage, ne serait-ce qu'un affichage
    (utlra-)basique (indication : utiliser la méthode `join` des chaînes
    de caractères). En attendant, vous pouvez utiliser le code ci-dessous
    pour visualiser correctement le plateau de jeu :

        ``` python
        from pprint import pprint
        pprint(grille, width=23)    # Paramètre width à adapter selon les besoins 
        ```

    * une fonction qui permette aux joueurs de saisir le coup qu'ils veulent
    jouer, et qui vérifie que le coup saisi est licite (il faut **toujours**
    se méfier des saisies des usagers !


## 3. L'omega : faire fonctionner le tout !

Là, c'est à vous. Il faut faire en sorte que le jeu tourne jusqu'à ce qu'une
issue (victoire d'un joueur ou match nul) survienne.


## 4. Éléments pour aboutir à de bonnes solutions

### 4. a. Les constantes

* Il est pertinent de « variabiliser » la taille de la grille. Pour cela,
on déclarera deux variables (dans d'autres langages de programmation, on
parlera de  « constantes », mais cette notion n'a pas de sens en Python)
au tout début du programme :

    ``` python
    LARGEUR = 7
    HAUTEUR = 6
    ```

 * Il est **fondamental** de chercher à séparer le fond et le forme, en
 développement informatique. Ainsi, les données contenues dans la variable
 `grille` ne serviront pas *directement* à « fabriquer » l'affichage. Les
 symboles qui seront utilisés seront déclarés dans une variable de type
 `<list>` nommée :
 
    ``` python
    # Dans la liste ci-dessous, la chaîne à la position 0 sert à représenter
    # une case vide de la grille, celle à la position 1 sert à représenter
    # un pion du joueur 1, tandisque celle à la position 2 sert à représenter
    # un pion du joueur 2.
    SYMBOLES = [ "_", "O", "X" ]
    ```
       
    Une valeur `val` dans `grille` peut désigner, au choix :

    * une position vide si ` val` a pour valeur `0` ;
    * un pion du joueur 1 si ` val` a pour valeur `1` ;
    * un pion du joueur 2 si ` val` a pour valeur `2`.

    Alors `SYMBOLES[val]` désignera le caractère associé à la représentation
    de la case représentée par `val` dans `grille`. Et si l'on décide, pour
    une raison ou une autre, de changer la façon dont on représente le plateau
    de jeu, c'est très facile à faire, en modifiant la définition de `SYMBOLES`.

### 4. b. Modification de la grille de jeu

Les actions des joueurs vont modifier la grille du jeu.

* Chaque joueur jouera alternativement.
* Les actions des joueurs sont similaires : le joueur 1 met un pion dans
une colonne, le joueur 2 en fait autant. On a décidé qu'un pion du joueur
1 est représenté par l'entier `1` dans le grille. De même, un pion du joueur
2 est représenté par l'entier `2` dans le grille. Ainsi, il n'y a qu'une
seule fonction à écrire, avec deux paramètres :
    
    * l'un qui correspond au joueur qui joue (ainsi qu'au pion placé) ;
    * l'autre indique la colonne dans laquelle le joueur souhaite jouer.
    
* La fonction **devra** renvoyer une valeur booléenne, car il n'est pas
toujours possible de jouer (si une colonne est pleine, pas moyen d'ajouter
un pion supplémentaire !).

:warning: Comment indiquer la colonne dans laquelle un joueur place un pion ?

:warning: Il est important de s'assurer que les saisies des joueurs sont
**correctes** (au sens où elles sont conformes aux besoins du code : il ne faut
pas laisser un joueur saisir des chiffres si les colonnes sont identifiées
par des lettres !).

### 4. c. Vérifications de l'ordinateur

Entre chaque tour de jeu, le programme doit effectuer des modifications :
* le jeu est-il fini parce qu'un joeur a gagné ?
* le jeu est-il fini parce que la grille est pleine et qu'aucun joueur ne
peut ajouter de pion ?



[puissance4]: https://fr.wikipedia.org/wiki/Puissance_4 "Le jeu du « puissance 4 »"
