# Pyodide

On peut avoir un IDE avec un fichier Python préchargé.


Fichier local :

{{ IDE('51-moyenne') }}


Fichier dans ```scripts/51-pyodide/``` :

{{ IDE('51-pyodide/test') }}


Fichier dans un autre dossier :

{{ IDE('codes/40/cacher') }}


>>> Pour de plus amples informations, le travail de Vincent Bouillot est présenté ici : <https://bouillotvincent.gitlab.io/pyodide-mkdocs/>
