# Python : traitements d'images simples et stéganographie

Dans ce document, vous allez découvrir les bases de la manipulation d'images
en Python, en écrivant des programmes qui réalisent des actions simples
sur les images :

* créer informatiquement des images simples ;
* conversion d'une image en niveaux de gris vers une image noir & blanc ;
* conversion d'une image en couleurs vers une image en niveau de gris ;
* [« inverser » une image](https://fr.wikipedia.org/wiki/Film_n%C3%A9gatif){target='__blank' rel='noopener'}
(par exemple, pour une image noir & blanc, changer tous les pixels noirs
en pixels blancs et réciproquement) ;
* diverses actions sur les images (flouter, « durcir », etc.).

Vous verrez ensuite comment compresser une image, puis comment mettre en
place un système de stéganographie simple : **la stéganographie est l'art
de la dissimulation**, son objectif est de _cacher des informations_ aux
personnes non averties (alors que la _cryptographie_ cherche à rendre incompréhensibles
(on dit aussi _inintelligibles_) ces mêmes informations à toute personne
non habilitée). Typiquement, la stéganographie permet de _cacher une image
**dans** une autre image_.

Tout cela sera initialement réalisé avec des formats d'images très simples :
les « _[Portable Pixmaps ](https://fr.wikipedia.org/wiki/Portable_pixmap){target='__blank' rel='noopener'}_ »
(des formats plus standards, tels que le [PNG](https://fr.wikipedia.org/wiki/Portable_Network_Graphics){target='__blank' rel='noopener'},
seront ensuite utilisés).

Dans la suite, vous pourrez travailler à partir des images fournies dans
[l'archive associée à ce document](ressources/1ère-NSI-Images-et-stéganographie-Ressources.zip)
(ce devra **obligatoirement** être le cas lorsqu'un nom précis d'image vous
sera indiqué).


## Motivations

Ces travaux sur les images vont vous permettre :

* de réviser l'accès à des fichiers en lecture et en écriture ;

* de reparler de numérisation et de codage des informations visuelles
(l'occasion de revoir la numération binaire), ainsi que la façon dont
on peut agir dessus ;

* de renforcer la maîtrise des boucles et des listes (ainsi que des listes
de listes, car les images sont essentiellement des tableaux de pixels à
2 dimensions) ;

* d'avoir une première approche de la compression de données. En effet,
une image de grande taille (4000 × 3000 pixels par exemple), enregistrée
au format PPM (présenté plus bas), occupe une place énorme (environ 90 Mo
pour une image PPM ASCII, dans les 40 Mo pour une image PPM binaire) ! Sur
des images « simples » (au sens « pas trop riches en détails »), on verra
l'effet du codage [RLE](https://fr.wikipedia.org/wiki/Run-length_encoding){target='__blank' rel='noopener'}
(en guise de hors d'œuvre, car en Terminale on découvrira le
[codage de HUFFMAN](https://fr.wikipedia.org/wiki/Codage_de_Huffman){target='__blank' rel='noopener'},
plus sophistiqué — mais dont l'implémentation est bien plus délicate) ;

* d'acquérir des notions élémentaires sur la sécurité. En effet, rien de
tel qu'un message incompréhensible pour attirer l'attention et aiguiser
l'intérêt, au contraire d'une $n$-ième image échangée, en toute transparence,
_via_ un réseau quelconque (MMS, application dédiée, site web communautaire,
réseau social — ou même au sein d'un antique courriel)... C'est là tout
l'intérêt de la stéganographie, qui recherche la sécurité dans la lumière,
au contraire du chiffrement, qui recherche la sécurité par l'ombre et
l'obfuscation[^stéganographie-vs-cryptographie].

Sachez en outre :

* que la stéganographie joue un rôle essentiel dans l'industrie du divertissement.
Elle sert en effet à [« tatouer » des œuvres numériques](https://fr.wikipedia.org/wiki/Tatouage_numérique){target='__blank' rel='noopener'}
(en anglais on parle de _« watermarking »_, terme qui signifie filigrane).
L'objectif du tatouage est de [marquer de façon indélébile un fichier](https://interstices.info/le-tatouage-de-son/){target='__blank' rel='noopener'},
afin par exemple d'assurer la protection des droits d'auteur. Les technologies
associées sont commercialisées par divers laboratoires ou entreprises, et
employées par les plates-formes de vente en ligne de musiques et et de vidéos
dématérialisées afin de lutter contre le piratage[^tatouage-commerce] ;

* que cette logique de marquage ineffaçable peut également être utilisée
à des fins qui peuvent parfois être plus sensibles (lutte contre l'espionnage)...
ou contestables (comme par exemple traquer les lanceurs d'alertes)[^tatouage-traitres].

!!! note "À faire vous-même"

    === "Vos objectifs"

        1. Trouver des techniques non (purement) informatiques de stéganographie.
        Soyez inventifs, voire artistes !
        
        2. La lutte contre le piratage est un sujet complexe et délicat.
        Il importe de s'informer sur les aspects légaux lié aux œuvres numériques,
        les moyens techniques (tels que les [DRM](https://fr.wikipedia.org/wiki/Gestion_des_droits_numériques){target='__blank' rel='noopener'})
        et juridiques de les protéger. La question des licences, précisant
        ce que l'on peut faire d'une œuvre numérique, est alors centrale.
        Il faut aussi savoir qu'en FRANCE existe une [exception juridique permettant la copie d'une œuvre en vue d'un usage privé](https://fr.wikipedia.org/wiki/Copie_priv%C3%A9e#France){target='__blank' rel='noopener'}...
        mais qu'[HADOPI](https://fr.wikipedia.org/wiki/Haute_Autorité_pour_la_diffusion_des_œuvres_et_la_protection_des_droits_sur_internet){target='__blank' rel='noopener'}, une structure visant à la protection des intérêts
        économiques liés aux œuvres numériques, existe toujours.
        
        3. Le simple fait de travailler sur des images oblige donc à se
        poser la question de la légalité de leur obtention et des possibilités qui nous sont (ou pas) accordées de les modifier : il existe des banques d'images libres de droit (sous [licence Creative Commons](https://creativecommons.org/licenses/?lang=fr-FR){target='__blank' rel='noopener'}), et même (au moins) un moteur
        de recherche d'images libres : trouvez-les !
        
    === "Indications"

        * Vous pourrez chercher du côté de la littérature, spécialement
        de certains poèmes qui sont plus que ce qu'ils paraissent être
        (par exemple quand on ne conserve que la première lettre de chaque
        vers, ou le premier mot, ou...). Certains sont assez fameux et...
        coquins :sweat_smile: !
        * Du côté des arts graphiques, certains dessins peuvent être perçus
        de deux façons différentes : on est dans le domaine des illusions
        d'optique...
        
        N'hésitez pas à parler de ce sujet avec les enseignants des disciplines
        concernées !

    === "Réponses"
        
        1. [Les acrostiches sur Wikipedia](https://fr.wikipedia.org/wiki/Acrostiche#Une_rosserie){target='__blank' rel='noopener'}.
        
        2. De nombreux dessins peuvent être perçus (plus ou moins facilement) de plusieurs façons.
        
            * Le [canard-lapin](https://fr.wikipedia.org/wiki/Canard-lapin){target='__blank' rel='noopener'}, peut-être la plus célèbre [image ambiguë](https://fr.wikipedia.org/wiki/Image_ambigüe){target='__blank' rel='noopener'}.
        
            * Verrez-vous [un joueur de saxophone ou une visage de femme](https://tra.img.pmdstatic.net/scale/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2Fcam.2F2020.2F02.2F05.2Ff77b17f6-fcba-449f-92a4-aa6da2cb5a09.2Ejpeg/autox600/quality/65/un-visage-ou-un-joueur-de-saxophone.jpg){target='__blank' rel='noopener'} ?
            * [Ne percevrez-vous qu'un arbre, ou des visages](https://tra.img.pmdstatic.net/scale/http.3A.2F.2Fprd2-bone-image.2Es3-website-eu-west-1.2Eamazonaws.2Ecom.2Fcam.2F2020.2F02.2F05.2Fa926f918-e868-4fcd-8152-41d44c67794f.2Ejpeg/autox600/quality/65/voici-l-arbre-a-visages.jpg){target='__blank' rel='noopener'} (et dans ce cas, combien) ?
            * [Voyez ici](https://i.pinimg.com/236x/23/29/3d/23293dff61a412d204b4ed4d2454831a--sleeping-tiger-tiger-art.jpg){target='__blank' rel='noopener'} un paysage qui comporte un tigre... un seul ?
            * Verrez-vous dans [ce tableau](https://i.pinimg.com/236x/c5/24/f9/c524f99c508f247c5652d60e775b97d4--oleg-shuplyak-old-mans.jpg){target='__blank' rel='noopener'} un paysage champêtre ou le visage d'un vieux moustachu ?
            * [Dernier exemple](https://i.pinimg.com/236x/a0/b2/df/a0b2df13702273b399b8e25dde68fc44--oleg-shuplyak-amazing-art.jpg){target='__blank' rel='noopener'} : discernerez-vous un artiste-peintre ou un visage ?
            
        3. [Openverse](https://wordpress.org/openverse/){target='__blank' rel='noopener'} (anciennement [Creative Commons Search](https://oldsearch.creativecommons.org/){target='__blank' rel='noopener'} est un moteur de recherche spécialisé dans les médias
        diffusés sous [licences Creative Commons](https://creativecommons.org/licenses/?lang=fr-FR){target='__blank' rel='noopener'} ou relevant du [domaine public](https://fr.wikipedia.org/wiki/Domaine_public_(propriété_intellectuelle)){target='__blank' rel='noopener'}.



## Découverte des formats PPM

### Description

Les formats d'images [Portable Pixmaps](https://fr.wikipedia.org/wiki/Portable_pixmap){target='__blank' rel='noopener'} regroupent en fait 3 formats différents de fichiers graphiques,
prévus pour pouvoir être utilisés sans difficulté pour les échanges _via_ la messagerie électronique qui, à ses débuts, n'acceptait que du texte encodé en [ASCII](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange){target='__blank' rel='noopener'}.

Ce sont des formats d'_images **matricielles**_ (les pixels de l'image sont représentés par un tableau de valeurs, qu'on appelle également matrice en sciences d'où la dénomination « d'images matricielles »). Leur organisation interne est très simple, ce qui la rend facilement lisible (et donc intelligible) _par un humain !_ **Vous pourrez ainsi aisément observer l'impact de vos programmes** (qui transformeront les images) **sur l'organisation interne des fichiers associés,** chose impossible avec les formats classiquement utilisés, plus modernes et sophistiqués.

**Il vous suffira d'ouvrir les fichiers-images directement dans un simple éditeur de textes[^TraitementTextes] (Notepad, Gedit, ...).** On évite ainsi la « magie » liée aux bibliothèques telles que [Pillow](https://pillow.readthedocs.io/){target='__blank' rel='noopener'}[^Pillow].

* Le [Portable BitMap](https://fr.wikipedia.org/wiki/Portable_pixmap#PBM){target='__blank' rel='noopener'} ou **PBM** est utilisé pour des images noir et blanc : un pixel noir est représenté par un caractère 1, un pixel blanc est codé par un caractère 0.

* Le [Portable GrayMap](https://fr.wikipedia.org/wiki/Portable_pixmap#PGM){target='__blank' rel='noopener'} ou **PGM** est utilisé pour des images en nuances de gris (on dit « niveaux de gris »). Une valeur maximale est définie comme limite aux valeurs codant la profondeur de gris : elle est associée au blanc (absence de gris). Chaque niveau de gris est codé par une valeur entre 0 et cette valeur maximale, proportionnellement à son intensité (un pixel noir, donc _dépourvu de blanc_, correspond donc à la valeur 0).

* Le [Portable PixMap](https://fr.wikipedia.org/wiki/Portable_pixmap#PPM){target='__blank' rel='noopener'} ou PPM est utilisé pour des images en couleurs. Chaque pixel est codé par trois valeurs (définissant le niveau de rouge, de vert et de bleu du pixel). Une valeur maximale est également utilisée pour limiter les niveaux de couleur. 

Ces 3 formats ont été définis dans le cadre du projet [NetPBM](http://netpbm.sourceforge.net/){target='__blank' rel='noopener'}. 


### Convertir une image en PxM

!!! note "À faire vous-même"

    === "Vos objectifs"
    
        1. Prendre une photographie avec votre téléphone portable — attention
        à bien respecter le [droit à l'image et au respect de la vie privée](https://www.service-public.fr/particuliers/vosdroits/F32103#:~:text=Le%20droit%20%C3%A0%20l'image,vous%20pouvez%20saisir%20le%20juge.){target='__blank' rel='noopener'} !
        2. Convertir cette photographie (probablement enregistrée sur votre
        téléphone au format [JPEG](https://fr.wikipedia.org/wiki/JPEG){target='__blank' rel='noopener'} ou [HEIF](https://fr.wikipedia.org/wiki/High_Efficiency_Image_File_Format){target='__blank' rel='noopener'} si vous utilisez un appareil Apple®), en fichier PPM.
        3. D'après les caractéristiques du format PPM, combien de nuances
        de couleurs différentes un pixel peut-il prendre, au maximum ?
        3. Comparez les tailles des fichiers JPEG et PPM. Que peut-on en
        conclure ?
        4. Trouvez un logiciel de compression de données libre et gratuit,
        et utilisez-le pour compresser l'image PPM (par exemple aux formats
        ZIP et 7z). Comparez à nouveau les tailles du fichier JPEG et du
        fichier PPM compressé. Qu'en conclure ?
        5. Pour aller plus loin :
            * jouez avec le niveau de compression proposé lors de l'enregistrement
            d'images en JPEG et comparez à la fois la taille des fichiers
            obtenus et le rendu à l'écran d'images compressées avec un taux
            de 95 %, 80 %, 50 % et 20 % (ce paramètre peut aussi être appelé
            « qualité », en fonction du logiciel).  
            :warning: **Attention** à bien créer un nouveau fichier à _chaque_
            fois, de sorte à **ne pas** effacer le fichier d'origine !
            * comparez à nouveau taille et qualité avec la même image enregistrée
            au format [PNG](https://fr.wikipedia.org/wiki/Portable_Network_Graphics){target='__blank' rel='noopener'} ;
            * que donne la compression au format ZIP d'une image JPEG ?
        
    === "Indications"

        * Vous pourrez utiliser [GIMP][gimp]{target='__blank' rel='noopener'} pour changer de type de fichiers, un logiciel de traitement d'images *libre*, *gratuit* et fonctionnant sur une grande variété de systèmes d'exploitation différents.
        * Un excellent logiciel de compression-décompression, également libre et gratuit, se nomme [7-zip](https://www.7-zip.org/download.html){target='__blank' rel='noopener'}.
        * Lorsqu'une situation présente plusieurs possibilités qui peuvent
        évoluer chacune indépendamment des autres, le nombre de combinaisons
        possibles s'obtient en multipliant toutes les possibilités. En mathématiques,
        on appelle **dénombrement** les logiques et techniques qui se rapportent
        à ces problématiques.
    
    === "Éléments de réponses"
    
        * Les formats JPEG / HEIF donnent des fichiers de bien plus petite taille (en octets) que le format PPM. Ils ont été conçus pour cela ! Mais cela a un coût : plus le niveau de qualité baisse (donc plus le taux de compression augmente), plus la qualité de l'image se dégrade : on parle de **compression avec pertes**. L'enregistrement d'une image dans ces formats supprime des informations, c'est pour cela que la taille du fichier obtenu diminue. Mais si l'on en supprime trop, cela finit par se voir...
        * Le format PNG permet d'enregistrer une image _sans perte d'information_, donc en conservant _tous_ les détails de l'image d'origine. Il fait appel à un algorithme de compression de données, mais comme il y a toujours autant d'information à enregistrer, la taille du fichier obtenu ne diminue pas autant que lorsqu'on utilise le format JPEG ou HEIF.
        * Parfois, compresser deux fois des fichiers permet de gagner encore un peu en taille, mais le gain est souvent très limité. Dans certains cas, le fichier ainsi obtenu peut même être _plus volumineux_ que le fichier d'origine !
        * En PPM, chaque canal de couleur peut comporter jusqu'à $65\,536 = 2^{16}$
        nuances différentes. Comme il y a 3 couleurs primaires (le Rouge,
        le Vert et le Bleu en [synthèse additive](https://fr.wikipedia.org/wiki/Synth%C3%A8se_additive){target='__blank' rel='noopener'}),
        on a donc $2^{16} \times 2^{16} \times 2^{16} = \left(2^{16}\right)^3 = 2^{16 \times 3} = 2^{48} \approx 2,\!8\times10^{14}$
        nuances possibles ! Soit [beaucoup plus qu'un écran informatique
        standard ne peut en afficher](https://www.latelierducable.com/tv-televiseur/8-bits-10-bits-cest-quoi-la-profondeur-des-couleurs/){target='__blank' rel='noopener'} !


### Description des formats PxM

Les 3 formats (noir et blanc, niveaux de gris, couleurs) existent chacun en deux variantes : binaire ou [ASCII](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange){target='__blank' rel='noopener'}.
**On ne s'intéressera dans la suite qu'aux fichiers [ASCII](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange){target='__blank' rel='noopener'} !**

??? info "Remarque : différences entre mode ASCII et mode binaire"

    * Pour une image PxM enregistrée en **ASCII**, le (ou les) nombres codant
    chaque pixel (voir les explications plus bas) sont stockés comme des
    caractères, comme un simple texte. Par exemple la valeur `#!python 235`
    qui, en PGM, code un gris très léger (proche du blanc), est enregistrée
    sous la forme du caractère `#!python 2`, suivi d'un `#!python 3`, suivi
    d'un `#!python 5`, suivi d'une espace qui sépare ce groupe de caractères
    du suivant.
    
    * Pour une image PxM enregistrée en **binaire**, chaque nombre codant
    un pixel est converti en base 2 et la suite de bits ainsi obtenue est
    enregistrée telle quelle dans un fichier qui, de ce fait, n'est pas
    intelligible lorsqu'on l'ouvre dans un éditeur de textes. La même valeur
    `#!python 235` sera convertie en `#!python 11101011`.  
    Si l'on ouvre le fichier [smiley-binaire.pbm](images/40/smiley-binaire.pbm){target='__blank' rel='noopener'}
    avec un éditeur de textes, on verra apparaître quelque chose comme :
    ``` title="Format binaire affiché en tant que texte de l'image « smiley-binaire.pbm »" hl_lines="3"
    P4
    10 10
    ^^^@!^@@��@�@�@�@L�!^@^^^@
    ```
    Notez la ==dernière ligne==, parfaitement inintelligible.
    

Les fichiers PxM (c'est-à-dire PBM, PGM ou PPM) ont une structure similaire. On trouve, dans l'ordre :

1. le _nombre magique_ du format (codé sur 2 octets) : il indique le type de format (PBM, PGM, ou PPM) et la variante (binaire ou ASCII) suivi d'un caractère d'espacement (comprendre : l'espace, la tabulation ou le caractères de « nouvelle ligne »). Les différents types existants sont :

<center>

| ASCII          | PBM | PGM | PPM |
|:--------------:|:---:|:---:|:---:|
| Nombre magique | P1  | P2  | P3  |


| Binaire        | PBM | PGM | PPM |
|:--------------:|:---:|:---:|:---:|
| Nombre magique | P4  | P5  | P6  |

</center>


2. la _largeur_ de l'image (nombre de pixels, écrit explicitement sous forme d'un nombre en caractères ASCII) suivie d'un caractère d'espacement (même remarque que précédemment), puis de la _hauteur_ de l'image (codée de la même façon) à son tour suivie d'un caractère d'espacement ;

3. pour les images PGM et PPM, la valeur maximale du codage d'une (composante de) couleur est ensuite indiquée (typiquement 255, soit 256 nuances — mais ce nombre peut atteindre 65$\,$535, sans l'excéder). Comme d'habitude, ce nombre est suivi d'un caractère d'espacement ;

4. les _données_ de l'image : succession des valeurs associées à chaque pixel. L'image est codée _ligne par ligne_ en partant du haut, chaque ligne étant codée de gauche à droite.  
En PBM, il est inutile (mais pas impossible) de séparer les valeurs par un caractère d'espacement, mais c'est obligatoire pour les PGM et PPM.  
Pour les images PPM, chaque pixel est codé par un triplet de valeurs (une pour coder la composante rouge, une pour la composante verte, une pour la composante bleue), toujours séparées par un caractère d'espacement.

??? info "Remarque : mettre des commentaires dans les images PxM"

    Toutes les lignes commençant par le caractère # sont ignorées (ce sont des lignes de _commentaires_). Chaque ligne ne devrait pas dépasser 70 caractères, mais en pratique cela ne semble pas poser de problème...
        

### Trois exemples

??? warning "Une difficulté, un piège de Windows^®^ : les extensions de fichiers !"

    Depuis de nombreuses années, Windows cache _par défaut_ les [extensions
    de fichier](https://fr.wikipedia.org/wiki/Extension_de_nom_de_fichier){target='__blank' rel='noopener'}.
    Mais Windows^®^ est **stupide** : il cherchera _obstinément_ à interpréter
    un fichier en se basant sur son _nom_, par sur sa _structure interne_ !
    
    **Pour bien comprendre le problème, il faut déjà forcer Windows à afficher
    les extensions de tous les fichiers.** Pour cela, [suivez ce lien (pour Windows 7)](https://www.pcastuces.com/pratique/astuces/2540.htm){target='__blank' rel='noopener'},
    ou [celui-ci pour Windows 10](https://www.commentcamarche.net/informatique/windows/185-afficher-les-extensions-et-les-fichiers-caches-sous-windows/){target='__blank' rel='noopener'}.
    
    Une fois les instructions suivies, tous les fichiers devraient désormais
    présenter une extension, visible dans l'Explorateur de fichier. Ainsi,
    un document créé dans le bloc-notes et enregistré depuis ce programme
    très simple (trop simple, en fait, car il ne permet pas de choisir l'encodage
    des caractères !) s'affichera sous le nom `document.txt`.
    
    Téléchargez maintenant [cette image](images/40/poterie-indigène-australie.jpg){target='__blank' rel='noopener'},
    vous verrez que son nom est `poterie-indigène-australie.jpg` (elle devrait
    se trouver, sous Windows^®^, dans le dossier nommé « Téléchargements »).
    Si vous double-cliquez dessus, elle devrait s'afficher dans la visionneuse
    de photos. Renommez-là en `poterie-indigène-australie.zip` et recommencez :
    que se passe-t-il ?
    
    Sous GNU/Linux, au moins, la nature d'un fichier est déterminée en se
    basant sur son organisation interne. **La commande [`file`](https://fr.wikipedia.org/wiki/File_(Unix)){target='__blank' rel='noopener'}
    permet d'obtenir nombre de détails sur un fichier.** Par exemple :
    
    ```bash
    user@machine:~/Images$ file fichier.png 
    fichier.png: PNG image data, 3460 x 1440, 8-bit/color RGBA, non-interlaced
    user@machine:~/Images$
    ```


#### Un smiley en PBM

![](images/40/smiley.png){ align=right width=300 }

``` title="Code de l'image « smiley.pbm »"
--8<-- "docs/images/40/smiley-multi-lignes.pbm"
```

??? info "Remarque : comment créer ce fichier _en pratique_"

    Vous pouvez créer un nouveau fichier **texte**, l'ouvrir dans l'**éditeur de textes**[^TraitementTextes]
    de votre choix (**pas** un traitement de textes), puis effectuer un
    copier-coller du texte ci-dessus dedans.
    
    Un bouton est disponible en haut à droite de l'encart, pour vous faciliter
    l'opération : ![](images/40/bouton-copie.png).
    
    Il vous suffit ensuite de renommer ce fichier en « `smiley.pbm` » (attention
    aux [extensions de fichier, souvent masquées par défaut sous Windows^®^](https://www.commentcamarche.net/informatique/windows/185-afficher-les-extensions-et-les-fichiers-caches-sous-windows/#){target='__blank' rel='noopener'} !).
    
    Une fois cela fait, vous pourrez voir cette image dans une visionneuse
    supportant ce format (c'est par exemple le cas d'[IrfanView](https://www.irfanview.com/){target='__blank' rel='noopener'}
    sous Windows, qui est un logiciel gratuit pour un usage personnel, mais
    qui n'est pas _libre_). [IrfanView existe en version portable][irfanview-portable]{target='__blank' rel='noopener'}
    (qui ne nécessite pas d'installation).
    
    Sous GNU/Linux, la visionneuse [Eye of Gnome, qui existe également en
    AppImage][eye-of-gnome-appimage]{target='__blank' rel='noopener'}
    (l'équivalent d'une version portable sous Windows^®^), supporte naturellement
    les fichiers PBM, PGM et PPM. Mais il est probable que le gestionnaire
    de fichier permette directement d'afficher un aperçu de l'image !
    
    Pour modifier _graphiquement_ une telle image, avec les outils traditionnels
    de la retouche photographique, là encore il faut un logiciel de traitement
    d'images compatible, comme par exemple [GIMP][gimp]{target='__blank' rel='noopener'}.

[//]: # (This may be the most platform independent comment)


#### Un immeuble en PGM

![](images/40/immeuble.png){ align=right width=300 }

``` title="Code de l'image « immeuble.pgm »" hl_lines="3"
--8<-- "docs/images/40/reformatage-immeuble.pgm"
```    

??? info "Remarque : comprendre le choix des valeurs utilisées dans le code de l'image"

    La valeur **0** code un pixel **noir** et la valeur **255** code un pixel **blanc** (cela peut paraître contre-intuitif au début !). Il faut comprendre que c'est en fait la quantité de blanc qui est ainsi codifiée : 0 signifie pas de blanc, ==255== (avec une valeur maximale ici définie à 255, cf. ligne 3 du code de l'image) correspond au maximum de blanc possible (donc le pixel est _totalement_ blanc).


#### Le drapeau français en PPM

![](images/40/drapeau-francais.png){ align=right width=300 }

``` title="Code de l'image « drapeau-français.ppm »" hl_lines="3"
--8<-- "docs/images/40/reformatage-drapeau-français.ppm"
```    

??? info "Explications"

    Cette image a une [définition](https://fr.wikipedia.org/wiki/Image_num%C3%A9rique#D%C3%A9finition_et_r%C3%A9solution){target='__blank' rel='noopener'}
    de 6 × 3 = 18 pixels. Chaque pixel voit sa couleur codée par 3 nombres :
    le 1^er^ pour le niveau de rouge, le 2^nd^ donne le niveau de vert, le
    ^3ème^ code pour le niveau de bleu. Chaque couleur primaire dispose
    de 256 nuances différentes, codées de 0 à 255 (cette dernière valeur
    étant précisée à la ==ligne 3== du code de l'image, ci-dessus).
    
    * Les deux premières colonnes de pixels de l'image, de couleur bleue,
    correspondent donc à deux enchaînements des nombres 63, 63 et 255 (le
    triplet $(63, 63, 255)$ code pour la couleur bleue que vous pouvez observer
    ci-dessus).
    
    * Les deux  colonnes suivantes sont blanches, correspondent donc à deux
    suites du triplet $(255, 255, 255)$ qui code la [« couleur » blanche](https://fr.wikipedia.org/wiki/Blanc){target='__blank' rel='noopener'}.
    
    * Les deux dernières contiennent deux fois le triplet $(255, 63, 63)$
    qui code le route du drapeau.


### Remarque importante sur l'organisation interne du codage des images

<center>
:warning: **La disposition des codes en grille n'est pas obligatoire** :warning:
</center>
<span id="gimp28"></span>

=== "Variante 1"

    L'image montrant le _smiley_, rencontrée précédemment, peut également voir
    son code organisé ainsi :
    ```  title="Code alternatif de l'image « smiley.pbm »"
    --8<-- "docs/images/40/smiley-une-ligne.pbm"
    ```

=== "Variante 2"

    L'image montrant le _smiley_, rencontrée précédemment, peut également voir
    son code organisé... un peu n'importe comment !
    ```  title="Autre code alternatif de l'image « smiley.pbm »"
    --8<-- "docs/images/40/smiley-quelconque.pbm"
    ```

=== "Variante « GIMP v. 2.8 »"

    GIMP, en version 2.8, exporte en PBM l'image montrant le _smiley_, rencontrée
    précédemment, en produisant le code suivant :
    ```  title="Code « GIMP v. 2.8 » de l'image « smiley.pbm »"
    --8<-- "docs/images/40/smiley-gimp-2.8.pbm"
    ```

=== "Variante « GIMP v. 2.10 »"

    GIMP, en version 2.10 (la dernière en date) exporte en PBM l'image montrant
    le _smiley_, rencontrée précédemment, en produisant le code suivant :
    ```  title="Code « GIMP v. 2.10 » de l'image « smiley.pbm »"
    --8<-- "docs/images/40/smiley-gimp-2.10.pbm"
    ```

!!! warning "Remarque importante"

    GIMP, lorsqu'il exporte des images vers les formats PGM et PPM (en codage
    ASCII, *pas* en mode RAW), organise les données des fichiers en mettant
    **une seule valeur par ligne.**  
    Cette spécificité sera utilisée dans la suite, pour simplifier nos premières
    manipulations informatisées d'images.
   

### À vous !

!!! note "À faire vous-même"

    === "Vos objectifs"

        À l'aide d'un **éditeur de textes**[^TraitementTextes] de votre choix
        (**pas** un traitement de textes), créez une **image simple**, d'une
        taille maximale de 32 × 32 pixels. Suggestions :

        * un jeu de morpion ou un motif géométrique en noir et blanc ;
        * une voiture, un bateau ou un livre en niveaux de gris ;
        * un autre drapeau, une maison ou une fleur en couleurs...
    
    === "Indications"
    
        Pour les images en niveaux de gris ou de couleurs, rien ne vous
        empêche de donner dans le [pixel art](https://fr.wikipedia.org/wiki/Pixel_art){target='__blank' rel='noopener'} !


## Manipulation d'images en nuances de gris

### Importants détails d'implémentation

Dans un premier temps, les programmes que vous allez créer exploiteront
la particularité décrite dans la section précédente nommée « [Remarque importante
sur l'organisation interne du codage des images](#remarque-importante-sur-lorganisation-interne-du-codage-des-images) ».

On a vu que, lorsqu'on utilise le codage ASCII pour les valeurs numériques
correspondant aux « couleurs », il était possible de positionner les codes
à peu près n'importe comment : sur une seule ligne, séparés par des espaces,
ou sur plusieurs lignes (séparées par des caractères « saut de ligne »,
bien sûr), ou n'importe quel mélange de ces deux organisations.

<center>
**On va donc <u>dans un premier temps</u>  
:warning:  faire la supposition que chaque code  :warning:  
se situe sur <u>une seule</u> ligne**  
</center>

Quitte à utiliser un programme modifiant l'organisation du code des images pour parvenir à cette situation[^modif-orga-code-image], on peut donc supposer que _toutes les images_ seront codées comme dans la [variante « GIMP v. 2.8 » vue ci-dessus](#gimp28). Parcourir l'image revient alors à lire le fichier ligne à ligne : c'est facile !

<center>
**Dans la suite on ne  
:warning:       travaillera qu'avec des       :warning:  
images en <u>niveaux de gris</u>**
</center>

En effet, cela sera nécessaire pour certaines des opérations que vous serez
amené à coder. Et qui peut le plus peut le moins ! Le format PGM pouvant
coder différentes nuances de gris (mélange de noir et de blanc), allant du noir
(absence de blanc) au blanc (que du blanc), il suffit donc pour coder une
image noir et blanc de n'utiliser aucun code intermédiaire.  
**Rappel :** en PGM, le code correspond à la quantité de blanc de la nuance.
Ainsi, la valeur `#!python 0` code le noir et, dans une image pouvant présenter
256 nuances de gris, la valeur `#!python 255` code le blanc

        
### Ouvrir un fichier (en lecture et/ou écriture)

Vous aurez besoin de savoir manipuler les fichiers. Voici un rappel des commandes les plus utiles. Pour commencer, voyons comment ouvrir **proprement**
un fichier (et donc, ici, un fichier dont le contenant est le code PGM d'une image) :

```python
with open("image.pgm", "r") as fichier_image :
    ... # Ici, un code doit faire quelque chose d'utile.
    ... # Attention au décalage des lignes qui suivent : il est impératif !
```

!!! warning "À savoir"

    Le symbole `#!python "r"` ici, deuxième argument de la fonction `#!python open(...)` (après le nom du fichier auquel on souhaite accéder), détermine
    le **mode d'ouverture** du fichier : il précise la façon dont le fichier est utilisé.
    
    * `#!python "r"` signifie « read » : on ouvre le fichier en **lecture <u>seule</u>**.
    
    * `#!python "w"` signifie « write » : on ouvre le fichier en **écriture
    <u>seulement</u>** (un fichier existant portant le même nom sera alors
    _écrasé_).
    
    * `#!python "a"` signifie « append » :  on ouvre le fichier en **mode
    ajout** (toute donnée écrite dans le fichier est automatiquement ajoutée
    à sa fin).
    
    * `#!python "r+"` ouvre le fichier en mode lecture/écriture.
    
    * Enfin, l'argument mode est optionnel, sa valeur par défaut est `#!python "r"`.

??? info "Remarque : attention à l'encodage !"

    On devrait toujours s'assurer de l'encodage d'un fichier qu'on souhaite
    ouvrir en lecture (ou en écriture) avant de le faire pour de bon. Ici,
    il serait donc raisonnable d'écrire `#!python open("image.pgm", "r", encoding="ascii")`, mais cela pourrait en fait s'avérer problématique
    si le fichier en question, bien que le code des couleurs soit réalisé en ASCII, intègre également des commentaires en UTF-8 ! Essayez par exemple d'ouvrir [ce fichier PBM](images/40/smiley-commentaire-utf8.pbm) avec l'encodage ASCII, pour voir...
    

**L'avantage de la syntaxe** « `#!python open(...)` » est de ne pas avoir à se préoccuper de **fermer le fichier <u>après</u> avoir travaillé dessus** (oublier de le faire peut conduire à des incohérences dans son contenu lorsque le fichier est ensuite
ré-ouvert en écriture[^ecritures-differees]).

??? done "Remarque : travailler avec plusieurs fichiers simultanément"

    Il est possible de travailler simultanément avec **plusieurs** fichiers
    sans compliquer la syntaxe avec de multiples emplois de la fonction
    `#!python with ...` qui amèneraient autant de décalages. Le code
    ci-dessous vous montre comment faire.

    ```python
    with open("a.pnm", "r") as a, open("b.pnm", "r") as b, \
         open("c.pnm", "w") as c:
        ...
        # Notez le "w", qui permet d'ouvrir un fichier en écriture.
        # Notez aussi la façon d'écrire une longue commande qui ne
        # tient pas sur une seule ligne, avec un « \ » qui précède
        # un saut de ligne !
    ```
    
    À comparer avec :
    
    ```python
    with open("a.pnm", "r") as a:
        with open("b.pnm", "r") as b:
            with open("c.pnm", "w") as c:
            ...
    ```
    qui est bien plus pénible...


### Lire le contenu d'un fichier

Une fois le fichier ouvert, on doit le lire — et on a supposé qu'il suffirait
de le faire **ligne à ligne**, chaque valeur numérique codant pour chaque
pixel se trouvant seule sur une ligne (rappel : on ne s'intéresse pour le
moment qu'à des images en niveaux de gris, chaque pixel est donc codé par
une seule valeur).

Il faudra bien sûr récupérer et traiter à part les informations présentes
sur les premières lignes du fichier :

* le type de l'image, codé sur la ligne n°1 ;
* la largeur et la hauteur de l'image, codées sur la ligne n°2 ;
* la valeur maximale qui code un pixel totalement blanc ;
* **il faut bien sûr ignorer tout commentaire !**
 
Après, une simple boucle non bornée peut convenir (rappel : parler de boucle
non bornée signifie qu'on ne sait pas à l'avance combien de fois on va passer
dans la boucle). En Python, une telle boucle s'obtient avec l'instruction
`#!python while`. Voici deux codes possibles.

=== "Une approche convenable"

    Un « drapeau » `#!python pas_fini` est créé, avec la valeur initiale
    `#!python True`, qui signifie qu'on n'a pas fini de parcourir toutes
    les lignes du fichier. Lorsque la ligne suivante s'avère être une ligne
    vide, on met ce drapeau à `#!python False` et la boucle `#!python while`
    se termine.
    ```python
    pas_fini = True
    while pas_fini:
        ligne = c.readline()
        if ligne == "":
            pas_fini = False
    ```

=== "Une approche moins élégante"

    Cette façon de faire est souvent considérée comme moins « propre »,
    mais savoir utiliser l'instruction `#!python break` peut s'avérer utile...

    ```python
    while True :
        ligne = c.readline()
        if ligne == "":
            break
    ```

!!! done "À retenir : lire un fichier ligne par ligne ou en entier ?"

    Notez la syntaxe **`#!python descripteur_fichier.readline()`** qui permet
    de récupérer le contenu d'un fichier texte **ligne par ligne.**
    
    Il existe aussi **`#!python descripteur_fichier.readlines()`** (avec un
    « s » final) qui permet de **récupérer en une seule étape l'ensemble du
    contenu d'un fichier texte,** sous forme d'une liste de lignes (chaque
    ligne correspondant à une chaîne de caractères). Cette approche, pour
    plus simple qu'elle puisse paraître, est en fait dangereuse si on ne
    vérifie pas au préalable la taille du fichier dont on récupère ainsi
    le contenu ! Si c'est un gros de fichier, cela peut vite devenir gênant...


### Opérer sur les données acquises depuis un fichier

Une fois les données récupérées ligne par ligne, il faut « travailler » dessus : cela implique une phase de conversion (ou transtypage), car les
données ainsi obtenues sont de type « **chaîne de caractères** »
(_string_ ou `#!python <class str>` en Python). On n'aura jamais affaire,
avec les images PxM, qu'à des entiers : on peut donc forcer la conversion
avec un simple `#!python int(ligne)`, qui a de bonnes chances de fonctionner
avec toute version récente de Python.

**Il reste néanmoins <u>malsain</u> de traiter ainsi une chaîne sans avoir
cherché au préalable à la débarrasser d'éventuels [caractères « blancs »](https://docs.python.org/fr/3/reference/lexical_analysis.html#index-23){target='__blank' rel='noopener'} :**
des caractères invisibles mais bien présents. Par exemple, le caractère
« saut de ligne » en Python, représenté par `#!python "\n"`, ne laisse aucune
trace à l'écran, tout comme le `#!python "\r"` (nouvelle ligne), `#!python "\t"`
(tabulation horizontale), `#!python "\v"` (tabulation verticale) ou
`#!python "\f"` (saut de page).  
**Pour enlever tout caractère invisible qui figurerait avant ou après une
valeur, on utilisera la [méthode `strip()` des chaînes de caractères](https://docs.python.org/fr/3.10/library/stdtypes.html?highlight=strip#str.strip){target='__blank' rel='noopener'} : `#!python chaine.strip()`.**

**Après la conversion en entier, on peut enfin opérer dessus :** cela signifie
réaliser des opérations mathématiques qui vont **transformer** l'image.
À l'aide de ces opérations, on pourra effectuer un recadrage, une rotation,
des échanges de couleurs (le noir devient blanc et réciproquement). On pourra
également flouter l'image, renforcer les contours (c'est plus dur !), etc.

Enfin, il faudra écrire les résultats de ces calculs dans un fichier-destination
(qui doit être un **autre** fichier, préalablement ouvert en écriture donc).

!!! warning "Pour rester cohérent !"
    **Attention :** pour avoir une valeur par ligne dans les nouveaux fichiers générés par nos futurs programmes (comme dans les fichiers-sources),
    **il faudra que chaque ligne écrite se termine impérativement par le
    [caractère de saut de ligne](https://fr.wikipedia.org/wiki/Saut_de_ligne){target='__blank' rel='noopener'} `#!python "\n"`.**


### Première mise en pratique

Voici un programme simple mettant en œuvre les points évoqués ci-avant.
Lisez-le, en particulier les commentaires qui l'émaillent.

```python title="Code mystère"
--8<-- "docs/codes/40/nb2bn.py"
```

!!! note "À faire vous-même (:star:)"

    === "Vos objectifs"
    
        1. Déterminer son effet (dit autrement : quel est l'action de ce
        programme sur une image PGM ?).
        2. Intégrer une vérification de type de l'image traitée.
        3. :warning: **Difficile :** modifier le code de sorte à ce qu'il
        comporte une vérification de ce qu'une ligne n'est pas un commentaire
        (c'est-à-dire qu'elle ne débute pas par le caractère `#!python "#"`),
        avec éradication des commentaires éventuels. Vous ajouterez pour
        cela une fonction `#!python est_commentaire(ligne)`, prenant en
        paramètre une `#!python ligne` quelconque et renvoyant un booléen
        (`#!python True` si `#!python ligne` est un commentaire,
        `#!python False` sinon).
        
    === "Indications"

        1. Appliquez **à la main** le code fourni à cette [très petite image](images/40/croix.pgm).
        2. Il s'agit de vérifier si la première ligne du fichier-image indique
        `#!python P2`...
        2. La difficulté vient de ce que la présence d'un commentaire d'en
        l'entête du fichier-image modifie le nombre de lignes qui le composent...
        Vous pourrez tester votre amélioration sur
        [cette image](images/40/croix-avec-commentaires.pgm), qui comporte
        _plusieurs_ commentaires.

    === "Solutions"
    
        1. Ce programme convertit le blanc en noir et réciproquement. On
        dit qu'on « inverse » les « couleurs » de l'image.
        2. Voici un code appelé `nb2bn.py` qui convient.

            ```python
            --8<-- "docs/codes/40/nb2bn-v2.py"
            ```

### Applications

!!! note "Création informatique d'images (:star: à :star::star::star:)"

    :warning: Pensez **toujours** à vérifier les préconditions à l'aide d'assertions
    et à documenter vos fonctions à l'aide d'une _docstring_ !

    === "Vos objectifs"
    
        1. :star: Écrire une fonction `#!python rectangle_vide(...)` qui prend en
        paramètres un nom de fichier d'extension PGM, deux entiers strictement
        positifs `#!python largeur` et `#!python hauteur`, un 3^e^ entier
        indiquant le nombre de nuances de gris pris en charge, ainsi qu'un
        nombre décimal (compris entre 0 et 1) codant pour un niveau de gris.
        La fonction fabriquera un fichier image (au format PGM, donc), contenant
        un rectangle dont le tour sera noir et l'intérieur coloré avec la
        nuance de gris passée en paramètre.
        
        2. :star: Écrire une fonction `#!python demi_carre(...)` qui prend en paramètres
        un nom de fichier d'extension PGM, un entier strictement positif
        `#!python coté`, un 2^e^ entier indiquant le nombre de nuances
        de gris pris en charge, ainsi qu'un nombre décimal codant pour un
        niveau de gris. La fonction fabriquera un fichier image (au format
        PGM, donc), contenant un carré dont une partie « sous une diagonale »
        sera noire, l'autre partie « au-dessus de la diagonale » sera colorée
        avec la nuance de gris passée en paramètre.
        
        3. :star::star: Écrire une fonction `#!python immeuble(...)` qui
        crée un fichier image (en niveau de gris) représentant un immeuble
        d'aspect rectangulaire, d'une taille personnalisable, doté de fenêtres
        noires positionnées régulièrement, et d'une porte d'entrée noire
        également (positionnement à volonté : toujours identique, définissable,
        aléatoire...). La couleur de l'immeuble sera également personnalisable.
        
        4. :star::star: Écrire une fonction `#!python maison(...)` qui
        crée un fichier image (en niveau de gris) représentant une maison
        avec un toit présentant (au moins en partie) une pente, d'une taille
        personnalisable, dotée de fenêtres noires positionnées régulièrement,
        et d'une porte d'entrée noire également (positionnement à volonté).
        La couleur de l'immeuble sera également personnalisable.
        
        5. :star::star::star: Écrire une fonction `#!python disque(...)`
        qui prend en paramètres un nom de fichier d'extension PGM, un entier
        strictement positif `#!python rayon`, un 2^nd^ entier indiquant
        le nombre de nuances de gris pris en charge, ainsi qu'un nombre
        décimal (compris entre 0 et 1) codant pour un niveau de gris. La
        fonction fabriquera un fichier image (au format PGM, donc), contenant
        un disque dont le bord sera noir et l'intérieur coloré avec la nuance
        de gris passée en paramètre.

    === "Illustration des résultats souhaités"
    
        Un jour, peut-être...
    
    === "Indications"
    
        Un jour, peut-être...
    
    === "Solutions"
    
        Un jour, peut-être...

!!! note "À faire vous-même : éclaircir / assombrir (:star:)"

    === "Vos objectifs"
    
        1. Écrire un programme appelé `assombrir.py` qui **diminue** la
        quantité de blanc d'une image PGM (pas nécessairement une image
        en noir et blanc, votre code devra être en mesure de gérer une image
        en niveaux de gris). Vous écrirez une fonction `#!python transformation()` qui modifiera la valeur codant chaque pixel.
        
        2. Écrire un programme appelé `éclaircir.py` qui **augmente** la
        quantité de blanc d'une image PGM (pas nécessairement une image
        en noir et blanc, votre code devra être en mesure de gérer une image
        en niveaux de gris). Vous écrirez une fonction `#!python transformation()` qui modifiera la valeur codant chaque pixel.
        
        3. Vous penserez à doter vos fonctions d'une chaîne de documentation (_docstring_) adaptée, et vous vérifierez les préconditions à l'aide
        d'assertions bien choisies.
    
    === "Illustration des résultats souhaités"
    
        <center>
        
        | [Image d'origine](images/40/montagne.pgm) | [Image assombrie<br />_(niveau de blanc divisé par 2)_](images/40/montagne-plus-sombre.pgm) | [Image éclaircie<br />_(niveau de blanc multiplié par 2)_](images/40/montagne-plus-claire.pgm) |
        |:----:|:---:|:---:|
        | [![](images/40/montagne.png)](images/40/montagne-nb-seuil-0.5.pgm) | [![](images/40/montagne-plus-sombre.png)](images/40/montagne-plus-sombre.pgm) | [![](images/40/montagne-plus-claire.png)](images/40/montagne-plus-claire.pgm) |
        
        </center>
    
    === "Indications"
    
        1. Dans une image PGM présentant 256 nuances de gris, le noir total
        est codé par `#!python 0` et le blanc pur par un `#!python 255`.
        Diminuer la quantité de blanc signifie donc baisser la valeur qui
        code chaque pixel, tout en la maintenant au minimum à 0.  
        Quelle vous semble être la meilleure façon d'y parvenir : soustraction
        ou... ? Trouvez une alternative à la soustraction d'une valeur constante
        et discutez de ses avantages.  
        À quel(s) autre(s) point(s) faut-il également prêter attention ?
        
        2. Pour éclaircir une image, il faut faire le contraire de ce qui
        précède : augmenter la valeur codant pour chaque pixel de l'image.
        Mais la situation est-elle exactement symétrique ? Les deux mêmes
        techniques peuvent-elles être envisagées ?  
        À quel(s) autre(s) point(s) faut-il également prêter attention ?
    
    === "Solutions"
    
        1. On **assombrir** une image PGM, il faut **diminuer** la valeur codant
        chaque pixel. Pour cela, on peut soustraire à chacune de ces valeurs
        une quantité identique, ou on peut la multiplier par un coefficient compris entre 0 et 1 (exclus).  
        Il faut en outre s'assurer de deux choses :
        
            * le résultat doit être entier ;
            * il doit être compris entre 0 et la valeur maximale codées
            dans le fichier-image.
        
        2. Pour **éclaircir** une image PGM, il faut **augmenter** la valeur
        codant chaque pixel. Pour cela, on peut ajouter à chacune de ces valeurs une quantité identique, ou on peut la multiplier par un coefficient supérieur à 1 (exclus). Il faut là aussi vérifier les
        deux points précédemment évoqués.
        
        3. Voici le code du script `assombrir.py`. À vous de vous débrouiller
        pour en dériver `éclaircir.py`. Veuillez prêter attention aux
        commentaires qui figurent dans ce code !
        
        ```python
        --8<-- "docs/codes/40/assombrir.py"
        ```
        
    
!!! note "À faire vous-même : convertir une image en niveaux de gris en une image en noir & blanc (:star::star:)"

    === "Votre objectif"
    
        Écrire un script `ng2nb.py` qui convertisse une image en niveaux
        de gris en une image en noir et blanc (mais toujours enregistrée
        au format PGM, dans un souci de simplicité).  
        Un seuil, fixé sous forme d'une variable définie au début du code,
        sera utilisé :
        
        * les valeurs codant un pixel qui seront inférieures ou égales à
        ce seuil seront transformées en `#!python 0` ;
        * les valeurs supérieures à ce seuil seront transformées en la
        valeur maximale (qu'il faudra donc récupérer depuis le code de
        l'image d'origine).
    
    === "Illustration du résultat souhaité"
    
        <center>
        
        | [Image d'origine](images/40/montagne.pgm) | [Image transformée en N&B avec un seuil à 127](images/40/montagne-nb-seuil-0.5.pgm) |
        |:----:|:---:|
        | [![](images/40/montagne.png)](images/40/montagne-nb-seuil-0.5.pgm) | [![](images/40/montagne-nb-seuil-0.5.png)](images/40/montagne-nb-seuil-0.5.pgm)  |
        
        </center>
    
    === "Indication"
    
        Aucun besoin d'indication supplémentaire ici. Vous disposez de
        tout le nécessaire si vous avez réussi l'exercice précédent.
        
    === "Solution"
    
        ```python
        --8<-- "docs/codes/40/ng2nb.py"
        ```

!!! note "À faire vous-même 3 : inversion des couleurs (:star::star:)"

    === "Votre objectif"
    
        Écrire un script `inverser.py` qui convertisse une image en niveaux
        de gris en l'image « complémentaire ». Concrètement, et sous réserve
        que l'image en niveau de gris soit codée avec 256 nuances, cela signifie
        que :
        
        * le code `#!python 0` (codant pour le noir) sera changé en le code
        `#!python 255` (codant pour le blanc) ;
        * le code `#!python 1` sera changé en `#!python 254` ;
        * le code `#!python 2` sera changé en `#!python 253` ;
        * etc. ;
        * le code `#!python 254` sera changé en `#!python 1` ;
        * le code `#!python 255` sera changé en `#!python 0`.
    
    === "Illustration du résultat souhaité"
    
        <center>
        
        | [Image d'origine](images/40/montagne.pgm) | [Image inversée](images/40/montagne-inversée.pgm) |
        |:----:|:---:|
        | [![](images/40/montagne.png)](images/40/montagne.pgm) | [![](images/40/montagne-inversée.png)](images/40/montagne-inversée.pgm)  |
        
        </center>
    
    === "Indication"
    
        Il faudra mettre en évidence la fonction qui permet de faire les
        transformations des valeurs (la solution la plus simple repose sur
        une fonction affine !).
        
    === "Solution"
    
        ```python
        --8<-- "docs/codes/40/inverser.py"
        ```
??? question "Pour aller plus loin..."

    À votre avis, le code précédent peut-il fonctionner avec une image en
    couleurs ? Si non, que doit-on modifier ? Quel serait le résultat ?
    
    ??? ok "Illustration"
    
        <center>
        
        | [Image d'origine](images/40/poterie-indigène-australie.ppm) | [Image inversée](images/40/poterie-indigène-australie-inversée.ppm) |
        |:----:|:---:|
        | [![](images/40/poterie-indigène-australie.jpg)](images/40/poterie-indigène-australie-inversée.ppm) | [![](images/40/poterie-indigène-australie-inversée.jpg)](images/40/poterie-indigène-australie-inversée.ppm)  |
    
        </center>


### Des exercices plus compliqués

Je vous propose dans cette section d'écrire des programmes qui réalisent
des tâches plus complexes :

* flouter une image ;
* renforcer les contrastes (ce qui va « durcir » l'image).

Pour y parvenir, on va devoir **effectuer des manipulations numériques sur
plusieurs pixels voisins.** Il va donc falloir recourir à une structure
de données capable de mémoriser **toutes** les valeurs codant les pixels
de l'image : un **tableau à deux dimensions** aussi appelé **matrice**.

**Il existe deux façons de créer un tel tableau ou matrice :**

* la plus efficace serait d'utiliser l'objet [`#!python numpy.array`](https://numpy.org/doc/stable/reference/generated/numpy.array.html){target='__blank' rel='noopener'},
de la puissante bibliothèque `numpy`. Mais cela nous entraînerait trop loin ;
* la plus simple est de fabriquer nous-même un pseudo-tableau à 2 dimensions
en utilisant une liste de listes (en outre, c'est pédagogiquement intéressant
en ce que cela vous fera travailler toujours plus le parcours de listes).
**C'est donc cette dernière approche qu'on va retenir !**

??? example "Exemple de liste de listes"

    Une liste de listes est juste une liste dont chaque élément est à son
    tour une liste. Créer une liste de liste est facile.
    
    * Directement :
        ```pycon
        >>> matrice = [ [1, 2, 3, 4],
                        [5, 6, 7, 8]
                        [9, 0] ]      # Pas obligation d'harmonisation des tailles !
        >>> matrice[2]                # Le 1er indice fourni donne le n° de la ligne.
        [9, 0]
        >>> matrice[2][1]             # Le 2nd indice fourni donne celui de la colonne.
        0
        ```
    * Par compréhension :
        ```pycon
        >>> matrice = [ [ 10*j + i for i in range(3) ] for j in range(2) ]
        >>> matrice
        [[0, 1, 2], [10, 11, 12]]     # Ici les tailles des sous-listes sont identiques.
        >>> matrice[1]                # Le 1er indice fourni donne le n° de la ligne.
        [10, 11, 12]
        >>> matrice[1][0]             # Le 2nd indice fourni donne celui de la colonne.
        10
        ```

!!! warning "IMPORTANT : subtilité pour accéder ou modifier une liste de listes"
        
    L'exemple précédent montre que **pour accéder à une case dont le numéro
    de colonne est `i`** (l'abscisse, en quelque sorte) **et le numéro de
    ligne est `j`** (l'ordonnée, mais avec un axe des ordonnées orienté
    _vers le bas_),  **il faut saisir `#!python valeur = matrice[j][i]`**, ce qui
    est contraire à l'intuition (qui, en l'occurrence, vient directement
    du poids des habitudes).
    
    De même, pour modifier l'élément du « tableau 2D » situé à la colonne
    `i` et à la ligne `j`, il faudra saisir l'instruction `#!python matrice[j][i] = nouvelle_valeur`.

!!! note "À faire vous-même : flouter une image (:star::star::star:)"

    === "Vos objectifs"
    
        Écrire un script `flouter.py` qui... floute une image en niveaux
        de gris. L'idée est, pour chaque pixel de « coordonnées `(i, j)` »
        (colonne `i`, ligne `j`), de **calculer la moyenne des codes des
        pixels environnants** (dans un voisinage qui s'étend de façon symétrique
        aussi bien en largeur qu'en hauteur, de part et d'autre de la « case »
        de « coordonnées » `(i, j)`). **La valeur codant le pixel `(i, j)` est alors remplacée par la moyenne précédemment calculée** (et qui doit donc
        être entière).
        
        Vous veillerez à écrire plusieurs fonctions, pour rendre le
        code plus lisible et maintenable.
        
        * `#!python obtenir_matrice(fichier)` : prend en paramètre une chaîne de
        caractères désignant le nom du fichier source (une image PGM).
        
            Valeur de retour : un tuple. Il contiendra, dans l'ordre :
            
            - une matrice (liste de liste) contenant les valeurs de chaque pixel ;
            - la largeur de cette matrice (et donc de l'image) ;
            - la hauteur de cette matrice ;
            - la valeur maximale codant le blanc pur.
        
        * `#!python voisinage(x, y, distance, i_max, j_max)` : prend en
        paramètres le numéro de colonne puis de ligne d'un pixel de l'image,
        la distance qui définira la taille du voisinage (dans les 4 directions :
        à gauche, à droite, en haut et en bas), puis les dimensions de la
        matrice (et donc de l'image), d'abord la largeur et ensuite la hauteur.
        
            Valeur de retour : une liste de tuples correspondant aux coordonnées
            de chaque case voisine de la case dont les coordonnées ont été
            passées en paramètres.
        
        * `#!python floute(matrice, distance, largeur, hauteur)` : prend
        en argument une matrice (une liste de listes correspondant aux codes
        des pixels de l'image d'origine), la distance sur laquelle s'appliquera
        le flou, ainsi que les dimenstions de l'image (et donc de la matrice).
        
            Valeur de retour : une matrice contenant de nouvelles valeurs pour
            chaque pixel, correspondant à une image plus floue.
        
        * `#!python enregistre_image_PGM(fichier_dst, matrice, largeur,
        hauteur, valeur_max)` : prend en paramètres une chaîne de caractères
        (le nom du fichier de destination), une matrice (les codes de chaque
        pixel de la nouvelle image), les dimensions de cette image ainsi
        que le code correspondant à la couleur blanche.
        
            Aucune valeur de retour.
    
    === "Illustration du résultat souhaité"
    
        <center>
        
        | [Image d'origine](images/40/montagne.pgm) | [Image floutée](images/40/montagne-floue.pgm) |
        |:----:|:---:|
        | [![](images/40/montagne.png)](images/40/montagne.pgm) | [![](images/40/montagne-floue.png)](images/40/montagne-floue.pgm)  |
        
        </center>
    
    === "Indications"
    
        Voici une illustration de ce qu'on entend par « voisinage » d'une
        « case » de « coordonnées » `(i, j)` (ce qui signifie qu'on regarde
        la valeur codant le pixel situé à la colonne n°`i` de la ligne n°`j`).
        Deux voisinages différents sont représentés ci-dessous :
        
        <center>
        ![](images/40/comment-flouter.svg)
        </center>
        
        * **Un voisinage avec une distance de 1** : il est constitué des
        « cases » de couleur mauve (et il _inclut_ la case jaune). Le calcul
        de moyenne correspondant sera donc :
        
            $\bar{x} = \dfrac{45 + 2 + 30 + 88 + 112 + 52 + 40 + 144 + 125}{9} \approx 70\!,9$ donc $\lfloor \bar x \rfloor = 70$.
            
            **La valeur `#!python 112` sera donc remplacée par la valeur `#!python 70`.**
        
        * **Un voisinage avec une distance de 2** : il est constitué des
        « cases » de couleur mauve (et il _inclut_ les cases mauves et la
        case jaune). Le calcul de moyenne correspondant sera donc :
        
            $\bar{x} = \dfrac{52 + 125 + 250 + 255 + 0 + 187 + 45 + 2 + 30 + 50 + 101 + 88 + 112 + 52 + 120 + 10 + 40 + 144 + 125 + 151 + 125 + 95 + 145 + 97 + 123}{25} = 100\!,96$ donc $\lfloor \bar x \rfloor = 100$.
            
            **La valeur `#!python 112` sera donc remplacée par la valeur `#!python 100`.**
        
        ??? info "Remarque"
            
            La moyenne étant toujours comprise entre les valeurs extrêmes,
            on ne risque pas d'obtenir ici un résultat négatif ou qui
            dépasserait la valeur maximale codant le blanc (`#!python 255`
            dans l'illustration précédente).
            
        ??? note "Base de code"
        
            Notez, ligne 38 et 39, la façon dont on parvient à convertir
            la variable `compteur` (qui compte le nombre de lignes du fichier,
            et donc le nombre de pixels parcourus, en balayant l'image du
            coin supérieur gauche au coin inférieur droit), en un numéro
            de colonne `i` et un numéro de ligne `j` grâce à la division
            entière (ou _euclidienne_) `//` et à l'opérateur « modulo »
            (reste de la division euclidienne) `%`).
            
            Les _docstrings_ ont été retirées pour plus de concision...
            
            ```python
            # Attention, ce programme est prévu pour travailler à partir d'une image
            # PGM générée par GIMP, qui semble toujours metre chaque valeur sur une
            # ligne à part !
            fichier_src = "src.pgm"  # Fichier d'origine.
            fichier_dst = "dst.ppm"  # Fichier produit en sortie.

            def obtenir_matrice(fichier):
                with open(fichier_src, "r") as src:
                    # 1ère ligne : le type (on suppose que tout est OK et conforme : aucun test !)
                    src.readline()      # On "consomme" la ligne sans vérification.
                    
                    # 2ère ligne : Gimp met toujours un commentaire. On décide de l'ignorer...
                    # Il faut pourtant "consommer" cette ligne pour passer à la suivante !
                    src.readline()
                    
                    # 3ème ligne : la taille de l'image (on suppose que les dimensions sont
                    # égales, là encore aucun test : tout est censé être OK !).
                    ligne_src = src.readline()
                    largeur, hauteur = ligne_src.split() # On a une liste de 2 chaînes ici.
                    # Bien lire la doc de la méthode split ! Dans Idle, faire : help("".split)
                    largeur = int(largeur)                  # Conversion en entier.
                    hauteur = int(hauteur)
                    
                    # 4ème ligne : la finesse du codage, qu'on récupère.
                    valeur_max = int(src.readline())

                    # On crée une structure capable d'accueillir l'ensemble des valeurs codant
                    # le niveau de gris de chaque pixel : une liste de liste.
                    pixels_image = [ [ 0 for i in range(largeur)] for j in range(hauteur) ]

                    # On attaque la récupération des données de chaque pixel de l'image...
                    compteur = 0
                    while True:
                        ligne_src = src.readline()
                        if ligne_src == "":  # On est à la fin du fichier : on quitte !
                            break
                        else:
                            i = compteur % largeur      # « L'abscisse » du pixel.
                            j = compteur // largeur     # « L'ordonnée » du pixel.
                            valeur = int(ligne_src)     # La valeur codant le pixel.
                            pixels_image[j][i] = valeur # Piège ici : d'abord l'ordonnée !
                        compteur += 1
                return pixels_image, largeur, hauteur, valeur_max


            def voisinage(x, y, distance, i_max, j_max):
                voisinage = [(x+i, y+j) for i in [ _ for _ in range(..., ...)]
                                        for j in [ ... ]
                             if ...                 # On ne regarde pas la case courante
                             and ... and 0 ... ]    # On ne déborde pas
                return voisinage


            def floute(matrice, distance, largeur, hauteur):
                pixels_image_floue = [ [ 0 for i in range(largeur)] for j in range(hauteur) ]
                for j in range(hauteur):
                    print("Ligne {} / {} traitée".format(j, hauteur))
                    for i in range(largeur):
                        ...
                return pixels_image_floue

            def enregistre_image_PGM(fichier_dst, matrice, largeur, hauteur, valeur_max):
                ...

            image, L, H, blanc = obtenir_matrice(fichier_src)
            print("Image d'origine chargée. Début du floutage...")
            image_floue = floute(image, 2, L, H)
            print("Floutage effectué. Enregistrement...")
            enregistre_image_PGM(fichier_dst, image_floue, L, H, blanc)
            print("L'image", fichier_dst, "est créée !")
            ```
        
    === "Solution"
    
        ```python
        --8<-- "docs/codes/40/flouter.py"
        ```

!!! note "À faire vous-même : « durcir » une image (:star::star::star:)"

    === "Vos objectifs"
    
        Écrire un script `durcir.py` qui renforce la netteté d'une image
        en niveaux de gris. Vous reprendez l'organisation générale décrite
        dans l'exercice précédent.
        
        La fonction « durcissant », que vous nommerez... `durcir` reposera
        sur une logique un peu plus compliquée. Pour chaque pixel de « coordonnées
        `(i, j)` » (colonne `i`, ligne `j`) :
        
        1. vous **calculerez la moyenne des codes des pixels environnants**
        (même principe qu'à l'exercice précédent) ;
        2. vous calculerez la **différence entre la valeur du pixel et
        cette moyenne.**
        
            * **si le résultat est strictement négatif,** une fraction de
            de la _valeur absolue_ de ce résultat sera retirée à la valeur
            du pixel (80 %, mettons) ;
            * **si le résultat est positif ou nul,** une fraction de ce
            résultat sera ajoutée à la valeur du pixel (20 %, mettons).
        
        3. **la valeur codant le pixel `(i, j)` est alors remplacée
        par le résultat obtenu** à l'étape précédente (qui doit donc
        être entier).
        
        L'idée est de diminuer la quantité de blanc d'un pixel si les pixels
        environnants sont en moyenne plus clairs, et d'augmenter cette
        quantité de blanc si les pixels environnants sont plus sombres.
        Les contrastes sont ainsi renforcés.
    
    === "Illustration du résultat souhaité"
    
        <center>
        
        | [Image d'origine](images/40/montagne.pgm) | [Image « durcie »](images/40/montagne-dure.pgm) |
        |:----:|:---:|
        | [![](images/40/montagne.png)](images/40/montagne.pgm) | [![](images/40/montagne-dure.png)](images/40/montagne-dure.pgm)  |
        
        </center>
        
        Notez « l'effet de halo » à la frontière entre la montagne et le ciel.
        Un objectif des algorithmes plus évolués est, entre autre, de limiter
        ces artefacts...
    
    === "Indications"
    
        Voici une illustration de ce qu'on entend par « voisinage » d'une
        « case » de « coordonnées » `(i, j)` (ce qui signifie qu'on regarde
        la valeur codant le pixel situé à la colonne n°`i` de la ligne n°`j`).
        Deux voisinages différents sont représentés ci-dessous :
        
        <center>
        ![](images/40/comment-flouter.svg)
        </center>
        
        * Avec un écartement de 1 pixel, on trouve une valeur moyenne du
        niveau de blanc des pixels du voisinage valant :
        
            $\bar{x} = \dfrac{45 + 2 + 30 + 88 + 112 + 52 + 40 + 144 + 125}{9} \approx 70$.
            
        * La différence $112 - 70 = 42 \geqslant 0$. On va donc **ajouter**
        80 % de 42 à 112 : $112 + \dfrac{80}{100} \times 42 \approx 145$
        (en arrondissant à l'entier inférieur).
        
        * **La valeur `#!python 112` sera donc remplacée par la valeur `#!python 145`.**

        ??? warning "Remarque"
            
            Ici, il va _vraiment_ falloir vérifier que les valeurs calculées
            sont bien positives ou nulles **et** inférieures à la valeur
            qui code le blanc pur. Si les résultats obtenus dépassent ces
            bornes, il faudra les remplacer :
            
            * par `#!python 0` si on obtient un résultat strictement négatif ;
            * par la valeur maximale codant le blanc (`#!python 255` dans
            l'illustration donnée) si le résultat des calculs dépasse cette
            valeur.
            
        ??? note "Base de code"
        
            C'est la même qu'à l'exercice précédent.
        
    === "Solution"
    
        Ah non, là, il va falloir vous débrouiller seul !


## Compression d'images : le codage RLE

![](images/40/lune-eclipse.jpg){width=300, align=right}
Dans certaines images, de nombreux pixels identiques se suivent : c'est
le cas de la photo d'une éclipse de Lune, ci-contre. L'idée du codage RLE,
pour [Run-Length Encoding](https://fr.wikipedia.org/wiki/Codage_par_plages){target='__blank' rel='noopener'},
est d'éviter les répétitions des valeurs codant ces pixels identiques en
ne stockant que la valeur qui se répète ainsi que le nombre de fois où
elle apparaît. 

Naturellement, cette approche sera plus intéressante sur le « tour » de
l'image, complètement noir, que sur la zone montrant la Lune, qui offre
plus de nuances, et donc moins de répétitions de pixels...

On va donc recommencer ici à travailler sur des **images PBM** (donc en
**noir et blanc**) : les répétitions y seront plus nombreuses, et la compression
par codage RLE plus efficace.


### À la main

!!! note "À faire vous-même"

    === "Vos objectifs"
    
        ![](images/40/e-code-rle-fax.png){width=300, align=right}
        Le codage RLE a longtemps été utilisé dans les transmissions par fax. L'image
        ci-contre est codée par les nombres figurant à sa droite.

        1. Quel sens donner au codage employé ?
        2. Quel est le taux de compression de cette image ?
        3. La compression s'effectue-t-elle [avec ou sans pertes](http://igm.univ-mlv.fr/~dr/XPOSE2013/La_compression_de_donnees/types.html){target='__blank' rel='noopener'} ?
    
    === "Indications"
    
        1. Corrélez les nombres et les occurrences des pixels, ainsi que
        l'alternance des couleurs.
        2. Le [taux de compression](https://fr.wikipedia.org/wiki/Taux_de_compression_de_donn%C3%A9es){target='__blank' rel='noopener'}
        possède au moins deux définitions (équivalentes, bien sûr). On peut
        aussi l'exprimer à travers un rapport : lorsqu'on évoque un algorithme
        permettant une compression de « 4 : 1 », on indique que la taille
        du fichier obtenu est divisée par 4 par rapport à celle du fichier
        initial.
        3. Demandez-vous si, dans ce cas précis, de l'information est perdue
        au cours du processus de codage...
        
    === "Solution"
    
        1. Le code repose sur l'alternance blanc puis noir, dans cet ordre.
        Le 1^er^ nombre donne le nombre de pixels blancs qui se suivent,
        le 2^nd^ donne le nombre de pixels noirs qui viennent ensuite, puis
        le 3^e^ code à nouveau un nombre de pixels blancs consécutifs,
        et ainsi de suite.
        2. L'image d'origine comporte 25 pixels, soit 25 octets en admettant
        que la valeur codant le blanc et celle codant le noir sont stockées
        sur 1 octet. Le code obtenu comporte 23 signes (on ne peut pas se
        passer de la virgule, qui permet de séparer sans amiguïté les valeurs
        codant pour les répétitions — il pourrait très bien y avoir plus
        de 9 pixels identiques consécutifs !). Si l'on tient compte des
        caractères de saut de ligne, on obtient en tout 27 signes qui,
        s'ils sont codés sur 1 octet également (aucune difficulté avec le
        code ASCII), donnent un rapport de compression... **inférieur à 1:1 !**
        C'est donc un mauvais exemple :sweat_smile: !
        3. La compression s'effectue sans perte ici, les données après
        décompressions sont rigoureusement identiques aux données avant
        compression.


### Avec un script Python

On suppose ici que l'on ne traitera que des images :warning: **PBM** :warning:
créées par GIMP. Cela signifie :

* que le type est nécessairement `#!python P1` et figure sur la 1^re^ ligne ;
* qu'un commentaire est présent sur la 2^e^ ligne (vous l'ignorerez) ;
* que la largeur et la hauteur sont sur la 3^e^ ligne, séparés d'une espace ;
* que les valeurs des pixles (0 ou 1) suivent alors, et se succèdent sur
des lignes faisant chacune 70 caractères au maximum.

??? example "Exemple d'image PBM produite par GIMP"

    ![](images/40/smiley.png){align=right width=150}
    ``` title="Code de l'image « smiley-gimp-2.10.pbm »"
    --8<-- "docs/images/40/smiley-gimp-2.10.pbm"
    ```

!!! note "À faire vous-même"

    === "Vos objectifs"
    
        Écrire un script `compresser_rle.py` qui :
        
        1. définisse une variable `fichier_src` pointant vers un fichier
        image (supposé conforme, cf. description ci-dessus) ;
        2. ouvre en *écriture** un fichier de destination, qui portera le
        même nom que le fichier source, mais dont l'extension sera `.pbmz`,
        le type `P1z` (et dont l'entête contiendra les dimensions, sur le
        même principe que le fichier PBM) ;
        3. récupère les informations habituelles du fichier source (type,
        dimensions) ;
        4. parcourre chaque ligne du fichier source en comptant le nombre
        de 0 ou de 1 successifs (on commencera systématiquement par considérer
        la valeur 0, quitte à indiquer qu'elle **n'est pas** présente en
        début de ligne !) ;
        5. écrive simultanément, pour chaque ligne du fichier source,
        une ligne dans le fichier de destination dont le contenu sera
        le nombre d'occurences des 0, puis des 1, puis des 0 etc. séparés
        par une virgule `,`.
        
        **Exemple :**
        
        ![](images/40/e.png){align=right width=110}
        ``` title="Code de l'image « e.pbm »"
        --8<-- "docs/images/40/e.pbm"
        ```
        
        Notez les différences avec le codage utilisé pour un fax :
        
        ![](images/40/e-code-rle-fax.png){align=right width=220}
        ``` title="Code de l'image « e.pbmz »"
        P1z
        5 5
        1,3,1,1,3,7,5,3,1
        ```
    
    === "Indications"
    
        * Repartir sur les bases du début de ce document, en consommant
        chacune des 3 premières lignes.
        * Parcourir chaque ligne du fichier jusqu'à rencontrer une ligne
        vide (auquel cas, on a atteint la fin du fichier source).
        * Parcourir chaque ligne en mémorisant les occurences d'une valeur
        dans une liste Python (nommée par exemple `occurrences`).
        * Pour détecter les changements de valeurs, il faudra mémoriser
        la valeur précédentes (par exemple _via_ une variable `valeur_prec`).
        * Pas besoin de convertir les valeurs en entiers !
        * **Rappel :** on considère toujours qu'on commence par la valeur
        0, même si celle-ci ne figure pas en première position d'une ligne.
        Ainsi, on aura, au début du parcours de chaque ligne de `fichier_src`,
        `#!python valeur_prec = "0"` et `#!python occurrences = [0]`.
        
    === "Solution"
    
        ```python
        --8<-- "docs/codes/40/compresser_rle.py"
        ```

    === "Pour aller plus loin"

        1. Comment faire en sorte que le travail du compresseur ne s'arrête
        pas à la fin de chaque ligne du fichier source ?
        2. Adapter votre programme au fichiers PGM. Est-il aussi efficace ?
        Pourquoi selon vous ?
        3. Comment faire en sorte que ce script puisse prendre en argument
        un nom de fichier passé **en ligne de commande** ? Vous pourrez
        vous intéresser au [module `sys`](https://docs.python.org/3/library/sys.html){target='__blank' rel='noopener'}, et plus précisément à `sys.argv`. Le [module `fileinput`](https://docs.python.org/3/library/fileinput.html#module-fileinput){target='__blank' rel='noopener'}
        pourra aussi vous aider.
        4. À l'aide de l'introduction à Tkinter, écrire un programme qui
        permette de visualiser dans un canevas une image PBM compressée.
        5. Enrichir ce dernier programme de sorte à pouvoir choisir une
        image, et à pouvoir se déplacer dans les images présentes dans le
        répertoire courant à l'aide des flèches de direction du clavier.

??? info "Remarque un peu subtile"

    Le codage RLE donne des résultats assez impressionnants lorsqu'une image
    comporte d'importants aplats de « couleurs », mais dès que l'on rencontre
    des zones plus variées, son efficacité va drastiquement diminuer.
    
    En fait, ce sera le cas avec la plupart des images réelles ! C'est
    pourquoi ce codage, pour simple et efficace qu'il puisse parfois être,
    n'est guère utilisé.

    Cela étant, le codage RLE reste souvent employé comme algorithme **supplémentaire**,
    par dessus d'autres algorithmes de compressions avec pertes, comme le
    JPEG par exemple.
    
    En effet, l'on compresse une image avec pertes, on obtient toujours
    une image moins détaillée, avec moins de nuances, ce qui permet donc
    d'augmenter la probabilité d'avoir des répétitions de nuances (en termes
    mathématiques, on dit que _la variance globale des données de l'image
    a baissé_)... Le codage RLE retrouve donc de l'intérêt en deuxième passe,
    après une première compression.


## Stéganographie

### Le principe : cacher une image dans une image

Nous allons écrire un programme qui permettra de cacher une image en noir
et blanc **à l'intérieur** d'une image en niveaux de gris.

Pour simplifier les choses, on supposera :

* que les deux images **auront toujours** exactement **les mêmes dimensions** ;
* que **l'image à cacher** (en noir et blanc, donc), sera enregistrée au
**format PGM**. On pourra ainsi travailler sur deux images PGM, qui auront
donc exactement la même organisation interne (d'où plus de simplicité).

Ces images seront nommées `image-à-cacher.pgm` (c'est celle en noir et blanc)
et `image-conteneur.pgm` (celle en niveaux de gris).

??? question "Comment créer de telles images ?"

    La [réponse est en annexe](#produire-et-visualiser-des-images-pbm-pgm-ou-ppm) !

??? warning "Si vous voulez faire d'autres essais..."
  
    Pour conserver des programmes simples, on partira ici toujours du principe
    que les images auront mêmes dimensions. Si ce n'est pas le cas de celles
    avec lesquelles vous souhaitez travailler, pour pourrez utiliser GIMP
    pour les redimensionner. Pour cela, dans le menu **Image**, cliquez
    sur l'entrée **Échelle et taille de l'image...**, puis choisissez les
    dimensions souhaitées.

    **Conseils :**

    * **ne** cliquez **pas** sur les « maillons » de chaînes, ils permettent
    de conserver les proportions de l'image, c'est-à-dire le rapport largeur
    / hauteur ;
    * vous devriez également conserver l'interpolation cubique pour le redimensionnement
    de l'image ;
    * si vous souhaitez en apprendre un peu plus sur les algorithmes de
    redimensionnement d'images, consultez
    [cette page Wikipedia](https://en.wikipedia.org/wiki/Comparison_gallery_of_image_scaling_algorithms){target='__blank' rel='noopener'},
    et observez l'incroyable efficacité des algorithmes à base de réseaux
    de neurones !  
    [:gb:Cette page](https://www.cambridgeincolour.com/tutorials/image-interpolation.htm){target='__blank' rel='noopener'}
    est également intéressante.


### L'algorithme
 
* On parcourt successivement et simultanément (non, ça n'est pas antinomique,
réfléchissez bien !) _tous_ les pixels de _chacune_ des _deux_ images afin
de les _combiner_ (vu le choix d'images PGM, ce sera facile : chaque valeur
associée occupe une ligne).
* On obtient la valeur codant un pixel de l'image conteneur (un entier compris
**entre** `#!python 0` et `#!python 255`), et on se débarasse de son **chiffre
des unités**. Ainsi, la valeur `#!python 248` deviendra `#!python 240`, la
valeur `#!python 80` restera à `#!python 80` et la valeur `#!python 7` passera
à `#!python 0`.
* On note son équivalent de l'image à camoufler (là encore, un entier qui
vaudra `#!python 0` **ou** `#!python 255`). On convertit cette valeur
en pour la ramener à `#!python 1` (lorsque la valeur initiale est `#!python 0`)
ou `#!python 0` (lorsque la valeur initiale est `#!python 255` (voir la
remarque de la section « [Un immeuble en PGM](#un-immeuble-en-pgm) »).
* On combine ces deux valeurs, la 2^de^ devenant le chiffre des unités
de la 1^re^.
* On écrit cette nouvelle valeur dans le fichier de destination.


### Mise en pratique

!!! note "À faire vous-même"

    === "Vos objectifs"
    
        1. Écrire un script `cacher.py` qui implémente l'algorihtme précédemment
        décrit. Il emploiera des variables nommées :
        
            * `fichier_a_cacher` pour désigner le nom de l'image « à cacher »
            et `a_cacher` pour l'accès effectif aux données ;
            * `fichier_conteneur` pour désigner le nom de l'image « conteneur »
            et `conteneur` pour l'accès effectif aux données ;
            * `fichier_dst` pour désigner le nom de l'image « destination »
            et `dst` pour l'écriture effective des données.
        
        2. Exécuter ce script et comparer le contenu du fichier « image-conteneur »
        et celui du fichier produit, afin de bien observer l'effet du programme
        sur les valeurs des pixels.

        3. Écrire un script `révéler.py` qui effectue le dé-camouflage.
        Il faudra procéder de façon symétrique : à partir d'une image qui
        en camoufle (supposément) une autre, il suffit d'extraire le chiffre
        des unités pour chaque valeur codant un pixel.
        
        4. Exécuter plusieurs fois ce programme, avec différentes
        images-conteneurs (par exemple [celle-ci : le Grand Palais,
        à Paris](images/40/grand-palais.pgm)), puis comparer dans une visionneuse
        l'image-conteneur d'origine et le résultat de l'enfouissement de
        l'image secrète. Observer des différences importantes et très visibles !
            
        5. Formuler une hypothèse quant à l'origine de ce phénomène, qui
        constitue un vrai souci (pourquoi ?).
        
        6. Imaginer une approche qui donnerait un meilleur résultat.
        
    === "Indications"
            
        1. On peut imaginer deux approches différentes, pour implémenter
        l'[algorithme décrit à la section précédente](#lalgorithme).
        
            * Pour supprimer le chiffre des unités d'un entier, on peut
            utiliser « l'opérateur modulo `%` » (qui donne le reste de la
            division euclidienne). Ainsi, en Python, l'instruction
            `#!python val_s = val_s - val_s % 10` fait le travail (sous
            réserve que `val_s`, qui désigne la valeur d'un pixel de
            l'image-conteneur, soit bel et bien un nombre entier).
        
                Une autre possibilité serait de faire `#!python val_s = int(val_s/10)*10`
                (efforcez-vous de comprendre ces formules !).
            
                Il faut ensuite ajouter `val_c` (qui désigne la valeur codant
                un pixel de l'image à cacher) à `val_s` pour obtenir la
                nouvelle valeur `val_n` qui permettra de construire petit
                à petit l'image de destination, camouflant l'image à cacher :
                `#!python val_n = val_s + val_c`.

            * Une autre approche consisterait à travailler _directement_ sur
            les _chaînes de caractères_ : ce serait adapté au contexte. En
            effet, on obtient des _chaînes de caractères_ en lisant un fichier
            ASCII ! Si `val_s` désigne la chaîne de caractères contenant
            la valeur d'un pixel de l'image conteneur, que `val_c` désigne
            la chaîne de caractères valant `#!python 0` ou `#!python 1`
            (en fonction de la valeur du pixel de l'image à cacher), on
            peut alors écrire : `#!python val_n = val_s[:-2] + val_c` (le `#!python -2`
            sera indispensable car on sait qu'ici chaque valeur sera enregistrée
            sur une ligne indépendante du fichier-image généré par GIMP, et
            un [caractère de fin de ligne](https://fr.wikipedia.org/wiki/Fin_de_ligne){target='__blank' rel='noopener'}
            sera donc présent.
        
                ??? info "Remarques"
                
                    * Python est capable de [gérer les différentes façon de passer
                    à la ligne](https://docs.python.org/3/library/functions.html#open){target='__blank' rel='noopener'}
                    qui existent, selon les systèmes d'exploitation (cf. paramètre
                    `newline` de la commande `open(...)`).
                    * L'affection `#!python val_n = val_s[:2] + val_c` ne serait
                    pas adaptée : pourquoi ?

            * **Question subsidiaire** : laquelle de ces deux approches (mathématique
            ou informatique) sera la plus performante ?  
            En d'autres termes, vaut-il mieux convertir les chaînes en entiers,
            travailler mathématiquement, et convertir à nouveau les résultats
            en chaînes ? Ou au contraire est-il plus efficient de se cantonner
            à l'astuce de l'informaticien ? À vous de chercher la réponse... 
        
        2. Le chiffre des unités de l'image produite par le script doit
        correspondre à la valeur du pixel correspondant de l'image à cacher
        (en gardant à l'esprit l'équivalence 255 ↔ 0 et 0 ↔ 1).
        
        3. Pour « décamoufler », il faut extraire le chiffre des unités
        de chaque valeur codant un pixel de l'image qui en cache une autre.
        Il suffit donc de faire, selon l'approche retenue (mathématique
        ou informatique), soit `#!python val_c = val_n % 10` soit
        `#!python val_c = val_n[-1]`.  
        On reconstruit alors l’image cachée à partir de l’ensemble de ces
        valeurs.
        
        4. Ça saute aux yeux quand on passe d'une image à l'autre (souvent,
        les visionneuses d'images permettent cela à l'aide des flèches de
        direction ++"←"++ et ++"→"++).
        
        5. Au maximum, combien de nuances peuvent séparer le pixel d'origine
        du pixel « camouflant » ?
        
        6. Réfléchissez plus :wink: !
    
    === "Illustration du résultat souhaité"
    
        <center>
        
        | [Image d'origine](images/40/montagne.pgm) | [Image à cacher](images/40/carte.pgm) | [Image résultante](images/40/montagne-cachant-carte.pgm) |
        |:----:|:---:|:---:|
        | [![](images/40/montagne.png)](images/40/montagne.pgm) | [![](images/40/carte.png)](images/40/carte.pgm) | [![](images/40/montagne-cachant-carte.png)](images/40/montagne-cachant-carte.pgm) |
        
        </center>
        
        Notez les « bandes » bien visibles, dans l'image de droite !
        
    === "Solution"
    
        J'ai choisi de faire le fainéant et d'exploiter directement les
        chaînes de caractères, au détriment de l'approche mathématique...
        
        ```python
        --8<-- "docs/codes/40/cacher.py"
        ```
        
        Pour le programme-réciproque `révéler.py`, celui qui extrait une
        image de celle qui la cache (supposément...) : débrouillez-vous
        par vous-même :wink: !
        
        **Question subsidiaire :** que donnerait ce programme sur une image
        « innocente » (comprendre : qui ne cache rien), à votre avis ?


### Plus de finesse dans le camouflage : on passe au binaire

Vous avez normalement observé que les altérations de l'image-conteneur
sont nettement visibles, après le camouflage de l'image à cacher.

En effet, notre approche manque pour le moins de... nuances ! C'est lié
au fait qu'en remplaçant le chiffre des unités dans les valeurs codant
les pixels de l'image-conteneur par `#!python 0` ou `#!python 1`, **on peut
perdre jusqu'à 9 nuances de gris** (si le chiffre des unités était initialement
`#!python 9`) : c'est donc une altération _importante_ de l'image d'origine,
que notre œil remarque sans peine. Ainsi, après une recherche sur Internet
de l'image suspecte, il est possible de retrouver l'image d'origine, et
de suspecter la supercherie : c'est tout le contraire de ce que l'on souhaite !

Il est dommage de perdre autant de nuances pour seulement stocker un `#!python 0`
ou un `#!python 1`... c'est à dire 1 bit ! Pour agir plus discrètement,
on va donc convertir chaque valeur décimale en binaire, et mettre en œuvre
la même logique **mais _uniquement_ sur le _bit de poids faible_** (c'est le
bit le plus à droite dans l'écriture binaire d'un nombre — celui qui détermine
si un nombre est pair ou impair, au passage).

En le remplaçant par la valeur `#!python 0` ou `#!python 1` issue de l'image
à cacher, **on ne perdra alors plus qu'une seule nuance _au plus_** !

??? info "Remarque"

    Avec cette approche, on peut également consacrer plus d'un bit de
    l'image d'origine au camouflage, ce qui permet de stocker soit une
    image avec quelques nuances de gris dans une image ayant plus de
    nuances de gris, soit de stocker _plusieurs_ images en noir et blanc
    au sein d'une même image conteneur !

On pourrait recycler notre code précédent en l'adaptant, naturellement.
Mais je vous propose d'en profiter pour mettre en œuvre des méthodes plus
efficaces de manipulation des images, en nous appuyant sur la bibliothèque
[Pillow](https://python-pillow.org/){target='__blank' rel='noopener'} ([voir
en annexe comment l'installer](#installer-pillow), si nécessaire). Cela
vous permettra en outre d'employer _enfin_ des formats d'images plus
traditionnels (JPEG, PNG, etc.).

!!! note "À faire vous-même"

    === "Vos objectifs"

        1. Voici le code du programme [`cacher-mieux.py`](codes/40/cacher-mieux.py){target='__blank' rel='noopener'},
        qui exploite cette approche plus fine. Après l'avoir étudié avec
        attention, écrire le code du programme réciproque `révéler-mieux.py`,
        qui révèle l'image cachée par le programme précédent.
        
            ```python
            --8<-- "docs/codes/40/cacher-mieux.py"
            ```
        
        2. Expliquer la ligne 23 : `#!python pxn = pxc - pxc % 2 + pxs`.

    === "Indications"
    
        Il y a plusieurs différences avec ce qu'on a fait jusqu'ici :
        
        * on ne lit pas ni n'écrit directement et au fur et à mesure le
        contenu du fichier de sortie. À la place, on charge l'ensemble des
        données des images dans une structure de données de Pillow (je vous
        laisse déterminer son type) : c'est le rôle des lignes 16 et 17 ;
        * on modifie l'une de ces deux structures (pourquoi en créerait-on
        une troisième ? ce serait un gâchis de mémoire vive) ;
        * on crée le fichier de sortie à partir de la structure de données
        modifiée.
        
        Pour créer le script `révéler.py`, il faut encore comprendre une
        subtilité. On travaille ici à partir d'une image en niveaux de gris,
        dont on a chargé dans une structure de données l'ensemble des valeurs
        codant ses pixels. On va alors modifier chacune de ces valeurs pour
        en extraire le bit de poids faible (`#!python 0` ou `#!python 1`).
        Mais on souhaite obtenir au final une image en noir et blanc, et
        si on enregistre l'image en l'état, elle sera à peine visible dans
        une visionneuse, car elle sera toujours considérée comme étant en
        niveau de gris !
        
        Il faudra donc convertir chaque bit de poids faible récupéré :
        
        * si le bit vaut `#!python 0`, rien à faire !
        * mais s'il vaut `#!python 1`, il faudra le transformer en
        `#!python 255` (qui, ici et dans ce contexte, correspondra au
        noir — encore une différence avec ce qu'on a vu jusqu'ici).
        
        Il ne restera plus, ensuite, qu'à convertir l'image obtenue en
        image 1 bit : [c'est facile avec Pillow](https://pillow.readthedocs.io/en/stable/reference/Image.html#PIL.Image.Image.convert){target='__blank' rel='noopener'}.
        Et bien sûr, il faudra l'enregistrer...
    
    === "Résultat obtenu lors du camouflage"
    
        On cache toujours une carte au trésor...
        
        <center>
        
        | [Image d'origine](images/40/montagne-800x600.png){target='__blank' rel='noopener'} | [Image résultante](images/40/montagne-800x600-cachant-mieux-carte.png){target='__blank' rel='noopener'} |
        |:----:|:---:|
        | [![](images/40/montagne-800x600.png)](images/40/montagne-800x600.png){target='__blank' rel='noopener'} | [![](images/40/montagne-800x600-cachant-mieux-carte.png)](images/40/montagne-800x600-cachant-mieux-carte.png){target='__blank' rel='noopener'} |
        
        </center>
        
        Notez les différences quasi-inexistantes entre les deux images !
        Mais il y en a bel et bien. Vous pouvez même en deviner l'origine...
        
    === "Solution"
    
        ```python
        --8<-- "docs/codes/40/révéler-mieux.py"
        ```

### Pour aller plus loin

!!! note "À faire vous-même"

    === "Vos objectifs"
    
        En vous inspirant des programmes précédents, écrivez **deux**
        programmes mettant en œuvre les principes qui viennent d'être
        exposés, mais :
        
        * qui permette de cacher une image en niveau de gris dans une image en
        couleurs ;
        * qui permette de cacher **trois** images en noir et blanc dans **une seule**
        image en couleurs !


    === "Indications"
        
        * Pour cacher une image en niveau de gris au sein d'une image en
        couleurs, on peut camoufler séparément la valeur des centaines,
        celle des dizaines et celle des unités, chacune venant altérer
        le chiffre des unités de l'une des composantes de l'image en
        couleurs.
        
            Par exemple :

            * le chiffre des centaines de l'image à cacher viendra
            écraser le chiffre des unités de la composante rouge de
            l'image-conteneur ;
            * le chiffre des dizaines de l'image à cacher viendra
            écraser le chiffre des unités de la composante verte de*
            l'image-conteneur ;
            * le chiffre des unités de l'image à cacher viendra écraser
            le chiffre des unités de la composante bleue de
            l'image-conteneur.
        
        * Cacher trois images en noir et blanc peut se faire sur le même
        principe.


## Annexes

### Produire et visualiser des images PBM, PGM ou PPM

Vous pouvez utiliser [GIMP](https://www.gimp.org/){target='__blank' rel='noopener'}
pour convertir une image quelconque en niveau de gris ou en noir et blanc.

1. Ouvrez une image en couleur dans GIMP (par exemple [celle-ci](images/40/bouleau.jpg){target='__blank' rel='noopener'}).
2. Il faut maintenant convertir l'image avant de l'exporter vers le
format adapté.

    * Pour convertir une image en niveaus de gris, dans le menu **Image**,
    sélectionnez le sous-menu **Mode**, puis cliquez sur **Niveau de gris**.
    
        <center>
        ![](images/40/gimp-conversion-gris.png)
        </center>

    * Pour convertir une image en noir et blanc, dans le menu **Image**,
    sélectionnez le sous-menu **Mode**, puis cliquez sur **Couleurs
    indexées** (juste _en dessous_ de l'entrée « Niveau de gris »).
    Une fenêtre s'ouvre alors, dans laquelle il faut cliquer sur **Utiliser
    une palette noir et blanc (1 bit)**, puis sur le bouton **Convertir**.

        <center>
        ![](images/40/gimp-conversion-nb.png)
        </center>
    
    ??? warning "Choisir le seuil séparant noir et blanc"
    
        Dans cette étape, on ne maitrîse pas la façon dont GIMP réalise
        le « seuillage » de l'image, c'est-à-dire le seuil au-delà duquel
        la proportion de blanc dans le niveau de gris d'un pixel conduira
        GIMP à convertir ce pixel en blanc (symétriquement, en dessous de
        ce seuil, le pixel gris sera converti en noir).  
        Il peut donc être utile de jouer avec l'outil « **Courbes** » de GIMP,
        qu'on trouve dans le menu **Couleurs** :
        
        <center>
        ![](images/40/gimp-courbes-1.png)
        </center>
        
        On peut ensuite, dans la fenêtre qui appraît, cliquer sur ce qui
        est initialement une droite, afin de la transformer en courbe aussi
        complexe qu'on le souahite. Ce faisant, on parviendra (dans une
        image en niveaux de gris) à renforcer certaines zones et à en estomper
        d'autres. En rapprochant certains tonalités du noir, on sera assuré
        que GIMP les transformera en noir pur en convertissant l'image au
        mode « 1 bit ». De même, les zones très claires seront converties
        en blanc.
        
        <center>
        ![](images/40/gimp-courbes-2.png)
        </center>
        
        Comparez :
        
        <center>
        
        | Image d'origine | Conversion en N&B par défaut | N&B après action sur la courbe |
        |:----:|:---:|:---:|
        | ![](images/40/bouleau.jpg){width=300} | ![](images/40/bouleau-nb.jpg){width=300} | ![](images/40/bouleau-nb-2.jpg){width=300} |
        
        </center>
    
3. Pour enregistrer une image au format _Portable PixMaps_ : dans le
menu **Fichier**, cliquer sur l'entrée **Exporter comme...** et choisissez
(ou écrivez directement) l'extension adaptée (« nouvelle_image.pbm »
pour une image en noir & blanc, « nouvelle_image.pgm » pour une image
en niveaux de gris ou « nouvelle_image.ppm » pour une image en couleurs).

4. Pour visualiser ces images sans avoir à démarrer GIMP (ce qui peut
s'avérer longuet sur des ordinateurs un peu anciens), n'importe quelle bonne
visionneuse fera l'affaire.

    * Sous GNU/Linux, aucun souci.
    * Cela m'étonnerait qu'il n'y en ait pas sous MacOS (mais n'ayant
    jamais utilisé ce système, je ne saurais être catégorique).
    * Quant à Windows, [IrfanView](http://www.irfanview.com/){target='__blank' rel='noopener'}
    est gratuit et fonctionne parfaitement.


### Comparer 2 images à la recherche d'informations cachées

Vous disposez de 2 images apparemment identiques (...plus ou moins !), et
vous souhaitez savoir si elles le sont **réellement**, ou s'il y a des chances
que l'une des deux contienne des informations cachées par stéganographie.
Vous pouvez là encore utiliser [GIMP](http://www.gimp.org/){target='__blank' rel='noopener'},
c'est très simple !

1. Ouvrez une image (n'importe laquelle).

2. Ouvrez l'autre image en tant que calque : dans le menu **Fichier**, cliquez
sur **Ouvrir en tant que calques...** (il est aussi possible d'utiliser
le raccourci clavier ++ctrl+alt+o++).

3. Dans la zone de gestion des calques (si elle n'est pas visible, vous
pouvez la faire apparaître ou mettre ladite zone en évidence en allant dans
le menu **Fenêtres**, puis dans le sous-menu **Fenêtres ancrables**, cliquez
ensuite sur l'entrée **Calques** — le raccourci clavier ++ctrl+l++ donne
le même résultat), changez le mode du _calque le plus **haut**_ pour **Différence**.

    <center>
    ![](images/40/gimp-calques-difference.png)
    </center>


4. Il faut ensuite aplatir l'image (ce qui revient à fusionner les calques) :
le bouton ![](images/40/gimp-bouton-aplatir-image-fusionner-calques.png)
de la zone « Calques » le permet. Un accès est également possible _via_
le menu **Image**, entrée **Aplatir l'image**.

5. Pour mettre en évidence les différences, il faut jouer avec les seuils
des « couleurs » : dans le menu **Couleurs**, cliquer sur **Seuil...**.
Positionnez alors le curseur de gauche à des valeurs basses — faites des
essais : il devra être mis à 1 dans le cas où l'on travaillerait sur le
bit de poids faible de chacune des valeurs des pixels de l'image conteneur.
Une fois le bon réglage trouvé, vous pourrez valider. Vous obtiendrez alors
une image qui pourrait ressembler à l'une de celles qui suivent, où les
différences entre les deux fichiers suspects sont nettement visibles.
    
    <center>
    
    | Image d'origine | Image à cacher |
    |:----:|:---:|
    | ![](images/40/montagne-800x600.png){width=300} | ![](images/40/carte-800x600.png){width=300} |
    
    
    | Image résultant du camouflage de<br> la seconde image dans la première | Image obtenue par<br>différence et seuillage |
    |:---:|:---:|
    | ![](images/40/montagne-800x600-cachant-carte.png){width=300} | ![](images/40/montagne-800x600-difference-seuil.png){width=300} |
    
    
    | Image<br>d'origine | Image cachant la carte<br>au trésor ci-dessus | Image obtenue par<br>différence et seuillage |
    |:----:|:---:|:---:|
    | ![](images/40/grand-palais.png){width=300} | ![](images/40/grand-palais-cachant-carte-1bit.png){width=300} | ![](images/40/grand-palais-differences.png){width=300} |
        
    </center>


### Installer [Pillow](https://python-pillow.org/){target='__blank' rel='noopener'}

La bibliothèque [Pillow](https://python-pillow.org/){target='__blank' rel='noopener'}
est une bibliothèque de traitement d'images pour Python. Elle permet d'ouvrir,
de manipuler, et de sauvegarder différents formats de fichiers graphiques.

Si son utilisation implique l'import `#!python from PIL import ...`, c'est
que [Pillow](https://python-pillow.org/){target='__blank' rel='noopener'}
succède à la bilbiothèque PIL, dont le développement a cessé
en 2009. Elle fournit donc des fonctionnalités largement semblables à celles
de PIL, mais est adaptée à Python 3.x (PIL ne fonctionne qu'avec Python 2.x).

??? info "Remarque"

    C'est par [Pillow](https://python-pillow.org/){target='__blank' rel='noopener'}
    qu'il faut passer si l'on veut recycler du code Python 2.x reposant
    sur PIL et l'adapter à Python 3.x. On dit que [Pillow](https://python-pillow.org/){target='__blank' rel='noopener'} est
    un [_fork_](https://fr.wikipedia.org/wiki/Fork_(développement_logiciel)){target='__blank' rel='noopener'}
    de PIL. Le code (libre, et donc accessible) de PIL a été repris, amélioré,
    et le nom a été modifié. Pour des besoins simples, rien ne change. Vous
    pouvez consulter la [documentation officielle](https://pillow.readthedocs.io/en/stable/){target='__blank' rel='noopener'}
    pour des informations exhaustives sur [Pillow](https://python-pillow.org/){target='__blank' rel='noopener'},
    ou [cet article](http://www.pythonforbeginners.com/gui/how-to-use-pillow){target='__blank' rel='noopener'}
    pour une brève introduction.

Sous Windows, vous pourrez procéder comme suit si [Pillow](https://python-pillow.org/){target='__blank' rel='noopener'}
n'est pas présent dans votre installation Python. Ouvrez une console en
ligne de commande en pressant simultanément les touches « Menu Windows »
<span style="position: relative; top: 10px;">![](images/40/windows-key-icon.png){width=30}</span>
et ++r++, puis saisissez `cmd` et validez en appuyant sur la touche ++return++.
Tapez alors les commandes ci-dessous :

```powershell
cd C:\<chemin>\<vers>\Python3x\Scripts
pip install --upgrade pip
pip install pillow
```


### Reformater le code des images PBM, PGM ou PPM

??? warning "Pour aller plus loin et faire un travail vraiment propre (:star::star::star:)"

    Ici, on va chercher à gérer convenablement les difficultés liées à la
    création des fichiers PBM et PGM par GIMP. Et, si l'on veut vraiment
    parer à toute éventualité, c'est une tâche **difficile**.

    Rappelons que, lorsqu'on utilise ce logiciel pour enregistrer une image
    en noir et blanc au format **PBM** (codé ASCII), les symboles `#!python 0`
    et `#!python 1` se suivent dans le fichier (texte) ainsi produit, séparés
    d'une espace, sur des lignes de 70 caractères de long (au maximum).

    ??? example "Exemple 1"
    
        ``` title="Code de l'image « e.pbm »"
        --8<-- "docs/images/40/e.pbm"
        ```

    Pour autant, l'organisation ci-dessous reste parfaitement licite !

    ??? example "Exemple 1 bis"
    
        ``` title="Code de l'image « e-bis.pbm »"
        --8<-- "docs/images/40/e-bis.pbm"
        ```

    Si on décide d'enregistrer une telle image en **PGM** (toujours codé
    ASCII), le noir sera codé par `#!python 0` (au lieu de `#!python 1`)
    et le blanc par `#!python 255` (au lieu de `#!python 0`). Mais, **surtout**,
    chaque code apparaîtra **seul sur « sa » ligne !**
    
    ??? example "Exemple 2"
    
        ``` title="Code de l'image « e.pgm »"
        --8<-- "docs/images/40/e.pgm"
        ```
    
    Si l'on souhaite créer des codes capables de gérer indifféremment ces
    deux types de fichiers, il faut absolument homogénéiser la façon dont
    les données qui les constituent sont organisées : on parle de **normalisation**.
    
    !!! note "À faire vous-même"

        === "Vos objectifs"
        
            1. Quelle est la structure de données la plus adaptée à la normalisation :
            une liste unique, dans laquelle il faut se repérer à l'aide
            de calculs arithmétiques exploitant les opérateurs `//` et `%` ?
            Ou une matrice (une liste de listes) ?
            2. Écrire une fonction permettant d'ouvrir n'importe quel fichier
            (PBM, PGM ou PPM) et renvoyant un dictionnaire de la forme :
            `{ "type" : <str>, "largeur" : <int>, "hauteur": <int>,
            "données" : ... }` où la valeur correspondant à la clef `"données"`
            sera la structure de données que vous aurez jugé la plus adaptée,
            convenablement « remplie » par les valeurs codant chaque pixel
            de l'image passée en argument (_via_ une chaîne de caractères
            nommant le fichier correspondant, bien sûr).
        
        === "Indications"
        
            1. Les travaux précédents sur le floutage (ou le durcissement)
            devraient vous convaincre qu'une des deux structures proposée
            est plus **compréhensible** que l'autre. Néanmoins, techniquement
            parlant, les deux sont possibles, il suffit juste d'écrire un
            code qui les gère convenablement...
            2. Pensez à utiliser les fonctions 
            
        === "Solution"
        
            1. Une matrice est plus adaptée car sa structure est plus claire.
            2. Le code ci-dessous est fonctionnel, mais loin d'être optimal.
            Il est peu lisible, et mériterait d'être redécoupé (en créant
            des fonctions). J'avoue l'avoir créé un peu « à l'arrache »,
            et j'aurais dû prendre quelques minutes pour faire un schéma
            avant de coder !  
            Vous pourrez tester ce code avec les fichiers
            [e.pbm](images/40/e.pbm){target='__blank' rel='noopener'}, 
            [e.pgm](images/40/e.pgm){target='__blank' rel='noopener'}, 
            [e-bis.pbm](images/40/e-bis.pbm){target='__blank' rel='noopener'}, 
            [e-bis.pgm](images/40/e-bis.pgm){target='__blank' rel='noopener'} et 
            [e-bad.pgm](images/40/e-bad.pgm){target='__blank' rel='noopener'}
            (regardez, dans ce dernier fichier, la liste des difficultés
            qu'on peut imaginer rencontrer avec un contenu non-conforme).
            
            ```python title="Script Python « harmoniser.py »"
            --8<-- "docs/codes/40/harmoniser.py"
            ```
        === "Pour aller encore plus loin (:star::star::star::star:)"

            Pensez-vous pouvoir faire de même avec une image PPM et ses
            **trois** valeur à assembler pour coder un pixel ?












[^stéganographie-vs-cryptographie]:
    Cela signifie qu'on peut exposer au vu et au su de tous une image qui
    en cacherait une autre par un procédé de stéganographie. Il s'agit bien
    d'une forme de camouflage, mais au lieu de reposer sur l'ombre (et tout
    le monde sait que ce qui est tapi dans l'ombre est immédiatement suspect),
    on expose le secret à la lumière !    
    Pour plus de sécurité, il est en outre tout à fait possible de combiner
    stéganographie et cryptographie : on écrit un message, on le chiffre
    (ce qui le rend inintelligibles), on transforme la suite incompréhensible
    de symboles ainsi obtenue en image, que l'on cache ensuite dans une
    autre image :sunglasses: !

[^tatouage-commerce]:
    Dans [ce rapport d'activité de l'institut Carnot-LSI](https://issuu.com/floralis/docs/rapport_d_activite_lsi){target='__blank' rel='noopener'}, 
    page 27, vous trouverez une trace de leur offre en la matière, dès 2011 :
    ![](images/carnot-LSI-tatouage.png)

[^tatouage-traitres]:
    En 1986, Margaret THATCHER, Première Ministre du ROYAUME-UNI, lassée
    des fuites d'informations dans la presse, aurait exigé que les traitements
    de textes de son cabinet soient configurés de sorte que l'identité des
    utilisateurs soit encodée via les valeurs d'espacement de leurs textes.
    En cas de fuite, il devenait ainsi possible d'identifier le coupable...
    
    D'autres équipes travaillent sur ces domaines, y compris en FRANCE.
    Vous pouvez trouver, par exemple dans ce [rapport du Laboratoire GIPSA](http://www.gipsa-lab.grenoble-inp.fr/fileadmin/gipsa/lab_presentation/rapport_gipsa_2009-2014.pdf){target='__blank' rel='noopener'}
    (pour Grenoble Images Parole Signal Automatique, laboratoire mixte issu
    notamment du CNRS et de l'Université de Grenoble-Alpes), page 139, la
    mention du « traçage de traîtres ».
    
    Dans le même esprit, vous pourrez peut-être observer d'étranges et discrets
    pixels jaunes sur certaines impressions ou sur des documents papiers
    que vous receve(re)z, en provenance d'administrations ou d'entreprises.
    Là encore, l'objectif est de tracer les documents (et leurs créateurs).
    Vous pourrez [lire cet article de Tuxicoman : « les métadonnées de vos
    impressions couleurs »](https://tuxicoman.jesuislibre.net/2015/05/les-metadonnees-de-vos-impressions-couleurs.html){target='__blank' rel='noopener'}.
    Moralité : malheur aux lanceurs d'alertes trop naïfs ! Il est **vraiment**
    important d'être conscient de l'impact qu'ont les technologies numériques
    sur le monde dans lequel on vit...

[^TraitementTextes]:
    Il faut distinguer un logiciel de [traitement de textes](https://fr.wikipedia.org/wiki/Traitement_de_texte){target='__blank' rel='noopener'},
    souvent désigné simplement par la locution [traitement de textes](https://fr.wikipedia.org/wiki/Logiciel_de_traitement_de_texte){target='__blank' rel='noopener'},
    d'un logiciel d'[édition de textes](https://fr.wikipedia.org/wiki/%C3%89diteur_de_texte){target='__blank' rel='noopener'}.  
    Le premier fait le plus souvent référence à un logiciel très sophistiqué
    (faisant parfois partie d'un ensemble de logiciels appelé _« suite bureautique »_),
    qui permet de modifier l'apparence des caractères (en offrant diverses
    polices ou fontes, le choix de leurs tailles, graisse, etc.), l'organisation
    du texte qui en résulte (centré, justifié, structuré _via_ des tableaux,
    etc.) et offre en outre la possibilité de vérifier l'orthographe, d'ajouter
    des photos, des dessins... Les documents « riches » ainsi créés doivent
    être enregistrés sous un format spécifique, capable de conserver tous
    ces détails. Ainsi, la suite bureautique [LibreOffice](https://fr.libreoffice.org/download/telecharger-libreoffice/){target='__blank' rel='noopener'}
    propose par défaut
    d'enregistrer les documents créés par son outil nommé « Writer » au format
    « [ODT](https://fr.wikipedia.org/wiki/OpenDocument){target='__blank' rel='noopener'} » :
    il s'agit en réalité d'un fichier compressé avec l'algorihtme
    [ZIP](https://fr.wikipedia.org/wiki/ZIP_(format_de_fichier)){target='__blank' rel='noopener'}
    (il suffit de renommer un fichier `document.odt` en `document.zip`
    et d'ouvrir `document.zip` avec le gestionnaire d'archive [7-zip](https://www.7-zip.fr/){target='__blank' rel='noopener'}
    pour découvrir la structure interne du fichier...).  
    L'éditeur de textes, lui, se contente de faciliter la saisie et la modification
    de textes. En général, il ne possède aucune fonction de mise en forme
    du texte (contrairement aux traitement des textes). Différents encodages
    sont souvent proposés (par exemple [ASCII](https://fr.wikipedia.org/wiki/American_Standard_Code_for_Information_Interchange){target='__blank' rel='noopener'},
    ou [UTF-8](https://fr.wikipedia.org/wiki/UTF-8){target='__blank' rel='noopener'},
    ou [ISO-8859-15](https://fr.wikipedia.org/wiki/ISO/CEI_8859-15){target='__blank' rel='noopener'},
    ou...). Ces logiciels se contentent donc de convertir les signaux émis
    par les touches du clavier en unités binaires représentant des caractères,
    selon les conventions indiquées par l'encodage choisi. Ces caractères sont
    affichés puis enregistrés dans un « fichier texte ».

[^Pillow]:
    Magie à laquelle on devra tout de même recourir par la suite, une fois les mécanismes à l'œuvre raisonnablement compris !

[^modif-orga-code-image]:
    Ce programme est donné dans la section « Annexes ».

[^ecritures-differees]:
Les données qu'on demande à un programme d'écrire dans un fichier **ne**
sont **pas** forcément écrites lorsque la commande se termine : elles ont
pu être mises en [mémoire tampon](https://fr.wikipedia.org/wiki/Mémoire_tampon){target='__blank' rel='noopener'}, en attente d'un moment favorable pour leur écriture effective (par exemple parce qu'au moment où la demande d'écriture est faite, le support de stockage est indisponible car fortement sollicité). Si, pendant cette attente, le fichier venait à être refermé, ces données seraient tout simplement
**perdues** ! Vous en conviendrez, ce serait ballot...

[gimp]: https://www.gimp.org/downloads/

[irfanview-portable]: https://portableapps.com/apps/graphics_pictures/irfanview_portable

[eye-of-gnome-appimage]: https://apprepo.de/appimage/eog
